package com.citi.mobileAutomation.runnerClass;

import java.io.IOException;

import org.junit.runner.RunWith;

import com.citi.mobileAutomation.cucumberClass.CustomCucumber;
import com.citi.mobileAutomation.mvnGenerator.CheckStatus;

import cucumber.api.CucumberOptions;

@RunWith(CustomCucumber.class)
@CucumberOptions(
	//tags = { "@Adhoc_TopUp_Done" }, 
		dryRun = false,
	//glue =
//	{"/src/test/java/com/citi/mobileAutomation/runnerClass"},
		features = {
				"Resources" })
public class RunnerFile {

}
