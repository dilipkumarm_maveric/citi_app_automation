package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.PromptPayObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class PromptPay_TH_Adhoc_TopUp_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Product :TH PromptPay Adhoc Mobile Payment
	 * Platform:Android
	 * 
	 */

	
	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	

    @When("^To verify whether on clicking Payments in footer menu for promptpay is navigate to payments drawer$")
    public void to_verify_whether_on_clicking_payments_in_footer_menu_for_promptpay_is_navigate_to_payments_drawer() throws Throwable {
    	ExtentReport.stepName = "To verify whether on clicking Payments in footer menu for promptpay is navigate to payments drawer";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @When("^To verify whether on clicking PromptPay in payments drawer is navigate to transfer to page$")
    public void to_verify_whether_on_clicking_promptpay_in_payments_drawer_is_navigate_to_transfer_to_page() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking PromptPay in payments drawer is navigate to transfer to page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_promptPayCTA);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required Search Transition Move screen up without showing tabs on clicking Search Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_search_transition_move_screen_up_without_showing_tabs_on_clicking_search_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required Search Transition Move screen up without showing tabs on clicking Search Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_search_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required Display default screen state same as when user first enters this screen on clicking Clear button in Search box Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_display_default_screen_state_same_as_when_user_first_enters_this_screen_on_clicking_clear_button_in_search_box_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required Display default screen state same as when user first enters this screen on clicking Clear button in Search box Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_clearButton_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking New payee Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaynewpayee300_on_clicking_new_payee_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking New payee Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_newPayee_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_TransferType_320 on clicking Transfer type value Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaytransfertype320_on_clicking_transfer_type_value_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_TransferType_320 on clicking Transfer type value Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_accNo_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Mobile number Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaynewpayee300_on_clicking_mobile_number_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Mobile number Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Next Arrow Button$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpayamount200_on_clicking_next_arrow_button() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Next Arrow Button";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_NextArrow_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking any available source account$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpayamount200_on_clicking_any_available_source_account() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking any available source account";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_cofSrcAccount_210);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_Review_600 on clicking Next Arrow Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpayreview600_on_clicking_next_arrow_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_Review_600 on clicking Next Arrow Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_nextArrow_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_Confirm_700 on clicking Pay Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpayconfirm700_on_clicking_pay_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_Confirm_700 on clicking Pay Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_payButton_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @When("^To verify whether the application is getting navigated to the required Payments Menu Screen From where TH Prompt Pay is started on clicking Done Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_payments_menu_screen_from_where_th_prompt_pay_is_started_on_clicking_done_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required Payments Menu Screen From where TH Prompt Pay is started on clicking Done Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_done_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @When("^To verify whether the application is getting navigated to the required It would take the screenshot of confirmation screen along with Citi Logo and customer can share it to any other person on clicking Share this transaction Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_it_would_take_the_screenshot_of_confirmation_screen_along_with_citi_logo_and_customer_can_share_it_to_any_other_person_on_clicking_share_this_transaction_label() throws Throwable {
    	ExtentReport.stepName = "To verify whether the application is getting navigated to the required It would take the screenshot of confirmation screen along with Citi Logo and customer can share it to any other person on clicking Share this transaction Label";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_share_700);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
    
    @When("^To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Pay Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpayamount200_on_clicking_pay_label() throws Throwable {
    	ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Pay Label";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_paymentsMenu);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(5000);
		}
    }

    @Then("^To verify whether the required mandatory field PromptPay in payments page is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_promptpay_in_payments_page_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field PromptPay in payments page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_promptPayCTA);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required Transfer to Label is getting displayed as per the condition Always$")
    public void to_verify_required_transfer_to_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer to Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferToLabel_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required Clear button in Search box Label is getting displayed as per the condition Upon customer entering atleast 1 char$")
    public void to_verify_required_clear_button_in_search_box_label_is_getting_displayed_as_per_the_condition_upon_customer_entering_atleast_1_char() throws Throwable {
       ExtentReport.stepName = "To verify required Clear button in Search box Label is getting displayed as per the condition Upon customer entering atleast 1 char";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_clearButton_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required New payee Prompt Pay TH Business to check Header is getting displayed as per the condition Always$")
    public void to_verify_required_new_payee_prompt_pay_th_business_to_check_header_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required New payee Prompt Pay TH Business to check Header is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_newPayeeHeader_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required Down Arrow Label is getting displayed as per the condition Always for default 300 screen$")
    public void to_verify_required_down_arrow_label_is_getting_displayed_as_per_the_condition_always_for_default_300_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Down Arrow Label is getting displayed as per the condition Always for default 300 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectTypeDownArrow_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required Mobile number Label is getting displayed as per the condition Always$")
    public void to_verify_required_mobile_number_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Mobile number Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_mobileNumber_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^To verify required Currency Label is getting displayed as per the condition Always$")
    public void to_verify_required_currency_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Currency Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			//CommonMethods.isElementPresent(PromptPayObjects.promptpay_ccy_200);
			CommonMethods.myAssertion(PromptPayObjects.promptpay_ccy_200, Xls_Reader_Path.mapReturnStringForKey("CCY", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @Then("^While entering required Amount Value in Amount field in the amount entry 200 screen$")
    public void while_entering_required_amount_value_in_amount_field_in_the_amount_entry_200_screen() throws Throwable {
       ExtentReport.stepName = "While entering required Amount Value in Amount field in the amount entry 200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			//CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_amountEditField_200,"56");
			CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_amountEditField_200,Xls_Reader_Path.mapReturnStringForKey("TransAmount", "PromptPay"));
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }


    @Then("^To verify required Tick Mark Label is getting displayed as per the condition NA$")
    public void to_verify_required_tick_mark_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Tick Mark Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferSubmittedLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    

    @Then("^To verify required Red Exclamation mark Label is getting displayed as per the condition NA$")
    public void to_verify_required_red_exclamation_mark_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required Red Exclamation mark Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @Then("^To verify required Blue Exclamation mark Label is getting displayed as per the condition NA$")
    public void to_verify_required_blue_exclamation_mark_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required Blue Exclamation mark Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field Payments in footer menu for promptpay is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_payments_in_footer_menu_for_promptpay_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field Payments in footer menu for promptpay is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required All Label is getting displayed as per the condition in 100 screen$")
    public void to_verify_required_all_label_is_getting_displayed_as_per_the_condition_in_100_screen() throws Throwable {
       ExtentReport.stepName = "To verify required All Label is getting displayed as per the condition in 100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_all_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account no Label is getting displayed as per the condition in 100 screen$")
    public void to_verify_required_account_no_label_is_getting_displayed_as_per_the_condition_in_100_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Account no Label is getting displayed as per the condition in 100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_accountNo_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Bill Payment Label is getting displayed as per the condition in 100 screen$")
    public void to_verify_required_bill_payment_label_is_getting_displayed_as_per_the_condition_in_100_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Bill Payment Label is getting displayed as per the condition in 100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_billPayment_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Mobile no Label is getting displayed as per the condition in 100 screen$")
    public void to_verify_required_mobile_no_label_is_getting_displayed_as_per_the_condition_in_100_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Mobile no Label is getting displayed as per the condition in 100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_mobileNo_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Search Magnifier Label is getting displayed as per the condition Always$")
    public void to_verify_required_search_magnifier_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Search Magnifier Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_search_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Search Label is getting displayed as per the condition NA$")
    public void to_verify_required_search_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Search Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_search_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Plus icon Label is getting displayed as per the condition Always$")
    public void to_verify_required_plus_icon_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Plus icon Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_newPayee_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required New payee Label is getting displayed as per the condition Always$")
    public void to_verify_required_new_payee_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required New payee Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_newPayee_100);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer type Label is getting displayed as per the condition Always$")
    public void to_verify_required_transfer_type_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer type Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_accNo_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account No transfer transfer type value Label is getting displayed as per the condition Always this default proxy will be shown$")
    public void to_verify_required_account_no_transfer_transfer_type_value_label_is_getting_displayed_as_per_the_condition_always_this_default_proxy_will_be_shown() throws Throwable {
       ExtentReport.stepName = "To verify required Account No transfer transfer type value Label is getting displayed as per the condition Always this default proxy will be shown";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_accNo_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Down Arrow Label is getting displayed as per the condition Always$")
    public void to_verify_required_down_arrow_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Down Arrow Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectTypeDownArrow_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Bank name Label is getting displayed as per the condition for bank selection refer to Prompt_BankSel_330$")
    public void to_verify_required_bank_name_label_is_getting_displayed_as_per_the_condition_for_bank_selection_refer_to_promptbanksel330() throws Throwable {
       ExtentReport.stepName = "To verify required Bank name Label is getting displayed as per the condition for bank selection refer to Prompt_BankSel_330";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_bankName_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account number Label is getting displayed as per the condition When Account Number Option is selected in Transfer Type$")
    public void to_verify_required_account_number_label_is_getting_displayed_as_per_the_condition_when_account_number_option_is_selected_in_transfer_type() throws Throwable {
       ExtentReport.stepName = "To verify required Account number Label is getting displayed as per the condition When Account Number Option is selected in Transfer Type";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_accountNumber_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Select transfer type Label is getting displayed as per the condition Always$")
    public void to_verify_required_select_transfer_type_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Select transfer type Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectTransferTypeHeader_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required List of transfer type Label is getting displayed as per the condition Below are the dropdown values which need to be shown to customer$")
    public void to_verify_required_list_of_transfer_type_label_is_getting_displayed_as_per_the_condition_below_are_the_dropdown_values_which_need_to_be_shown_to_customer() throws Throwable {
       ExtentReport.stepName = "To verify required List of transfer type Label is getting displayed as per the condition Below are the dropdown values which need to be shown to customer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectTransferTypeHeader_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Mobile number Label in 300 screen is getting displayed as per the condition Always$")
    public void to_verify_required_mobile_number_label_in_300_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Mobile number Label in 300 screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_mobileNumber_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required National ID Label is getting displayed as per the condition Always$")
    public void to_verify_required_national_id_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required National ID Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_nationalID_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Bill payment Label is getting displayed as per the condition Always$")
    public void to_verify_required_bill_payment_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Bill payment Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_billPayment_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account no transfer Label is getting displayed as per the condition Always$")
    public void to_verify_required_account_no_transfer_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Account no transfer Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_accountNo_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Top up PromptPay Label is getting displayed as per the condition Always$")
    public void to_verify_required_top_up_promptpay_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Top up PromptPay Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_topup_310);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required country code Label is getting displayed as per the condition Always will be auto populated 66 value$")
    public void to_verify_required_country_code_label_is_getting_displayed_as_per_the_condition_always_will_be_auto_populated_66_value() throws Throwable {
       ExtentReport.stepName = "To verify required country code Label is getting displayed as per the condition Always will be auto populated 66 value";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Mobile number value Label is getting displayed as per the condition Always while entering mobile number prediction search from contact list occurs Customer can select the corresponding contact from the contact list Mobile number should be exactly 9 digits entered by customer$")
    public void to_verify_required_mobile_number_value_label_is_getting_displayed_as_per_the_condition_always_while_entering_mobile_number_prediction_search_from_contact_list_occurs_customer_can_select_the_corresponding_contact_from_the_contact_list_mobile_number_should_be_exactly_9_digits_entered_by_customer() throws Throwable {
       ExtentReport.stepName = "To verify required Mobile number value Label is getting displayed as per the condition Always while entering mobile number prediction search from contact list occurs Customer can select the corresponding contact from the contact list Mobile number should be exactly 9 digits entered by customer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Next Arrow Button is getting displayed as per the condition in 300 screen$")
    public void to_verify_required_next_arrow_button_is_getting_displayed_as_per_the_condition_in_300_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Next Arrow Button is getting displayed as per the condition in 300 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_NextArrow_300);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Amount Value Label is getting displayed as per the condition$")
    public void to_verify_required_amount_value_label_is_getting_displayed_as_per_the_condition() throws Throwable {
       ExtentReport.stepName = "To verify required Amount Value Label is getting displayed as per the condition";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_amountLabel_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Down Arrow Label is getting displayed as per the condition If there is more than one SOF available$")
    public void to_verify_required_down_arrow_label_is_getting_displayed_as_per_the_condition_if_there_is_more_than_one_sof_available() throws Throwable {
       ExtentReport.stepName = "To verify required Down Arrow Label is getting displayed as per the condition If there is more than one SOF available";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_sourceAccountDownChevron_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required From account Label is getting displayed as per the condition Always$")
    public void to_verify_required_from_account_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required From account Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_fromAccountLabel_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account Name Label is getting displayed as per the condition Always$")
    public void to_verify_required_account_name_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Account Name Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_defaultSourceAccountName_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account number Label is getting displayed as per the condition Always Display Last 4 digits of Account Number$")
    public void to_verify_required_account_number_label_is_getting_displayed_as_per_the_condition_always_display_last_4_digits_of_account_number() throws Throwable {
       ExtentReport.stepName = "To verify required Account number Label is getting displayed as per the condition Always Display Last 4 digits of Account Number";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_defaultSourceAccountName_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Account number Label in 700 screen is getting displayed as per the condition Always$")
    public void to_verify_required_account_number_label_in_700_screen_is_getting_displayed_as_per_the_condition_always_display_last_4_digits_of_account_number() throws Throwable {
       ExtentReport.stepName = "To verify required Account number Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectedSrcAccount_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Available balance Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account$")
    public void to_verify_required_available_balance_label_is_getting_displayed_as_per_the_condition_always_display_available_balance_for_casa_accounts_ready_credit_account() throws Throwable {
       ExtentReport.stepName = "To verify required Available balance Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required CCY Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account$")
    public void to_verify_required_ccy_label_is_getting_displayed_as_per_the_condition_always_display_available_balance_for_casa_accounts_ready_credit_account() throws Throwable {
       ExtentReport.stepName = "To verify required CCY Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Balance Amount Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account$")
    public void to_verify_required_balance_amount_label_is_getting_displayed_as_per_the_condition_always_display_available_balance_for_casa_accounts_ready_credit_account() throws Throwable {
       ExtentReport.stepName = "To verify required Balance Amount Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Down Arrow Label is getting displayed as per the condition Optional To be display only if it satisfy below two conditions If user entered amount is less than minimum amount required If transaction is not LCY to LCY min transaction amount should be 0 dot 01 THB$")
    public void to_verify_required_down_arrow_label_is_getting_displayed_as_per_the_condition_optional_to_be_display_only_if_it_satisfy_below_two_conditions_if_user_entered_amount_is_less_than_minimum_amount_required_if_transaction_is_not_lcy_to_lcy_min_transaction_amount_should_be_0_dot_01_thb() throws Throwable {
       ExtentReport.stepName = "To verify required Down Arrow Label is getting displayed as per the condition Optional To be display only if it satisfy below two conditions If user entered amount is less than minimum amount required If transaction is not LCY to LCY min transaction amount should be 0 dot 01 THB";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_sourceAccountDownChevron_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the application is getting navigated to the required PromptPay_SOF_210 on clicking Down Arrow Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaysof210_on_clicking_down_arrow_label() throws Throwable {
       ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_SOF_210 on clicking Down Arrow Label";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_defaultSourceAccountName_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer from Label is getting displayed as per the condition NA$")
    public void to_verify_required_transfer_from_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer from Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferFromLabel_210);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    

    @And("^To verify required Account Name Nickname in 210 screen if available Label is getting displayed as per the condition NA$")
    public void to_verify_required_account_name_nickname_in_210_screen_if_available_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
        ExtentReport.stepName = "To verify required Account Name Nickname in 210 screen if available Label is getting displayed as per the condition NA";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_cofSrcAccount_210);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify required Account number Label in 210 screen is getting displayed as per the condition NA$")
    public void to_verify_required_account_number_label_in_210_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
        ExtentReport.stepName = "To verify required Account number Label in 210 screen is getting displayed as per the condition NA";
  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_cofSrcAccount_210);
  			}
  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
  			Thread.sleep(2000);
  			}
    }

    @And("^To verify required Available balance Label in 210 screen is getting displayed as per the condition NA$")
    public void to_verify_required_available_balance_label_in_210_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
        ExtentReport.stepName = "To verify required Available balance Label in 210 screen is getting displayed as per the condition NA";
  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_210);
  			}
  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
  			Thread.sleep(2000);
  			}
    }

    @And("^To verify required CCY Label in 210 screen is getting displayed as per the condition NA$")
    public void to_verify_required_ccy_label_in_210_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
        ExtentReport.stepName = "To verify required CCY Label in 210 screen is getting displayed as per the condition NA";
  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_210);
  			}
  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
  			Thread.sleep(2000);
  			}
    }

    @And("^To verify required Balance Amount Label in 210 screen is getting displayed as per the condition NA$")
    public void to_verify_required_balance_amount_label_in_210_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
        ExtentReport.stepName = "To verify required Balance Amount Label in 210 screen is getting displayed as per the condition NA";
  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_availableBalance_210);
  			}
  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
  			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
  			Thread.sleep(2000);
  			}
    }

    @And("^To verify required Account Name Nickname if available Label is getting displayed as per the condition NA$")
    public void to_verify_required_account_name_nickname_if_available_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Account Name Nickname if available Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectedSrcAccount_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Account number Label is getting displayed as per the condition NA$")
    public void to_verify_required_account_number_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Account number Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectedSrcAccount_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Available balance Label is getting displayed as per the condition NA$")
    public void to_verify_required_available_balance_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Available balance Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required CCY Label is getting displayed as per the condition NA$")
    public void to_verify_required_ccy_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required CCY Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_ccy_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Balance Amount Label is getting displayed as per the condition NA$")
    public void to_verify_required_balance_amount_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Balance Amount Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

  
    @And("^To verify required Next Arrow Label is getting displayed as per the condition Always in 200 screen$")
    public void to_verify_required_next_arrow_label_is_getting_displayed_as_per_the_condition_always_in_200_screen() throws Throwable {
       ExtentReport.stepName = "To verify required Next Arrow Label is getting displayed as per the condition Always in 200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_nextArrow_200);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    

    @And("^To verify required Review & Confirm Label is getting displayed as per the condition NA$")
    public void to_verify_required_review_confirm_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Review & Confirm Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_reviewAndConfirm_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }


    @And("^To verify required Amount Label is getting displayed as per the condition NA$")
    public void to_verify_required_amount_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Amount Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_amountLabel_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transaction amount Label is getting displayed as per the condition NA$")
    public void to_verify_required_transaction_amount_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transaction amount Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.myAssertionContains(PromptPayObjects.promptpay_amount_600, Xls_Reader_Path.mapReturnStringForKey("TransAmount", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required From Label is getting displayed as per the condition NA$")
    public void to_verify_required_from_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required From Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_fromLabel_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required To Label is getting displayed as per the condition NA$")
    public void to_verify_required_to_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "o verify required To Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_toLabel_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Contact Name Label is getting displayed as per the condition$")
    public void to_verify_required_contact_name_label_is_getting_displayed_as_per_the_condition() throws Throwable {
       ExtentReport.stepName = "To verify required Contact Name Label is getting displayed as per the condition";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Beneficiary name Label in review screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_name_label_in_review_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Beneficiary name Label in review screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryName_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Beneficiary name value in review screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_value_label_in_review_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Beneficiary name value in review screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.myAssertion(PromptPayObjects.promptpay_beneficiaryNameValue_600, Xls_Reader_Path.mapReturnStringForKey("Beneficiary Name", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Beneficiary bank name Label in review screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_bank_name_label_in_review_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Beneficiary bank name Label in review screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryBankNameLabel_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Beneficiary bank name value in review screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_bank_name_value_in_review_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Beneficiary bank name value in review screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.myAssertion(PromptPayObjects.promptpay_beneficiaryBankNameValue_600, Xls_Reader_Path.mapReturnStringForKey("Beneficiary Name in bank", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }


    @And("^To verify required When Label is getting displayed as per the condition NA$")
    public void to_verify_required_when_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required When Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_when_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Day Date Label is getting displayed as per the condition NA$")
    public void to_verify_required_day_date_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Day Date Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_date_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer will be made instantly Label in 600 screen is getting displayed as per the condition NA$")
    public void to_verify_required_transfer_will_be_made_instantly_label_in_600_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer will be made instantly Label in 600 screen is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferInstantly_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Transfer will be made instantly Label is getting displayed as per the condition NA$")
    public void to_verify_required_transfer_will_be_made_instantly_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer will be made instantly Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferInstantly_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Date Display Tool Tip Label is getting displayed as per the condition NA$")
    public void to_verify_required_date_display_tool_tip_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Date Display Tool Tip Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferInstantly_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required By tapping something you have read and understood the PDF Image Terms & Conditions Label is getting displayed as per the condition NA$")
    public void to_verify_required_by_tapping_something_you_have_read_and_understood_the_pdf_image_terms_conditions_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required By tapping something you have read and understood the PDF Image Terms & Conditions Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_disclaimer600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Pay Label is getting displayed as per the condition Always$")
    public void to_verify_required_pay_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Pay Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_payButton_600);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer submitted Label is getting displayed as per the condition NA$")
    public void to_verify_required_transfer_submitted_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer submitted Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferSubmittedLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required 700 screen ccy Label is getting displayed as per the condition NA$")
    public void to_verify_required_700_screen_ccy_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required 700 screen ccy Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			//CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDescription_700);
			CommonMethods.myAssertionContains(PromptPayObjects.promptpay_transferDescription_700, Xls_Reader_Path.mapReturnStringForKey("CCY", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required 700 screen amount Label is getting displayed as per the condition NA$")
    public void to_verify_required_700_screen_amount_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required 700 screen amount Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			//CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDescription_700);
			CommonMethods.myAssertionContains(PromptPayObjects.promptpay_transferDescription_700, Xls_Reader_Path.mapReturnStringForKey("TransAmount", "PromptPay"), "text");
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required will reach Label is getting displayed as per the condition NA$")
    public void to_verify_required_will_reach_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required will reach Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDescription_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required First name of beneficiary Label is getting displayed as per the condition NA$")
    public void to_verify_required_first_name_of_beneficiary_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required First name of beneficiary Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDescription_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required shortly Label is getting displayed as per the condition NA$")
    public void to_verify_required_shortly_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required shortly Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDescription_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }


    @And("^To verify required Transaction ID Label is getting displayed as per the condition NA$")
    public void to_verify_required_transaction_id_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transaction ID Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transactionId_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transaction ID value Label is getting displayed as per the condition NA$")
    public void to_verify_required_transaction_id_value_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Transaction ID value Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transactionId_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Recipient Details Label is getting displayed as per the condition NA$")
    public void to_verify_required_recipient_details_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
       ExtentReport.stepName = "To verify required Recipient Details Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_recipientDetailsLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Bank name mobile number Label is getting displayed as per the condition$")
    public void to_verify_required_bank_name_mobile_number_label_is_getting_displayed_as_per_the_condition() throws Throwable {
       ExtentReport.stepName = "To verify required Bank name mobile number Label is getting displayed as per the condition";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer date & type Label is getting displayed as per the condition always$")
    public void to_verify_required_transfer_date_type_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer date & type Label is getting displayed as per the condition always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferDateAndTypeLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Day, Date Label is getting displayed as per the condition Always$")
    public void to_verify_required_day_date_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Day, Date Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_date_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Transfer will be made instantly Label is getting displayed as per the condition Always$")
    public void to_verify_required_transfer_will_be_made_instantly_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Transfer will be made instantly Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_transferInstantly_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }


    
    @And("^To verify required Beneficiary name Label in confirmation screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_name_label_in_confirmation_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
        ExtentReport.stepName = "To verify required Beneficiary name Label in confirmation screen is getting displayed as per the condition Always";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryName_700);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify required Beneficiary name value in confirmation screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_name_value_in_confirmation_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
        ExtentReport.stepName = "To verify required Beneficiary name value in confirmation screen is getting displayed as per the condition Always";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryNameValue_700);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify required Beneficiary bank name Label in confirmation screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_bank_name_label_in_confirmation_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
        ExtentReport.stepName = "To verify required Beneficiary bank name Label in confirmation screen is getting displayed as per the condition Always";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryBankNameLabel_700);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify required Beneficiary bank name value in confirmation screen is getting displayed as per the condition Always$")
    public void to_verify_required_beneficiary_bank_name_value_in_confirmation_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
        ExtentReport.stepName = "To verify required Beneficiary bank name value in confirmation screen is getting displayed as per the condition Always";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_beneficiaryBankNameValue_700);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify required Sender name Label is getting displayed as per the condition Always$")
    public void to_verify_required_sender_name_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Sender name Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_senderNameLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Sender name value Label is getting displayed as per the condition Always$")
    public void to_verify_required_sender_name_value_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Sender name value Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_senderNameValue_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    
    @And("^To verify required From Label in 700 screen is getting displayed as per the condition Always$")
    public void to_verify_required_from_label_in_700_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required From Label in 700 screen is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_fromLabel_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Account Name Nickname in 700 screen if available Label is getting displayed as per the condition Always$")
    public void to_verify_required_account_name_nickname_in_700_screen_if_available_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Account Name Nickname in 700 screen if available Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectedSrcAccount_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Share this transaction Label is getting displayed as per the condition Always$")
    public void to_verify_required_share_this_transaction_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Share this transaction Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_share_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Done Label is getting displayed as per the condition Always$")
    public void to_verify_required_done_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       ExtentReport.stepName = "To verify required Done Label is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_done_700);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Payment unsucessful Label is getting displayed as per the condition NA$")
    public void to_verify_required_payment_unsucessful_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required Payment unsucessful Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required We are unable to process your payment now Label is getting displayed as per the condition NA$")
    public void to_verify_required_we_are_unable_to_process_your_payment_now_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required We are unable to process your payment now Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify required Payment pending Label is getting displayed as per the condition NA$")
    public void to_verify_required_payment_pending_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required Payment pending Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }

    @And("^To verify required Your payment is being reviewed Label is getting displayed as per the condition NA$")
    public void to_verify_required_your_payment_is_being_reviewed_label_is_getting_displayed_as_per_the_condition_na() throws Throwable {
    	 ExtentReport.stepName = "To verify required Your payment is being reviewed Label is getting displayed as per the condition NA";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
			Thread.sleep(2000);
			}
    }
    

    @When("^To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Top up PromptPay Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaynewpayee300_on_clicking_top_up_promptpay_label() throws Throwable {
    	ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Top up PromptPay Label";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_topup_310);
		Thread.sleep(5000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @Then("^To verify required Top up PromptPay Label in 300 screen is getting displayed as per the condition Always$")
    public void to_verify_required_top_up_promptpay_label_in_300_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
    	ExtentReport.stepName = "To verify required Top up PromptPay Label in 300 screen is getting displayed as per the condition Always";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_selectedType_300);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @Then("^While entering required e Wallet No Value in e Wallet field in the new payee 300 screen$")
    public void while_entering_required_e_wallet_no_value_in_e_wallet_field_in_the_new_payee_300_screen() throws Throwable {
    	ExtentReport.stepName = "While entering required e Wallet No Value in e Wallet field in the new payee 300 screen";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		//CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_eWalletNo_300,"456789435678963");
		CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_eWalletNo_300,Xls_Reader_Path.mapReturnStringForKey("eWalletNo", "PromptPay"));
		Thread.sleep(5000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required e Wallet No Label is getting displayed as per the condition Always$")
    public void to_verify_required_e_wallet_no_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
    	ExtentReport.stepName = "To verify required e Wallet No Label is getting displayed as per the condition Always";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_eWalletNo_300);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
    
    @And("^To verify required eWallet No Label is getting displayed as per the condition$")
    public void to_verify_required_ewallet_no_label_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required eWallet No Label is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		//CommonMethods.isElementPresent(PromptPayObjects.promptpay_eWalletNo_600);
		CommonMethods.myAssertion(PromptPayObjects.promptpay_eWalletNo_600, Xls_Reader_Path.mapReturnStringForKey("eWalletNo", "PromptPay"), "text");
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required eWallet Bank Name Label is getting displayed as per the condition$")
    public void to_verify_required_ewallet_bank_name_label_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required eWallet Bank Name Label is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_eWalletBank);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required eWallet No and Bank Name Label is getting displayed as per the condition$")
    public void to_verify_required_ewallet_no_and_bank_name_label_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required eWallet No and Bank Name Label is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		//CommonMethods.isElementPresent(PromptPayObjects.promptpay_recipientDetailsValue_700);
		CommonMethods.myAssertionContains(PromptPayObjects.promptpay_recipientDetailsValue_700, Xls_Reader_Path.mapReturnStringForKey("eWalletNo", "PromptPay"), "text");
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
	
    @When("^To verify whether the application is getting navigated to the required PromptPay_BankSelection_310 on clicking Bank name Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaybankselection310_on_clicking_bank_name_label() throws Throwable {
    	ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_BankSelection_310 on clicking Bank name Label";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_bankName_300);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
		
    }

    @When("^To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking any one of the bank Label$")
    public void to_verify_whether_the_application_is_getting_navigated_to_the_required_promptpaynewpayee300_on_clicking_any_one_of_the_bank_label() throws Throwable {
       	ExtentReport.stepName = "To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking any one of the bank Label";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_selbank_citibank_310);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @Then("^To verify required Recipient bank Label is getting displayed as per the condition Always$")
    public void to_verify_required_recipient_bank_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       	ExtentReport.stepName = "To verify required Recipient bank Label is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_selbank_title_310);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required Citi bank Label is getting displayed as per the condition Always$")
    public void to_verify_required_citi_bank_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       	ExtentReport.stepName = "To verify required Citi bank Label is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_selbank_citibank_310);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required HSBC bank Label is getting displayed as per the condition Always$")
    public void to_verify_required_hsbc_bank_label_is_getting_displayed_as_per_the_condition_always() throws Throwable {
       	ExtentReport.stepName = "To verify required HSBC bank Label is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_selbank_hsbcbank_310);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }   

    @Then("^While entering required Account number Value in Account number editText field in the new payee 300 screen$")
    public void while_entering_required_account_number_value_in_account_number_edittext_field_in_the_new_payee_300_screen() throws Throwable {
       	ExtentReport.stepName = "While entering required Account number Value in Account number editText field in the new payee 300 screen";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    			CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_accountNumber_300, Xls_Reader_Path.mapReturnStringForKey("AccountNo", "PromptPay"));
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }
    
    @And("^To verify required account number is getting displayed as per the condition$")
    public void to_verify_required_account_number_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required account number is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.myAssertion(PromptPayObjects.promptpay_eWalletNo_600, Xls_Reader_Path.mapReturnStringForKey("AccountNo", "PromptPay"), "text");
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required account number bank name is getting displayed as per the condition$")
    public void to_verify_required_account_number_bank_name_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required account number bank name is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_eWalletBank);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required account number and Bank Name Label is getting displayed as per the condition$")
    public void to_verify_required_account_number_and_bank_name_label_is_getting_displayed_as_per_the_condition() throws Throwable {
    	ExtentReport.stepName = "To verify required account number and Bank Name Label is getting displayed as per the condition";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.myAssertionContains(PromptPayObjects.promptpay_recipientDetailsValue_700, Xls_Reader_Path.mapReturnStringForKey("AccountNo", "PromptPay"), "text");
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
    

    @When("^To verify whether the application remove the nickname field when payee toggle is off on clicking save as payee list toggle button$")
    public void to_verify_whether_the_application_remove_the_nickname_field_when_payee_toggle_is_off_on_clicking_save_as_payee_list_toggle_button() throws Throwable {
    	ExtentReport.stepName = "To verify whether the application remove the nickname field when payee toggle is off on clicking save as payee list toggle button";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_saveAsPayeeToggle_600);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required save as payee list Label is getting displayed$")
    public void to_verify_required_save_as_payee_list_label_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required save as payee list Label is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_saveAsPayeeList_600);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required save as payee list toggle button is getting displayed$")
    public void to_verify_required_save_as_payee_list_toggle_button_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required save as payee list toggle button is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_saveAsPayeeToggle_600);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
    
    @And("^To verify required nickname Label in review screen is getting displayed$")
    public void to_verify_required_nickname_label_in_review_screen_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required nickname Label in review screen is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_nicknameLabel_600);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required nickname value in review screen is getting displayed$")
    public void to_verify_required_nickname_value_in_review_screen_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required nickname value in review screen is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.sendKeysTextBoxMultipleSession(PromptPayObjects.promptpay_nicknameValue_600, Xls_Reader_Path.mapReturnStringForKey("PayeeName", "PromptPay"));
		Thread.sleep(2000);
		CommonMethods.clickMethodMultipleSession(PromptPayObjects.promptpay_nicknameLabel_600);
		Thread.sleep(2000);
		CommonMethods.myAssertion(PromptPayObjects.promptpay_nicknameValue_600, Xls_Reader_Path.mapReturnStringForKey("PayeeName", "PromptPay"), "text");
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required save as payee list label in confirmation screen is getting displayed$")
    public void to_verify_required_save_as_payee_list_label_in_confirmation_screen_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required save as payee list label in confirmation screen is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_saveToPayeeListAs_700);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }

    @And("^To verify required saved payee value in confirmation screen is getting displayed$")
    public void to_verify_required_saved_payee_value_in_confirmation_screen_is_getting_displayed() throws Throwable {
    	ExtentReport.stepName = "To verify required saved payee value in confirmation screen is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_savedPayeeName_700);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
		Thread.sleep(2000);
		}
    }
    
    @Then("^To verify required Back Button in review screen is getting displayed as per the condition NA$")
    public void to_verify_required_back_button_in_review_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
      	ExtentReport.stepName = "To verify required Back Button in review screen is getting displayed as per the condition NA";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_backButton_600);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required Close Label in transfer to screen is getting displayed as per the condition Always$")
    public void to_verify_required_close_label_in_transfer_to_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
      	ExtentReport.stepName = "To verify required Close Label in transfer to screen is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_closeIcon_100);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required Back button in new payee screen is getting displayed as per the condition Always$")
    public void to_verify_required_back_button_in_new_payee_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
      	ExtentReport.stepName = "To verify required Back button in new payee screen is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_newPayeeBackButton_300);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required Back button in amount entry screen is getting displayed as per the condition Always$")
    public void to_verify_required_back_button_in_amount_entry_screen_is_getting_displayed_as_per_the_condition_always() throws Throwable {
      	ExtentReport.stepName = "To verify required Back button in amount entry screen is getting displayed as per the condition Always";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_backButton_200);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify required Close button in review screen is getting displayed as per the condition NA$")
    public void to_verify_required_close_button_in_review_screen_is_getting_displayed_as_per_the_condition_na() throws Throwable {
      	ExtentReport.stepName = "To verify required Close button in review screen is getting displayed as per the condition NA";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_close_600);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(PromptPayObjects.promptpay_paymentsMenu);
    		Thread.sleep(2000);
    		}
    }

    
		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
