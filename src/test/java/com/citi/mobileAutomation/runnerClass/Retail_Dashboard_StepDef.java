package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.objectRepository.Retail_Dashboard_Objects;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Retail_Dashboard_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Functionality :Retail Dashboard
	 * Domain:Enable
	 * Platform:Android
	 * 
	 */

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	@And("^To verify whether the required mandatory field checkings account is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checkings_account_is_getting_displayed() throws Throwable {
		 ExtentReport.stepName = "To verify whether the required mandatory field checkings account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checkings account number is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checkings_account_number_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checkings account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checkings account balance ccy is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checkings_account_balance_ccy_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checkings account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checkings account balance value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checkings_account_balance_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checkings account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checkings account right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checkings_account_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checkings account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking checking account right chevron it is navigated to depositada page$")
    public void to_verify_whether_on_clicking_checking_account_right_chevron_it_is_navigated_to_depositada_page() throws Throwable {
         ExtentReport.stepName = "To verify whether on clicking checking account right chevron it is navigated to depositada page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			//CommonMethods.isElementPresent(RetailDashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(60000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account number in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_number_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account number in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingAccNumber);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account manage cta in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_manage_cta_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account manage cta in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAMFACTA);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account available now label in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_available_now_label_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account available now label in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAavailableNowLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account inactive account speak label in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_inactive_account_speak_label_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account inactive account speak label in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account available now balance ccy in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_available_now_balance_ccy_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account available now balance ccy in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account available now balance value in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_available_now_balance_value_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account available now balance value in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account available now right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_available_now_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account available now right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAavailableNowRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account activate your account cta is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_activate_your_account_cta_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account activate your account cta is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account make a transfer cta is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_make_a_transfer_cta_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account make a transfer cta is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAMFACTA);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account global wallet icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_global_wallet_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account global wallet icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAGWIcon);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account global wallet header label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_global_wallet_header_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account global wallet header label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAGWLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account global wallet description is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_global_wallet_description_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account global wallet description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAGWDescrip);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account global wallet right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_global_wallet_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account global wallet right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAGWRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account statement icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_statement_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account statement icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAStatementIcon);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account statement label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_statement_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account statement label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAStatementLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account statement month and year value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_statement_month_and_year_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account statement month and year value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAStatementValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account statement right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_statement_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account statement right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAStatementRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account last transactions label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_last_transactions_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account last transactions label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAlastTransactions);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account last transaction search icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_last_transaction_search_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account last transaction search icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAsearch);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account last transaction search inline text is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_last_transaction_search_inline_text_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account last transaction search inline text is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAsearch);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account first transaction date is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_first_transaction_date_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account first transaction date is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAfirstTransMatureDate);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account first trasaction amount ccy is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_first_trasaction_amount_ccy_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account first trasaction amount ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAfirstTransAmount);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account first trasaction amount value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_first_trasaction_amount_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account first trasaction amount value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAfirstTransAmount);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account first trasaction description is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_first_trasaction_description_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account first trasaction description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAfirstTransDescrip);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account first trasaction right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_first_trasaction_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account first trasaction right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAfirstTransRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account view all transaction label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_view_all_transaction_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account view all transaction label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAViewAllTransactionLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field checking account view all transaction right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_checking_account_view_all_transaction_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field checking account view all transaction right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAViewAllTransactionRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }
    
    @And("^To verify whether the required mandatory field saving account is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_saving_account_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field saving account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAViewAllTransactionRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field saving account number is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_saving_account_number_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field saving account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field saving account balance ccy is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_saving_account_balance_ccy_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field saving account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field saving account balance value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_saving_account_balance_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field saving account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field saving account right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_saving_account_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field saving account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking savings account right chevron it is navigated to depositada page$")
    public void to_verify_whether_on_clicking_savings_account_right_chevron_it_is_navigated_to_depositada_page() throws Throwable {
         ExtentReport.stepName = "To verify whether on clicking savings account right chevron it is navigated to depositada page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAsavingsLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account number in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_number_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account number in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAsavingsAccNo);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account manage cta in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_manage_cta_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account manage cta in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAManageCTA);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account available now label in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_available_now_label_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account available now label in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAavailableNowLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account inactive account speak label in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_inactive_account_speak_label_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account inactive account speak label in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account available now balance ccy in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_available_now_balance_ccy_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account available now balance ccy in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account available now balance value in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_available_now_balance_value_in_depositada_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account available now balance value in depositada is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account available now right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_available_now_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account available now right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAavailableNowValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account activate your account cta is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_activate_your_account_cta_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account activate your account cta is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account make a transfer cta is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_make_a_transfer_cta_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account make a transfer cta is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAMFACTA);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account global wallet icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_global_wallet_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account global wallet icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account global wallet header label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_global_wallet_header_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account global wallet header label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account global wallet description is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_global_wallet_description_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account global wallet description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account global wallet right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_global_wallet_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account global wallet right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account statement icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_statement_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account statement icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAStatementIcon);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account statement label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_statement_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account statement label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAStatementLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account statement month and year value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_statement_month_and_year_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account statement month and year value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAStatementValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account statement right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_statement_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account statement right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAStatementValue);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account last transactions label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_last_transactions_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account last transactions label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADALastTransaction);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account last transaction search icon is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_last_transaction_search_icon_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account last transaction search icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAsearch);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account last transaction search inline text is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_last_transaction_search_inline_text_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account last transaction search inline text is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAsearch);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account first transaction date is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_first_transaction_date_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account first transaction date is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAfirstTransMatureDate);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account first trasaction amount ccy is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_first_trasaction_amount_ccy_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account first trasaction amount ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAfirstTransAmount);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account first trasaction amount value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_first_trasaction_amount_value_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account first trasaction amount value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAfirstTransAmount);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account first trasaction description is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_first_trasaction_description_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account first trasaction description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAfirstDescrip);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account first trasaction right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_first_trasaction_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account first trasaction right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAfirstDescrip);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account view all transaction label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_view_all_transaction_label_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account view all transaction label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAViewAllTransactionLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field savings account view all transaction right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_savings_account_view_all_transaction_right_chevron_is_getting_displayed() throws Throwable {
         ExtentReport.stepName = "To verify whether the required mandatory field savings account view all transaction right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailsavingsADAViewAllTransactionLabel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field time deposits account is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field time deposits account is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account number is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_number_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account number is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account balance ccy is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_balance_ccy_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account balance ccy is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account balance value is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_balance_value_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account balance value is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account right chevron is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_right_chevron_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account right chevron is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAtimedepositLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account number in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_number_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account number in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAtimedepositLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits account manage cta in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_account_manage_cta_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits account manage cta in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAtimedepositLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits total principal label in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_total_principal_label_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits total principal label in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAtimedepositLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits total principal amount value in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_total_principal_amount_value_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits total principal amount value in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAtotalprincipalValue);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits first multiple time deposit in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }


    @And("^To verify whether the required mandatory field time deposits first multiple time deposit principal number in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_principal_number_in_depositada_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit principal number in depositada is getting displayed";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositplacementNo);
		Thread.sleep(5000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
		Thread.sleep(2000);
		}
    }


    @And("^To verify whether the required mandatory field time deposits first multiple time deposit amount ccy in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_amount_ccy_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit amount ccy in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositAmount);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits first multiple time deposit amount value in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_amount_value_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit amount value in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositAmount);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits first multiple time deposit maturity date in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_maturity_date_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit maturity date in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositMatureDate);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits first multiple time deposit right chevron in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_first_multiple_time_deposit_right_chevron_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits first multiple time deposit right chevron in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAfirsttimedepositMatureDate);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits open time deposit cta in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_open_time_deposit_cta_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits open time deposit cta in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAOTDCTA);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits statement in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_statement_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits statement in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAStatementLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }

    @And("^To verify whether the required mandatory field time deposits statement right chevron in depositada is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_time_deposits_statement_right_chevron_in_depositada_is_getting_displayed() throws Throwable {
                ExtentReport.stepName = "To verify whether the required mandatory field time deposits statement right chevron in depositada is getting displayed";
 			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailtimedepositADAStatementLabel);
 			Thread.sleep(5000);
 			}
 			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
 			CommonMethods.isElementPresent(Retail_Dashboard_Objects.retailcheckingsADAcheckingLabel);
 			Thread.sleep(2000);
 			}
    }
		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
