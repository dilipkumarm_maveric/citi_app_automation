package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.SGFastDownTimeObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class SG_FastDowntime_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Product :SG Fast Down Time - PT2
	 * Platform:Android
	 * 
	 */

	
	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	

	 @And("^To verify whether the required mandatory field Payments in footer menu is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_payments_in_footer_menu_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Payments in footer menu is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking Payments in footer menu is navigate to payments page$")
	    public void to_verify_whether_on_clicking_payments_in_footer_menu_is_navigate_to_payments_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Payments in footer menu is navigate to payments page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Transfer in payments page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_transfer_in_payments_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Transfer in payments page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_Transfers);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking Transfer in payments page is navigate to transfer to page$")
	    public void to_verify_whether_on_clicking_transfer_in_payments_page_is_navigate_to_transfer_to_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Transfer in payments page is navigate to transfer to page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_Transfers);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field PT2 payee is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_pt2_payee_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field PT2 payee is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PT2Payee);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking PT2 payee is navigate to amount entry screen$")
	    public void to_verify_whether_on_clicking_pt2_payee_is_navigate_to_amount_entry_screen() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking PT2 payee is navigate to amount entry screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_PT2Payee);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the entering the amount value in amount field$")
	    public void to_verify_whether_the_entering_the_amount_value_in_amount_field() throws Throwable {
	           ExtentReport.stepName = "To verify whether the entering the amount value in amount field";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.sendKeysTextBoxMultipleSession(SGFastDownTimeObjects.sgfdt_AmountEntry200, "50.00");
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field next button is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_next_button_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field next button is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_NextButton);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking next button is navigate to review and confirm screen$")
	    public void to_verify_whether_on_clicking_next_button_is_navigate_to_review_and_confirm_screen() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking next button is navigate to review and confirm screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_NextButton);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field fast time transfer instantly description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_fast_time_transfer_instantly_description_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field fast time transfer instantly description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			Thread.sleep(20000);
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_FastTooltip);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field pay button is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_pay_button_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field pay button is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PayButton);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking pay button is navigate to fast down time drawer screen$")
	    public void to_verify_whether_on_clicking_pay_button_is_navigate_to_fast_down_time_drawer_screen() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking pay button is navigate to fast down time drawer screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_PayButton);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field red exclaimation mark in fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_red_exclaimation_mark_in_fast_down_time_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field red exclaimation mark in fast down time is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_RedExlamationMark);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field fast down time transfer header in fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_fast_down_time_transfer_header_in_fast_down_time_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field fast down time transfer header in fast down time is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_FastDowntimeHeader);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field fast down time transfer description in fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_fast_down_time_transfer_description_in_fast_down_time_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field fast down time transfer description in fast down time is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_FastDowntimeDescription);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field pay with giro button in fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_pay_with_giro_button_in_fast_down_time_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field pay with giro button in fast down time is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PayWithGIRO);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field cancel button in fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_cancel_button_in_fast_down_time_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field cancel button in fast down time is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_Cancel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking cancel button is navigate to stay on same screen$")
	    public void to_verify_whether_on_clicking_cancel_button_is_navigate_to_stay_on_same_screen() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking cancel button is navigate to stay on same screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_Cancel);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking pay with giro button in fast down time is navigate to fast down time drawer screen$")
	    public void to_verify_whether_on_clicking_pay_with_giro_button_in_fast_down_time_is_navigate_to_fast_down_time_drawer_screen() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking pay with giro button in fast down time  is navigate to fast down time drawer screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_PayWithGIRO);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
			Thread.sleep(2000);
			}
	    }
	    
	    //---------------adhoc--------//
	    
	    @And("^To verify whether the required mandatory field New Payee in Transfer to screen is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_new_payee_in_transfer_to_screen_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field New Payee in Transfer to screen is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdtAdhoc_NewPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking New Payee in Transfer to screen is navigate to transfer to New Payee page$")
	    public void to_verify_whether_on_clicking_new_payee_in_transfer_to_screen_is_navigate_to_transfer_to_new_payee_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking New Payee in Transfer to screen is navigate to transfer to New Payee page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_NewPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field New Payee Header is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_new_payee_header_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field New Payee Header is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdtAdhoc_NewPayeeHeader);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Country is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_country_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Country is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdtAdhoc_Country);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Country Value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_country_value_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Country Value is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdtAdhoc_Country);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering the payee name in Name field$")
	    public void to_verify_whether_the_entering_the_payee_name_in_name_field() throws Throwable {
	           ExtentReport.stepName = "To verify whether the entering the payee name in Name field";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_Name,"Ted");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking Bank field in New Payee screen is navigate to transfer to Recipient bank page$")
	    public void to_verify_whether_on_clicking_bank_field_in_new_payee_screen_is_navigate_to_transfer_to_recipient_bank_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Bank field in New Payee screen is navigate to transfer to Recipient bank page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_Bank);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking selected bank is navigate back to transfer to New Payee page$")
	    public void to_verify_whether_on_clicking_selected_bank_is_navigate_back_to_transfer_to_new_payee_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking selected bank is navigate back to transfer to New Payee page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_SelectedBank);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering account number in Account number field$")
	    public void to_verify_whether_the_entering_account_number_in_account_number_field() throws Throwable {
	           ExtentReport.stepName = "To verify whether the entering account number in Account number field";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_AccNumber,"674546799754");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking next arrow is navigate back to transfer to payee alert page$")
	    public void to_verify_whether_on_clicking_next_arrow_is_navigate_back_to_transfer_to_payee_alert_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking next arrow is navigate back to transfer to payee alert page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_NextButton);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking i know this payee is navigate back to transfer to amount entry screen page$")
	    public void to_verify_whether_on_clicking_i_know_this_payee_is_navigate_back_to_transfer_to_amount_entry_screen_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking i know this payee is navigate back to transfer to amount entry screen page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdtAdhoc_IknowthisPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering the amount value in amount field in adhoc flow$")
	    public void to_verify_whether_the_entering_the_amount_value_in_amount_field_in_adhoc_flow() throws Throwable {
	           ExtentReport.stepName = "To verify whether the entering the amount value in amount field in adhoc flow";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(SGFastDownTimeObjects.sgfdt_AmountEntry200,"456.00");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field next button is getting displayed in adhoc flow$")
	    public void to_verify_whether_the_required_mandatory_field_next_button_is_getting_displayed_in_adhoc_flow() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field next button is getting displayed in adhoc flow";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_NextButton);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow$")
	    public void to_verify_whether_on_clicking_next_button_is_navigate_to_review_and_confirm_screen_in_adhoc_flow() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(SGFastDownTimeObjects.sgfdt_NextButton);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(SGFastDownTimeObjects.sgfdt_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }
	
		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
