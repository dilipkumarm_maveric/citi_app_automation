package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class QuickCash_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	/*
	 * @Author:Dilipkumar 
	 * Platform:Android
	 * Functionality:ALOP - Quick Cash
	 * Domain:Borrow
	 */
	
    @And("^Click on cards in dashboard screen$")
    public void click_on_cards_in_dashboard_screen() throws Throwable {
    		ExtentReport.stepName = "Click on cards in dashboard screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Click on quick cash in dashboard screen$")
    public void click_on_quick_cash_in_dashboard_screen() throws Throwable {
        ExtentReport.stepName = "Click on quick cash in dashboard screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.quickCash_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Down Arrow image is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_image_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down Arrow image is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Down Arrow image is getting navigated in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_image_is_getting_navigated_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down Arrow image is getting navigated in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_what_is_quick_cash_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.whatisQuickCash_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field We convert your available credit limit into cash, payable in monthly installations among with your monthly Citi card payment is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_we_convert_your_available_credit_limit_into_cash_payable_in_monthly_installations_among_with_your_monthly_citi_card_payment_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field We convert your available credit limit into cash, payable in monthly installations among with your monthly Citi card payment is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.weConvert_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Benefits is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_your_benefits_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Benefits is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.yourBenefits_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Icon for No documents required is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_icon_for_no_documents_required_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Icon for No documents required is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noDocumentIcon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field No documents required is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_no_documents_required_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field No documents required is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noDocumentLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field No credit check Instant approval is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_no_credit_checks_instant_approval_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field No credit check Instant approval is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noDocumentDescription_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field icon for Hassle free process is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_icon_for_hasslefree_process_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field icon for Hassle free process is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.hassleIcon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Hassle free process is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_hasslefree_process_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Hassle free process is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.hassleLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Done in 60 secs is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_done_in_60_secs_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Done in 60 secs is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.hassleDescription_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field icon for No Processing fees is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_icon_for_no_processing_fees_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field icon for No Processing fees is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noProcessingIcon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field No Processing fees is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_no_processing_fees_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field No Processing fees is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noProcessingLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Zero hidden costs is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_zero_hidden_costs_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Zero hidden costs is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.noProcessingDescription_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_get_started_is_getting_displayed_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.get_Started_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen$")
    public void verify_whether_the_required_mandatory_field_get_started_is_getting_navigated_in_alopown_accounts100_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.get_Started_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the Field Down Arrow is display left side of the screen$")
    public void verify_whether_the_field_down_arrow_is_display_left_side_of_the_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the Field Down Arrow is display left side of the screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_quick_cash_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.quickCash_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Amount Currency is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_amount_currency_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Amount Currency is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.amount_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Amount is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_amount_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Amount is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmount_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field MinLoanAmount is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_minloanamount_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field MinLoanAmount is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMin_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field MaxLoanAmount is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_maxloanamount_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field MaxLoanAmount is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMax_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Amount edit button is clickable in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_amount_edit_button_is_clickable_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Amount edit button is clickable in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.editButton_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field amount digit comma is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_amount_digit_comma_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field amount digit comma is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmount_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field down arrow is getting navigated to Dashboard_200 screen from monthly repayment screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_is_getting_navigated_to_dashboard200_screen_from_monthly_repayment_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field down arrow is getting navigated to Dashboard_200 screen from monthly repayment screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmount_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Lowest rate is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_lowest_rate_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Lowest rate is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Monthly repayment value with country curreny code is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_value_with_country_curreny_code_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Monthly repayment value with country curreny code is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Monthly repayment value for is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_value_for_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Monthly repayment value for is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Monthly repayment value with down arrow is getting displayed in ALOP_Own accounts_200 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_value_with_down_arrow_is_getting_displayed_in_alopown_accounts200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Monthly repayment value with down arrow is getting displayed in ALOP_Own accounts_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }



    @And("^Verify whether the required mandatory Field Nominal Interest Rate Value with percentage and p a is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_value_with_percentage_and_pa_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal Interest Rate Value with percentage and p a is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field EIR is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_eir_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field EIR is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field EIR Value Interest Rate from API is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_eir_value_interest_rate_from_api_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field EIR Value Interest Rate from API is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field % is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field % is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field p a is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_pa_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field p a is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Question is getting displayed in ALOP_200 screen$")
    public void verify_whether_the_required_mandatory_field_question_is_getting_displayed_in_alop200_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Question is getting displayed in ALOP_200 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcQuestions_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Question is getting navigated to the questions screen$")
    public void verify_whether_the_required_mandatory_field_question_is_getting_navigated_to_the_questions_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Question is getting navigated to the questions screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcQuestions_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the application Question page to back navigation$")
    public void verify_whether_the_application_question_page_to_back_navigation() throws Throwable {
        ExtentReport.stepName = "Verify whether the application Question page to back navigation";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcQuestions_200_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field next arrow is getting navigated to_400 screen$")
    public void verify_whether_the_required_mandatory_field_next_arrow_is_getting_navigated_to400_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field next arrow is getting navigated to_400 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.next_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the required mandatory Field Down arrow is getting displayed in ALOP_400 screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_is_getting_displayed_in_alop400_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down arrow is getting displayed in ALOP_400 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen$")
    public void verify_whether_the_required_mandatory_field_send_quick_cash_to_is_getting_displayed_in_alop400_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.sendQuickCash_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen$")
    public void verify_whether_the_required_mandatory_field_masked_account_number_is_getting_displayed_in_alop400_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.pt0Account_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }


    @And("^Verify whether the required mandatory Field My account with other banks is getting displayed in ALOP_400 screen$")
    public void verify_whether_the_required_mandatory_field_my_account_with_other_banks_is_getting_displayed_in_alop400_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.myAccountwithOtherBanks_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^Verify whether the required mandatory Field My account with other banks is getting navigated to ALOP_410 screen$")
    public void verify_whether_the_required_mandatory_field_my_account_with_other_banks_is_getting_navigated_to_alop410_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field My account with other banks is getting navigated to ALOP_410 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.myAccountwithOtherBanks_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To Verify whether the required mandatory Field Other Bank accounts is getting displayed in Header$")
    public void to_verify_whether_the_required_mandatory_field_other_bank_accounts_is_getting_displayed_in_header() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Other Bank accounts is getting displayed in Header";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.otherBankAccount_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To verify whether the bank name select button is navigate to the bank name screen as per the condition$")
    public void to_verify_whether_the_bank_name_select_button_is_navigate_to_the_bank_name_screen_as_per_the_condition() throws Throwable {
        ExtentReport.stepName = "To verify whether the bank name select button is navigate to the bank name screen as per the condition";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.bankName_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To Verify whether the required mandatory Field Bank Name header is getting displayed as per the condition$")
    public void to_verify_whether_the_required_mandatory_field_bank_name_header_is_getting_displayed_as_per_the_condition() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Bank Name header is getting displayed as per the condition";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.bankName_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To Verify whether the required mandatory Field list of account name if select PT1 account navigate to 410 screen$")
    public void to_verify_whether_the_required_mandatory_field_list_of_account_name_if_select_pt1_account_navigate_to_410_screen() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field list of account name if select PT1 account navigate to 410 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.pt1Account_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To Verify whether the required mandatory Field list of account name if select PT2 account navigate to 410 screen$")
    public void to_verify_whether_the_required_mandatory_field_list_of_account_name_if_select_pt2_account_navigate_to_410_screen() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field list of account name if select PT2 account navigate to 410 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.pt2Account_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To Verify whether the required mandatory Field Account Number for above account has to be entered in the account number field$")
    public void to_verify_whether_the_required_mandatory_field_account_number_for_above_account_has_to_be_entered_in_the_account_number_field() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Account Number for above account has to be entered in the account number field";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.sendKeysTextBoxMultipleSession(QuickCash_Objects.accountNumber_Android, "5678345678");
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }

    @And("^To verify whether the required mandatory field select bank name and enter account number then visible the next button and navigated to ALOP 600 screen$")
    public void to_verify_whether_the_required_mandatory_field_select_bank_name_and_enter_account_number_then_visible_the_next_button_and_navigated_to_alop_600_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field select bank name and enter account number then visible the next button and navigated to ALOP 600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.next_420_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);		
			}
    }
    
    @And("^Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_own_account_detail_with_checkbox_is_select_navigate_to_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.pt0Account_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the required mandatory Field Down arrow is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down arrow is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_review_confirm_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.reviewAndConfirm_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Loan Details is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_loan_details_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Loan Details is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcLoanDetails_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Review & confirm is getting displayed with specified values in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_review_confirm_is_getting_displayed_with_specified_values_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Review & confirm is getting displayed with specified values in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount is getting displayed as per the condition If ALOP offer product type is Credit Credit based on ALOP_200 screen selected amount in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_is_getting_displayed_as_per_the_condition_if_alop_offer_product_type_is_credit_credit_based_on_alop200_screen_selected_amount_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount is getting displayed as per the condition If ALOP offer product type is Credit Credit based on ALOP_200 screen selected amount in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcLoanAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount with CCY is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_with_ccy_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount with CCY is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcLoanAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount with decimal validated is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_with_decimal_validated_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount with decimal validated is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcLoanAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRP_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field monthly repayment Amount is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_amount_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field monthly repayment Amount is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRPAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field monthly repayment Amount Country currency code is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_amount_country_currency_code_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field monthly repayment Amount Country currency code is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRPAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field monthly repayment value with for is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_monthly_repayment_value_with_for_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field monthly repayment value with for is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcMRPAmount_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Account Details is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_account_details_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Account Details is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAccDetails_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Charge to is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_charge_to_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Charge to is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcChargedTo_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Bank name with product name and account number display with mock is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_bank_name_with_product_name_and_account_number_display_with_mock_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Bank name with product name and account number display with mock is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcChargedtoAcc_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Nominal interest rate Label is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_label_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal interest rate Label is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_value_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field EIR is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_eir_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field EIR is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field EIR Value is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_eir_value_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field EIR Value is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field % is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field % is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field p a is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_pa_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field p a is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNI_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Receiving Quick Cash at is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_receiving_quick_cash_at_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Receiving Quick Cash at is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcReceive_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Source Account Name is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_source_account_name_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Source Account Name is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSelAcc600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Source Account Number is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_source_account_number_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Source Account Number is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSelAcc600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field select account is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_select_account_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field select account is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSelAcc600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field repayment schedule is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_repayment_schedule_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field repayment schedule is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRS_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Tap Get cash now is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_tap_get_cash_now_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Tap Get cash now is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcTap_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field PDF is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_pdf_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field PDF is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcTerms_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Terms and Conditions is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_terms_and_conditions_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Terms and Conditions is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcTerms_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field PDF Terms and conditions is navigate to respective page in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_pdf_terms_and_conditions_is_navigate_to_respective_page_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field PDF Terms and conditions is navigate to respective page in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcTerms_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen$")
    public void verify_whether_the_required_mandatory_field_get_cash_now_is_getting_displayed_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.getCashNow_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^verify whether the application is getting navigated to the required screen repayement schedule on clicking the provided Button navigate to respective page in ALOP_600 screen$")
    public void verify_whether_the_application_is_getting_navigated_to_the_required_screen_repayement_schedule_on_clicking_the_provided_button_navigate_to_respective_page_in_alop600_screen() throws Throwable {
        ExtentReport.stepName = "verify whether the application is getting navigated to the required screen repayement schedule on clicking the provided Button navigate to respective page in ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.qcRS_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the required mandatory Field Down Arrow is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down Arrow is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Repayment Schedule is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_repayment_schedule_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Repayment Schedule is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRS_600_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Repayment Details is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_repayment_details_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Repayment Details is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRPSDetails_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount Label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount Label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmount_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field our Quick Cash amount value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_our_quick_cash_amount_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field our Quick Cash amount value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmountValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount value country currency code is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_value_country_currency_code_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount value country currency code is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmountValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your Quick Cash amount value decimal validation is displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_your_quick_cash_amount_value_decimal_validation_is_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your Quick Cash amount value decimal validation is displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAmountValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First month instalment label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_first_month_instalment_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First month instalment label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFMI_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First month instalment amount value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_first_month_instalment_amount_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First month instalment amount value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First Month instalment amount value with country currency code is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_first_month_instalment_amount_value_with_country_currency_code_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First Month instalment amount value with country currency code is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First month instalment amount double validation is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_first_month_instalment_amount_double_validation_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First month instalment amount double validation is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent month instalment label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_month_instalment_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent month instalment label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMI_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent month instalment amount value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_month_instalment_amount_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent month instalment amount value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent Month instalment amount value with country currency code is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_month_instalment_amount_value_with_country_currency_code_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent Month instalment amount value with country currency code is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent month instalment amount double validation is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_month_instalment_amount_double_validation_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent month instalment amount double validation is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMIV_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Instalment label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_instalment_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Instalment label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcInstalment_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Principle label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_principle_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Principle label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcPrincipal_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field interest label is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_interest_label_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field interest label is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcInterest_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Instalment value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_instalment_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Instalment value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcInstalmentValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Principle value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_principle_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Principle value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcPrincipalValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field interest value is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_interest_value_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field interest value is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcInterestValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Principle amount country currency code is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_principle_amount_country_currency_code_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Principle amount country currency code is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcPrincipalValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field interest amount country currency code is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_interest_amount_country_currency_code_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field interest amount country currency code is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcInterestValue_RPS_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Down arrow Button is getting displayed in repayment schedule screen$")
    public void verify_whether_the_required_mandatory_field_down_arrow_button_is_getting_displayed_in_repayment_schedule_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Down arrow Button is getting displayed in repayment schedule screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
    
    @And("^Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen$")
    public void verify_whether_the_application_is_getting_navigated_to_the_required_screen_alopbooking700_if_success_on_clicking_get_cash_now_from_alop600_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.getCashNow_Android);
			Thread.sleep(30000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Success image is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_success_image_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Success image is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Tick image is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_tick_image_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Tick image is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAllDone_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_all_done_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcAllDone_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_your_quickcash_of_should_be_available_in_your_citibank_account_instantly_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcYourQuickCash_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field View summary label is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_view_summary_label_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field View summary label is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcViewSummary_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field View summary drop down option is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_view_summary_drop_down_option_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field View summary drop down option is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcViewSummary_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First instalment is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_first_instalment_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First instalment is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFirstMI_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First instalment amount is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_first_instalment_amount_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First instalment amount is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFirstMIV_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field First instalment amount country currency code is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_first_instalment_amount_country_currency_code_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field First instalment amount country currency code is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcFirstMIV_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent monthly instalments is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_monthly_instalments_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent monthly instalments is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMI_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent monthly instalments amount is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_monthly_instalments_amount_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent monthly instalments amount is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMIV_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Subsequent monthly instalments amount country currency code is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_subsequent_monthly_instalments_amount_country_currency_code_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Subsequent monthly instalments amount country currency code is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcSMIV_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Repayment period is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_repayment_period_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Repayment period is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRepayPeriod_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Repayment period value is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_repayment_period_value_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Repayment period value is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRPSValue_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNIR_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_nominal_interest_rate_value_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNIRValue_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field % is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field % is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNIRValue_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field p a is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_pa_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field p a is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcNIRValue_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field repayment schedules is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_repayment_schedules_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field repayment schedules is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRPS_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Hide Summary is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_hide_summary_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Hide Summary is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRPS_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Need more Quick cash Up to Amount from your Ready Credit is instantly avaliable is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_need_more_quick_cash_up_to_amount_from_your_ready_credit_is_instantly_avaliable_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Need more Quick cash Up to Amount from your Ready Credit is instantly avaliable is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.qcRPS_700_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Get it now label is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_get_it_now_label_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Get it now label is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen$")
    public void verify_whether_the_required_mandatory_field_done_is_getting_displayed_in_alop700_screen() throws Throwable {
        ExtentReport.stepName = "Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(QuickCash_Objects.done_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }

    @And("^verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button$")
    public void verify_whether_the_application_is_getting_navigated_to_the_required_screen_dashboard100_on_clicking_the_provided_image_or_button() throws Throwable {
        ExtentReport.stepName = "verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(QuickCash_Objects.done_Android);
			Thread.sleep(30000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(QuickCash_Objects.creditCard_Android);
			Thread.sleep(2000);
			}
    }
	

	@After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
