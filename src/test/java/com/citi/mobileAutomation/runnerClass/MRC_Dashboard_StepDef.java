package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class MRC_Dashboard_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Product :MRC Dashboard
	 * Platform:Android
	 * 
	 */

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	 @And("^To verify whether the required mandatory field profile and settings icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_profile_and_settings_icon_is_getting_displayed() throws Throwable {
		 ExtentReport.stepName = "To verify whether the required mandatory field profile and settings icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking profile and settings icon is it navigate to settings page$")
	    public void to_verify_whether_on_clicking_profile_and_settings_icon_is_it_navigate_to_settings_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking profile and settings icon is it navigate to settings page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking close icon is it navigate back to dashboard page$")
	    public void to_verify_whether_on_clicking_close_icon_is_it_navigate_back_to_dashboard_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking close icon is it navigate back to dashboard page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field customer name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_customer_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field customer name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field notifications icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_notifications_icon_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field notifications icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking notifications icon is it navigate to notifications page$")
	    public void to_verify_whether_on_clicking_notifications_icon_is_it_navigate_to_notifications_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking notifications icon is it navigate to notifications page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking close icon in notifications page is it navigate back to dashboard page$")
	    public void to_verify_whether_on_clicking_close_icon_in_notifications_page_is_it_navigate_back_to_dashboard_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking close icon in notifications page is it navigate back to dashboard page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field logout icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_logout_icon_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field logout icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking logout icon is it navigate to logout drawer page$")
	    public void to_verify_whether_on_clicking_logout_icon_is_it_navigate_to_logout_drawer_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking logout icon is it navigate to logout drawer page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking down arrow image is it navigate back to dashboard page$")
	    public void to_verify_whether_on_clicking_down_arrow_image_is_it_navigate_back_to_dashboard_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking down arrow image is it navigate back to dashboard page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field deposit category is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_deposit_category_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field deposit category is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }
	    
	    @And("^To verify whether the required mandatory field deposit category value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_deposit_category_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field deposit category value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryAmount_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking deposit down chevron is it expanded the category$")
	    public void to_verify_whether_on_clicking_deposit_down_chevron_is_it_expanded_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking deposit down chevron is it expanded the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking deposit up chevron is it collapse the category$")
	    public void to_verify_whether_on_clicking_deposit_up_chevron_is_it_collapse_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking deposit up chevron is it collapse the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field checking account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_checking_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field checking account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.checkingsAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field checking account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_checking_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field checking account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.checkingsAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field checking account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_checking_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field checking account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.checkingsAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field checking account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_checking_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field checking account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.checkingsAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field checking account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_checking_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field checking account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.checkingsRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field savings account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_savings_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field savings account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.savingsAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field savings account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_savings_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field savings account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.savingsAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field savings account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_savings_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field savings account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.savingsAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field savings account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_savings_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field savings account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.savingsAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field savings account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_savings_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field savings account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.savingsRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field globalwallet title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_globalwallet_title_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field globalwallet title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field globalwallet description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_globalwallet_description_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field globalwallet description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field globalwallet right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_globalwallet_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field globalwallet right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field globalwallet enrolled description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_globalwallet_enrolled_description_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field globalwallet enrolled description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field globalwallet enrolled add button is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_globalwallet_enrolled_add_button_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field globalwallet enrolled add button is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field time deposit account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_time_deposit_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field time deposit account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.timedepositAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field time deposit account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_time_deposit_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field time deposit account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.timedepositAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field time deposit account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_time_deposit_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field time deposit account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.timedepositAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field time deposit account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_time_deposit_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field time deposit account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.timedepositAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field time deposit account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_time_deposit_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field time deposit account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.timedepositRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit cards category is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_cards_category_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit cards category is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit cards category value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_cards_category_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit cards category value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardCategoryAmount_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    
	    @And("^To verify whether on clicking credit cards down chevron is it expanded the category$")
	    public void to_verify_whether_on_clicking_credit_cards_down_chevron_is_it_expanded_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking credit cards down chevron is it expanded the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.creditCardCategoryDownChevron_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking credit cards up chevron is it collapse the category$")
	    public void to_verify_whether_on_clicking_credit_cards_up_chevron_is_it_collapse_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking credit cards up chevron is it collapse the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.creditCardCategoryDownChevron_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit card name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_card_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit card name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit card number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_card_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit card number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit card balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_card_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit card balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit card balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_card_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit card balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field credit card right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_credit_card_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field credit card right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field citi payall title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_citi_payall_title_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field citi payall title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardPayAllTitle);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field citi payall description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_citi_payall_description_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field citi payall description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardPayAllDesc);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field citi payall right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_citi_payall_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field citi payall right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.creditCardPayAllRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field investments category is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_investments_category_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field investment category is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.investmentsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }
	    
	    @And("^To verify whether the required mandatory field investments category value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_investments_category_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field investment category value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.investmentsCategoryAmount_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on investments down chevron is it expanded the category$")
	    public void to_verify_whether_on_investments_down_chevron_is_it_expanded_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on investment down chevron is it expanded the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.investmentsCategoryDownChevron_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on investments up chevron is it collapse the category$")
	    public void to_verify_whether_on_investments_up_chevron_is_it_collapse_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on investment up chevron is it collapse the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.investmentsCategoryDownChevron_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field premium deposit account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_premium_deposit_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field premium deposit account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field premium deposit account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_premium_deposit_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field premium deposit account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field premium deposit account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_premium_deposit_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field premium deposit account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field premium deposit account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_premium_deposit_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field premium deposit account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field premium deposit account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_premium_deposit_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field premium deposit account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field gold account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gold_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field gold account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field gold account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gold_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field gold account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field gold account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gold_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field gold account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field gold account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gold_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field gold account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field gold account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gold_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field gold account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mutualfund account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mutualfund_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mutualfund account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.mfAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mutualfund account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mutualfund_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mutualfund account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.mfAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mutualfund account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mutualfund_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mutualfund account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.mfAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mutualfund account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mutualfund_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mutualfund account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.mfAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mutualfund account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mutualfund_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mutualfund account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.mfightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field brokerage account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_brokerage_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field brokerage account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field brokerage account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_brokerage_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field brokerage account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field brokerage account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_brokerage_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field brokerage account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field brokerage account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_brokerage_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field brokerage account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field brokerage account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_brokerage_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field brokerage account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field equity account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_equity_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field equity account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field equity account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_equity_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field equity account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field equity account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_equity_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field equity account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field equity account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_equity_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field equity account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field equity account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_equity_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field equity account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field bonds account is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_bonds_account_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field bonds account is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.bondsAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field bonds account number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_bonds_account_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field bonds account number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.bondsAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field bonds account balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_bonds_account_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field bonds account balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.bondsAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field bonds account balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_bonds_account_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field bonds account balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.bondsAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field bonds account right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_bonds_account_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field bonds account right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.bondsRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field loan category is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_loan_category_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field loan category is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.loansCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }
	    
	    @And("^To verify whether the required mandatory field loan category value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_loan_category_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field loan category value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.loansCategoryAmount_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on loans down chevron is it expanded the category$")
	    public void to_verify_whether_on_loans_down_chevron_is_it_expanded_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on loans down chevron is it expanded the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.loansCategoryDownChevron_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on loans up chevron is it collapse the category$")
	    public void to_verify_whether_on_loans_up_chevron_is_it_collapse_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on loans up chevron is it collapse the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field readycreditloans name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_readycreditloans_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field readycreditloans name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field readycreditloans number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_readycreditloans_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field readycreditloans number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field readycreditloans balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_readycreditloans_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field readycreditloans balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field readycreditloans balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_readycreditloans_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field readycreditloans balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field readycreditloans right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_readycreditloans_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field readycreditloans right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field personalloans name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_personalloans_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field personalloans name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field personalloans number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_personalloans_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field personalloans number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field personalloans balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_personalloans_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field personalloans balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field personalloans balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_personalloans_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field personalloans balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field personalloans right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_personalloans_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field personalloans right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mortgage name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mortgage_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mortgage name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.housingLoanAccountName_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mortgage number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mortgage_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mortgage number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.housingLoanAccountNumber_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mortgage balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mortgage_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mortgage balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.housingLoanAccountBalanceCCY_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mortgage balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mortgage_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mortgage balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.housingLoanAccountBalanceValue_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field mortgage right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_mortgage_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field mortgage right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.housingLoanRightChevron);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }
	    

	    @And("^To verify whether the required mandatory field insurances category is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_category_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances category is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on insurances down chevron is it expanded the category$")
	    public void to_verify_whether_on_insurances_down_chevron_is_it_expanded_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on insurances down chevron is it expanded the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on insurances up chevron is it collapse the category$")
	    public void to_verify_whether_on_insurances_up_chevron_is_it_collapse_the_category() throws Throwable {
	        ExtentReport.stepName = "To verify whether on insurances up chevron is it collapse the category";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances name is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_name_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances name is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances number is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_number_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances number is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances balance ccy is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_balance_ccy_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances balance ccy is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances balance value is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_balance_value_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances balance value is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances policy name and type is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_policy_name_and_type_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances policy name and type is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances sum insured is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_sum_insured_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances sum insured is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances coverage is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_coverage_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances coverage is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field insurances right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_insurances_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field insurances right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field EMGM Offer title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_emgm_offer_title_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field EMGM Offer title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field EMGM Offer description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_emgm_offer_description_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field EMGM Offer description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field EMGM Offer right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_emgm_offer_right_chevron_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field EMGM Offer right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field action tile offers or rewards is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_action_tile_offers_or_rewards_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field action tile offers or rewards is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field action tile invest or wealth is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_action_tile_invest_or_wealth_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field action tile invest or wealth is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field action tile payment menu is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_action_tile_payment_menu_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field action tile payment menu is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field action tile get more is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_action_tile_get_more_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field action tile get more is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field action tile help is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_action_tile_help_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field action tile help is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(MRC_Dashboard_Objects.depositsCategoryLabel_And);
			Thread.sleep(2000);
			}
	    }
	

		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
