package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.Positive_Friction_Banner_Objects;
import com.citi.mobileAutomation.objectRepository.GMSunsetObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Positive_Friction_Product_Palette_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	

	/*
	 * @Author:Dilipkumar
	 * Product :Positive Friction Banner - Product Palette
	 * Platform:Android
	 * 
	 */
	
	 @And("^To verify whether the required mandatory field get more in footer menu is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_get_more_in_footer_menu_is_getting_displayed() throws Throwable {
		 ExtentReport.stepName = "To verify whether the required mandatory field get more in footer menu is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking on get more in footer menu it is navigating to get more offer screen page$")
	    public void to_verify_whether_on_clicking_on_get_more_in_footer_menu_it_is_navigating_to_get_more_offer_screen_page() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking on get more in footer menu it is navigating to get more offer screen page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(15000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette price tag icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_price_tag_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette price tag icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppPriceTagIcon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette header title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_header_title_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette header title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppPaleteeTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category1 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category1_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category1 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory1Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category1 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category1_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category1 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory1Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category2 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category2_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category2 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory2Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category2 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category2_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category2 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory2Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category3 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category3_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category3 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory3Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category3 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category3_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category3 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory3Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }
	    
	    @And("^To verify whether the required mandatory field product palette category4 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category4_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category4 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory4Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category4 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category4_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category4 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory4Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category5 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category5_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category5 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory5Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category5 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category5_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category5 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory5Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category6 icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category6_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category6 icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory6Icon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette category6 label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_category6_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette category6 label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppCategory6Label_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette view all product label is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_view_all_product_label_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette view all product label is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppViewAllProductLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking on view all product label it is navigating to webview page$")
	    public void to_verify_whether_on_clicking_on_view_all_product_label_it_is_navigating_to_webview_page() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking on view all product label it is navigating to webview page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.gmppViewAllProductLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field product palette view all product webview title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_product_palette_view_all_product_webview_title_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field product palette view all product webview title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppWebviewTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking on webview back button it is navigating to get more offer screen page$")
	    public void to_verify_whether_on_clicking_on_webview_back_button_it_is_navigating_to_get_more_offer_screen_page() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking on webview back button it is navigating to get more offer screen page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.gmppWVBack_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	/*---------------------------------------------------------Share with friends--------------------------------------------------*/
	   	    
	    
	    @And("^To verify whether the required mandatory field share with friends icon is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_share_with_friends_icon_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field share with friends icon is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.swfIcon_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field share with friends title is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_share_with_friends_title_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field share with friends title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.swfTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field share with friends description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_share_with_friends_description_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field share with friends description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.swfDescription_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field share with friends right chevron is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_share_with_friends_right_chevron_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field share with friends right chevron is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.swfRightChevron_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking share with friends right chevron it is navigating to mobile share drawer$")
	    public void to_verify_whether_on_clicking_on_share_with_friends_right_chevron_it_is_navigating_to_mobile_share_drawer() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking share with friends right chevron it is navigating to mobile share drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.swfTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }
	    
	    
		/*--------------------------------------------End of Share with friends--------------------------------------------------*/
	    
	    /*--------------------------------------------Add to Supplementary cards-------------------------------------------------*/
	    
	    @And("^To verify whether the required mandatory field activate your replacement card is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_activate_your_replacemnt_card_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field activate your replacement card is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscActReplaceCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking activate your replacement card it is navigating to replacement card activation drawer$")
	    public void to_verify_whether_on_clicking_activate_your_replacemnt_card_it_is_navigating_to_otp_screen_page() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking activate your replacement card it is navigating to replacement card activation drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.atscActReplaceCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field yes button in replacement card activation drawer is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_yes_button_in_replacement_card_activation_drawer_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field yes button in replacement card activation drawer is getting displayed";
	  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	  			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscYes_Android);
	  			Thread.sleep(5000);
	  			}
	  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	  			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
	  			Thread.sleep(2000);
	  			}
	    }

	    @And("^To verify whether on clicking yes button in replacement card activation drawer it is navigating to OTP screen page$")
	    public void to_verify_whether_on_clicking_yes_button_in_replacement_card_activation_drawer_it_is_navigating_to_otp_screen_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking yes button in replacement card activation drawer it is navigating to OTP screen page";
	  			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	  			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.atscYes_Android);
	  			Thread.sleep(30000);
	  			}
	  			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	  			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
	  			Thread.sleep(2000);
	  			}
	    }
	    
	    @And("^To verify whether the required mandatory field Enter CVV number drawer is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_enter_cvv_number_drawer_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field Enter CVV number drawer is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscEnterCVVLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the entering the cvv value in Enter CVV field$")
	    public void to_verify_whether_the_entering_the_cvv_value_in_enter_cvv_field() throws Throwable {
	         ExtentReport.stepName = "To verify whether the entering the cvv value in Enter CVV field";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.sendKeysTextBoxMultipleSession(Positive_Friction_Banner_Objects.atscCVV_Android,"865");
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field activate button is enabled is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_activate_button_is_enabled_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field activate button is enabled is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscActivate_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking activate button it is navigating to card activation drawer$")
	    public void to_verify_whether_on_clicking_activate_button_it_is_navigating_to_card_activation_drawer() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking activate button it is navigating to card activation drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.atscActivate_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field your card is activated is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_your_card_is_activated_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field your card is activated is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscYourCardLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field add to suppplementaty card header is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_add_to_suppplementaty_card_header_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field add to suppplementaty card header is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscAddSuppCardLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field add to suppplementaty card description is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_add_to_suppplementaty_card_description_is_getting_displayed() throws Throwable {
	         ExtentReport.stepName = "To verify whether the required mandatory field add to suppplementaty card description is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.atscAddSuppCardDescrip_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking add to suppplementaty card it is navigating to STO navigation screen page$")
	    public void to_verify_whether_on_clicking_add_to_suppplementaty_card_it_is_navigating_to_sto_navigation_screen_page() throws Throwable {
	         ExtentReport.stepName = "To verify whether on clicking add to suppplementaty card it is navigating to STO navigation screen page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.atscAddSuppCardLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.gmppGetMore_Android);
			Thread.sleep(2000);
			}
	    }
	    
	    
	    
	    /*--------------------------------------------End of Add to Supplementary cards--------------------------------------------------*/
	
		    @After
			public void updateExcelStatus(Scenario scenario) throws FilloException {

				ExtentReport.updateExcelStatus(scenario);

			}

			@AfterStep
			public static void getResult() throws Exception {

				ExtentReport.getResult();

			}

}
