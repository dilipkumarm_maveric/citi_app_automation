package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.Positive_Friction_Banner_Objects;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Positive_Friction_CardsOnboarding_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	

	/*
	 * @Author:Dilipkumar
	 * Product :Positive Friction Banner - Cards Onboarding - Next Step,Cards Delivery Status Tracker
	 * Domain :Grow
	 * Platform:Android
	 * 
	 */
	
    /*-------------------------------------------------Cards Onboarding --------------------------------------------------------------------------------------------------*/
    
    
    @And("^To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_on_credit_cards_category_which_is_expanding_screen_for_cards_onboarding_next_step() throws Throwable {
     	ExtentReport.stepName = "To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepCardsCategory_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
        }
    	

    @And("^To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_on_credit_cards_right_chevron_which_is_navigating_screen_to_cards_ada_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepCCcardNo_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_view_card_benefits_thumbnail_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepViewCardBenefits_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_view_card_benefits_label_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepViewCardBenefits_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_add_to_apple_wallet_thumbnail_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepAppleWallet_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_add_to_apple_wallet_label_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepAppleWallet_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_pay_bills_thumbnail_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepPayall_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_next_step_pay_bills_label_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepPayall_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking CVP CTA Action pointed to offer label which is navigating to CVP Screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_cvp_cta_action_pointed_to_offer_label_which_is_navigating_to_cvp_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking CVP CTA Action pointed to Delight Box offer label which is navigating to CVP Screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepViewCardBenefits_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Next Step$")
    public void to_verify_whether_the_required_mandatory_field_expected_cvp_screen_offer_title_is_getting_displayed_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepCVPTitle_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_back_button_in_cvp_screen_which_is_navigate_back_to_dashboard_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepCVPRemindCTA_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking STP Journey CTA Action pointed to offer label which is navigating to expected STP Journey Screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_stp_journey_cta_action_pointed_to_offer_label_which_is_navigating_to_expected_stp_journey_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STP Journey CTA Action pointed to offer label which is navigating to expected STP Journey Screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepPayall_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether the required mandatory field expected STP screen offer title is getting displayed for Cards Onboarding Next Step$")
    public void to_verify_whether_the_required_mandatory_field_expected_stp_screen_offer_title_is_getting_displayed_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected STP screen offer title is getting displayed for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.nextStepSTPTitle_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking back button in STP screen which is navigate back to dashboard screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_back_button_in_stp_screen_which_is_navigate_back_to_dashboard_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in STP screen which is navigate back to dashboard screen for Cards Onboarding Next Step";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepXCTA_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }
    
    @And("^To verify whether on clicking STO Journey CTA Action pointed to offer label which is navigating to expected STO Journey Screen for Cards Onboarding Next Step$")
    public void to_verify_whether_on_clicking_sto_journey_cta_action_pointed_to_offer_label_which_is_navigating_to_expected_sto_journey_screen_for_cards_onboarding_next_step() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STO Journey CTA Action pointed to offer label which is navigating to expected STO Journey Screen for Cards Onboarding Next Step";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepViewCardBenefits_Android);
		Thread.sleep(5000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
		Thread.sleep(2000);
		}
    }
    
    @And("^To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen$")
    public void to_verify_whether_on_clicking_webview_cta_action_pointed_to_cards_onboarding_next_step_view_card_benefits_label_which_is_navigating_to_expected_webview_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.nextStepViewCardBenefits_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }
    
    @And("^To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_card_delivery_tracker_description_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerDesc_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_card_delivery_tracker_need_help_label_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerNeedHelpCTALabel_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_card_delivery_tracker_card_printed_label_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerCP_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_card_delivery_tracker_card_dispatched_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerCD_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cards_onboarding_card_delivery_tracker_card_arrived_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerCA_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking STP Journey CTA Action pointed to need help label which is navigating to expected STP Journey Screen for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_on_clicking_stp_journey_cta_action_pointed_to_need_help_label_which_is_navigating_to_expected_stp_journey_screen_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STP Journey CTA Action pointed to need help label which is navigating to expected STP Journey Screen for Cards Onboarding Card Delivery Tracker";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.cardTrackerNeedHelpCTALabel_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether the required mandatory field expected STP screen offer title is getting displayed for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_the_required_mandatory_field_expected_stp_screen_offer_title_is_getting_displayed_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected STP screen offer title is getting displayed for Cards Onboarding Card Delivery Tracker";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerSTPTitle_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }

    @And("^To verify whether on clicking back button in STP screen which is navigate back to dashboard screen for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_on_clicking_back_button_in_stp_screen_which_is_navigate_back_to_dashboard_screen_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in STP screen which is navigate back to dashboard screen for Cards Onboarding Card Delivery Tracker";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerSTPTitle_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}		
    }
    
    @And("^To verify whether on clicking STO Journey CTA Action pointed to need help label which is navigating to expected STO Journey Screen for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_on_clicking_sto_journey_cta_action_pointed_to_need_help_label_which_is_navigating_to_expected_sto_journey_screen_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STO Journey CTA Action pointed to need help label which is navigating to expected STO Journey Screen for Cards Onboarding Card Delivery Tracker";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.cardTrackerNeedHelpCTALabel_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }
    
    @And("^To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen$")
    public void to_verify_whether_on_clicking_webview_cta_action_pointed_to_cards_card_delivery_tracker_need_help_label_which_is_navigating_to_expected_webview_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen";
    		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.cardTrackerNeedHelpCTALabel_Android);
    		Thread.sleep(5000);
    		}
    		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    		Thread.sleep(2000);
    		}
    }
    

    @And("^To verify whether on clicking CVP CTA Action pointed to offer label which is navigating to CVP Screen for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_on_clicking_cvp_cta_action_pointed_to_offer_label_which_is_navigating_to_cvp_screen_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking CVP CTA Action pointed to offer label which is navigating to CVP Screen for Cards Onboarding Card Delivery Tracker";
      		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
      		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.cardTrackerNeedHelpCTALabel_Android);
      		Thread.sleep(5000);
      		}
      		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
      		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
      		Thread.sleep(2000);
      		}
    }

    @And("^To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_the_required_mandatory_field_expected_cvp_screen_offer_title_is_getting_displayed_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Card Delivery Tracker";
      		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
      		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.cardTrackerCVPTitle_Android);
      		Thread.sleep(5000);
      		}
      		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
      		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
      		Thread.sleep(2000);
      		}
    }

    @And("^To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Card Delivery Tracker$")
    public void to_verify_whether_on_clicking_back_button_in_cvp_screen_which_is_navigate_back_to_dashboard_screen_for_cards_onboarding_card_delivery_tracker() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Card Delivery Tracker";
      		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
      		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.cardTrackerCVPDismissCTA_Android);
      		Thread.sleep(5000);
      		}
      		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
      		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
      		Thread.sleep(2000);
      		}
    }
    
    
    /*-------------------------------------------------End of Cards Onboarding --------------------------------------------------------------------------------------------------*/

		    @After
			public void updateExcelStatus(Scenario scenario) throws FilloException {

				ExtentReport.updateExcelStatus(scenario);

			}

			@AfterStep
			public static void getResult() throws Exception {

				ExtentReport.getResult();

			}

}
