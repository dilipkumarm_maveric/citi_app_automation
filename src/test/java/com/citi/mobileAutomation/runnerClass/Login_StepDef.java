package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.objectRepository.LoginObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Login_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	/*
	 * @Author:Dilipkumar 
	 * Platform:Android
	 * 
	 */
	
	
	@Given("^Login with username and password$")
	
	public void loginwithmrccustomer() throws Throwable {
			
			OpeningAppiumServer.startAppiumServer();		
			ExtentReport.stepName = "Login with username and password";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				if(CommonMethods.isElementPresentCondition(LoginObjects.coutinue_Android)) {				
					CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
					Thread.sleep(5000);
					if(CommonMethods.isElementPresentCondition(LoginObjects.english_Android)) {	
						CommonMethods.clickMethodMultipleSession(LoginObjects.english_Android);
						Thread.sleep(5000);
						if(CommonMethods.isElementPresentCondition(LoginObjects.accept_Android)){
							CommonMethods.clickMethodMultipleSession(LoginObjects.accept_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}
						else{
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}						
					}
					else {
						if(CommonMethods.isElementPresentCondition(LoginObjects.accept_Android))
						{
							CommonMethods.clickMethodMultipleSession(LoginObjects.accept_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}
						else{
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}		
					}
				}				
				else {		
					if(CommonMethods.isElementPresentCondition(LoginObjects.english_Android)) {
						CommonMethods.clickMethodMultipleSession(LoginObjects.english_Android);
						Thread.sleep(5000);
						if(CommonMethods.isElementPresentCondition(LoginObjects.accept_Android)){
							CommonMethods.clickMethodMultipleSession(LoginObjects.accept_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
							}
						else{
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}
					}
					else {
						if(CommonMethods.isElementPresentCondition(LoginObjects.accept_Android))
						{
							CommonMethods.clickMethodMultipleSession(LoginObjects.accept_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}
						else{
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_Android);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.userID);
							CommonMethods.sendKeysTextBox(LoginObjects.username_Android, Xls_Reader.userID);
							Thread.sleep(5000);
							System.out.println(Xls_Reader.password);
							CommonMethods.sendKeysTextBox(LoginObjects.password_Android, Xls_Reader.password);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_Android);
							Thread.sleep(5000);
							CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_Android);
							Thread.sleep(5000);				
							if(CommonMethods.isElementPresentCondition(LoginObjects.enableButton_Android)) 
							{
								CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_Android);
								Thread.sleep(5000);							
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
								
							}
							else {				
								if(CommonMethods.isElementPresentCondition(LoginObjects.alert_Ok_Btn_Android))
								{
									CommonMethods.clickMethodMultipleSession(LoginObjects.alert_Ok_Btn_Android);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(20000);
								}
								else {
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.tapMethod(529,547);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_Android);
									Thread.sleep(5000);
									CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_Android);
									Thread.sleep(30000);	
								}
							}
						}
					}
				}				
			} 
			else 
			{
				Thread.sleep(10000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.english_iOS);
				Thread.sleep(3000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_iOS);
				Thread.sleep(3000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.loginwithExistAccount_iOS);
				CommonMethods.sendKeysTextBox(LoginObjects.username_iOS, Xls_Reader.userID);
				Thread.sleep(5000);
				CommonMethods.sendKeysTextBox(LoginObjects.password_iOS, Xls_Reader.password);
				Thread.sleep(10000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.loginButton_iOS);
				Thread.sleep(10000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.getStarted_iOS);
				Thread.sleep(10000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.enableButton_iOS);
				Thread.sleep(5000);
				CommonMethods.tapMethod(200,187);
				Thread.sleep(5000);
				CommonMethods.tapMethod(200,187);
				Thread.sleep(5000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.experienceCitiMobile_iOS);
				Thread.sleep(10000);
				CommonMethods.clickMethodMultipleSession(LoginObjects.coutinue_iOS);
				Thread.sleep(30000);
			}		
	}
	
	
	@Given("^Close the application$")
	public void user_click_on_logout_button() throws Throwable {
		//OpeningAppiumServer.stopAppiumServer();

		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			InvokingEmulator.stopAvd();
		} else {
			InvokingEmulator.stopSimulator();
		}

	}

	@After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
