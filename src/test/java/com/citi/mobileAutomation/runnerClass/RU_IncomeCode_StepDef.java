package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.RU_IncomeCodeObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class RU_IncomeCode_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Product :RU PT2 Payment - Income code type field
	 * Platform:Android
	 * 
	 */

	
	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	 @And("^To verify whether the required mandatory field Payments in footer menu for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_payments_in_footer_menu_for_ru_income_code_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field Payments in footer menu for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking Payments in footer menu for RU Income code is navigate to payments page$")
	    public void to_verify_whether_on_clicking_payments_in_footer_menu_for_ru_income_code_is_navigate_to_payments_page() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking Payments in footer menu for RU Income code is navigate to payments page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field By requisites in payments drawer for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_by_requisites_in_payments_drawer_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field By requisites in payments drawer for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_Byrequisties);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking By requisites in payments drawer for RU Income code is navigate to transfer to page$")
	    public void to_verify_whether_on_clicking_by_requisites_in_payments_drawer_for_ru_income_code_is_navigate_to_transfer_to_page() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking By requisites in payments drawer for RU Income code is navigate to transfer to page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_Byrequisties);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field PT2 payee for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_pt2_payee_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field PT2 payee for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PT2);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking PT2 payee for RU Income code is navigate to amount entry screen$")
	    public void to_verify_whether_on_clicking_pt2_payee_for_ru_income_code_is_navigate_to_amount_entry_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking PT2 payee for RU Income code is navigate to amount entry screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_PT2);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering the amount value in amount field for RU Income code$")
	    public void to_verify_whether_the_entering_the_amount_value_in_amount_field_for_ru_income_code() throws Throwable {
	          ExtentReport.stepName = "To verify whether the entering the amount value in amount field for RU Income code";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(RU_IncomeCodeObjects.ruic_Amount200,"56.00");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field next button for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_next_button_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field next button for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_Next200);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking next button for RU Income code is navigate to review and confirm screen$")
	    public void to_verify_whether_on_clicking_next_button_for_ru_income_code_is_navigate_to_review_and_confirm_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking next button for RU Income code is navigate to review and confirm screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_Next200);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Income type code label for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_type_code_label_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field Income type code label for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_IncomeCodeLabel);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Select button for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_select_button_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field Select button for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_SelectBtn);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking Select button for RU Income code is navigate to choose income type screen$")
	    public void to_verify_whether_on_clicking_select_button_for_ru_income_code_is_navigate_to_choose_income_type_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking Select button for RU Income code is navigate to choose income type screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_SelectBtn);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field income code value1 for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_code_value1_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field income code value1 for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_ICValue1);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field income code value2 for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_code_value2_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field income code value2 for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_ICValue2);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field income code value3 for RU Income code fast down time is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_code_value3_for_ru_income_code_fast_down_time_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field income code value3 for RU Income code fast down time is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_ICValue3);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking income code value1 for RU Income code is navigate back to review and confirm screen$")
	    public void to_verify_whether_on_clicking_income_code_value1_for_ru_income_code_is_navigate_back_to_review_and_confirm_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking income code value1 for RU Income code is navigate back to review and confirm screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_ICValue2);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking pay button is navigate to payment confirmation screen$")
	    public void to_verify_whether_on_clicking_pay_button_is_navigate_to_payment_confirmation_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking pay button is navigate to payment confirmation screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruic_PayBtn);
				Thread.sleep(20000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Income type code label for RU Income code in confirmation screen is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_type_code_label_for_ru_income_code_in_confirmation_screen_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field Income type code label for RU Income code in confirmation screen is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_IncomeLabel700);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field Income type code value for RU Income code in confirmation screen is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_income_type_code_value_for_ru_income_code_in_confirmation_screen_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field Income type code value for RU Income code in confirmation screen is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_IncomeValue700);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }
	    
	    @And("^To verify whether the required mandatory field New Payee for RU Income code in Transfer to screen is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_new_payee_for_ru_income_code_in_transfer_to_screen_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field New Payee for RU Income code in Transfer to screen is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruicAdhoc_NewPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking New Payee for RU Income code in Transfer to screen is navigate to transfer to New Payee page$")
	    public void to_verify_whether_on_clicking_new_payee_for_ru_income_code_in_transfer_to_screen_is_navigate_to_transfer_to_new_payee_page() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking New Payee for RU Income code in Transfer to screen is navigate to transfer to New Payee page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_NewPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field New Payee Header for RU Income code is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_new_payee_header_for_ru_income_code_is_getting_displayed() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field New Payee Header for RU Income code is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering account number for RU Income code in Account number field$")
	    public void to_verify_whether_the_entering_account_number_for_ru_income_code_in_account_number_field() throws Throwable {
	          ExtentReport.stepName = "To verify whether the entering account number for RU Income code in Account number field";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_AccNo,"56656585565565675756");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking Bank field in New Payee screen for RU Income code is navigate to transfer to Recipient bank page$")
	    public void to_verify_whether_on_clicking_bank_field_in_new_payee_screen_for_ru_income_code_is_navigate_to_transfer_to_recipient_bank_page() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking Bank field in New Payee screen for RU Income code is navigate to transfer to Recipient bank page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_BankName);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking selected bank for RU Income code is navigate back to transfer to New Payee page$")
	    public void to_verify_whether_on_clicking_selected_bank_for_ru_income_code_is_navigate_back_to_transfer_to_new_payee_page() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking selected bank for RU Income code is navigate back to transfer to New Payee page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_SelectBankPT2);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering the beneficiary name in Name field for RU Income code$")
	    public void to_verify_whether_the_entering_the_beneficiary_name_in_name_field_for_ru_income_code() throws Throwable {
	          ExtentReport.stepName = "To verify whether the entering the beneficiary name in Name field for RU Income code";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_BeneficiaryName,"Jane");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking next arrow for RU Income code is navigate back to transfer to review and confirm screen$")
	    public void to_verify_whether_on_clicking_next_arrow_for_ru_income_code_is_navigate_back_to_transfer_to_review_and_confirm_screen() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking next arrow for RU Income code is navigate back to transfer to review and confirm screen";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_NextArrowPayee);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the entering the amount value in amount field in adhoc flow for RU Income code$")
	    public void to_verify_whether_the_entering_the_amount_value_in_amount_field_in_adhoc_flow_for_ru_income_code() throws Throwable {
	          ExtentReport.stepName = "To verify whether the entering the amount value in amount field in adhoc flow for RU Income code";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.sendKeysTextBoxMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_Amount200,"76.00");
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether the required mandatory field next button is getting displayed in adhoc flow for RU Income code$")
	    public void to_verify_whether_the_required_mandatory_field_next_button_is_getting_displayed_in_adhoc_flow_for_ru_income_code() throws Throwable {
	          ExtentReport.stepName = "To verify whether the required mandatory field next button is getting displayed in adhoc flow for RU Income code";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruicAdhoc_Next200);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow for RU Income code$")
	    public void to_verify_whether_on_clicking_next_button_is_navigate_to_review_and_confirm_screen_in_adhoc_flow_for_ru_income_code() throws Throwable {
	          ExtentReport.stepName = "To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow for RU Income code";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(RU_IncomeCodeObjects.ruicAdhoc_Next200);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(RU_IncomeCodeObjects.ruic_PaymentsMenu);
				Thread.sleep(2000);
				}
	    }
	    
	    
	
		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
