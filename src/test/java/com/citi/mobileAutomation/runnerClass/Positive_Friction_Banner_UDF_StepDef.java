package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.Positive_Friction_Banner_Objects;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Positive_Friction_Banner_UDF_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	/*
	 * @Author:Dilipkumar
	 * Product :Positive Friction Banner
	 * Platform:Android & iOS
	 * 
	 */	

	//---------------------------------Cards -------------------------------------------------------------------------------------------------------------------//

	@And("^To verify whether the required mandatory field credit card category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_credit_card_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field credit card category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for credit card category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_credit_card_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for credit card category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategorySpeaklabel_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on credit card category to expand to show all cards products$")
    public void to_verify_whether_on_clicking_on_credit_card_category_to_expand_to_show_all_cards_products() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on credit card category to expand to show all cards products";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for credit card product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_credit_card_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for credit card product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategorySpeaklabel_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button under credit card product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_under_credit_card_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button under credit card product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategorySpeaklabelActionButton_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    @And("^To verify whether the required mandatory field todo widget title in cards category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_title_in_cards_category_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget title in cards category is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsCategoryToDoMRCTitle_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    }
    }

    @And("^To verify whether the required mandatory field todo widget description in cards category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_description_in_cards_category_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget description in cards category is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsCategoryToDoMRCDescription_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    }
    }

    @And("^To verify whether the required mandatory field todo widget action button under credit card product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_action_button_under_credit_card_product_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget action button under credit card product is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardDashboardToDoMRCActionButton_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    }
    }
    
    @And("^To verify whether the required mandatory field sp upsell banner offer thumbnail in cards category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_thumbnail_in_cards_category_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer thumbnail in cards category is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsCategorySPThumbnail_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    }
    }
    
    @And("^To verify whether the required mandatory field sp upsell banner offer title in cards category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_cards_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in cards category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsCategorySPTitle_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in cards category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_cards_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in cards category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsCategorySPDescription_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on the selected credit card which has speak label is navigating to the cards dashboard$")
    public void to_verify_whether_on_clicking_on_the_selected_credit_card_which_has_speak_label_is_navigating_to_the_cards_dashboard() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on the selected credit card which has speak label is navigating to the cards dashboard";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerCardCategoryClick_And);
	Thread.sleep(10000);
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerCardCategoryClick_And);
	Thread.sleep(10000);
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerCardCategoryClick_And);
	Thread.sleep(10000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for credit card in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_credit_card_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for credit card in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardDashboardSpeaklabel_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardDashboardSpeaklabelActionButton_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field todo widget title in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_title_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget title in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsToDoTitle_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }

    @And("^To verify whether the required mandatory field todo widget description in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_description_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget description in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsToDoDescriptionAndroid);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }
    
    @And("^To verify whether the required mandatory field todo widget action button in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_todo_widget_action_button_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field todo widget action button in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardDashboardToDoActionButton_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer thumbnail in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_thumbnail_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer thumbnail in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsSPThumbnail_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }
    
    @And("^To verify whether the required mandatory field sp upsell banner offer title in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsSPTitle_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsSPDescriptionAndroid);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer thumbnail in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_thumbnail_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer thumbnail in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsTJThumbnail_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }
    
    
    @And("^To verify whether the required mandatory field tj upsell banner offer title in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_title_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer title in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsTJTitle_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer description in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_description_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer description in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsTJDescription_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer thumbnail in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_thumbnail_in_cards_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer thumbnail in cards dashboard is getting displayed";
    	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsXSellThumbnail_Android);
    	Thread.sleep(5000);
    	}
    	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
    	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
    	Thread.sleep(2000);
    	}
    }
    
    @And("^To verify whether the required mandatory field cross sell banner offer title in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsXSellTitle_Android);
	Thread.sleep(5000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in cards dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_cards_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in cards dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardsXSellDescription_Android);
	Thread.sleep(10000);
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerBack_Android);
	Thread.sleep(10000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
	
	//---------------------------------End of Cards --------------------------------------------------------------------------------------------//	    
	
    //--------------------------------Loans ----------------------------------------------------------------------------------------------------//
    
    @And("^To verify whether the required mandatory field loans category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_loans_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field loans category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategory_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for loans category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_loans_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for loans category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategorySpeaklabel_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on loans category to expand to show all loans products$")
    public void to_verify_whether_on_clicking_on_loans_category_to_expand_to_show_all_loans_products() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on loans category to expand to show all loans products";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerLoansCategory_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for loans product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_loans_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for loans product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategorySpeaklabel_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button under loans product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_under_loans_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button under loans product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategorySpeaklabelActionButton_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

   

    @And("^To verify whether the required mandatory field sp upsell banner offer title in loans category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_loans_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in loans category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategorySPTitle_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in loans category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_loans_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in loans category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansCategorySPDescription_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on the selected loans which has speak label is navigating to the loans dashboard$")
    public void to_verify_whether_on_clicking_on_the_selected_loans_which_has_speak_label_is_navigating_to_the_loans_dashboard() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on the selected loans which has speak label is navigating to the loans dashboard";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.banner_LoansAccNo_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for loans in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_loans_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for loans in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansDashboardSpeaklabel_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansDashboardSpeaklabelActionButton_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    

    @And("^To verify whether the required mandatory field sp upsell banner offer title in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansSPTitle_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansSPDescriptionAndroid);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer title in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_title_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer title in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansTJTitle_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer description in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_description_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer description in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansTJDescription_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer title in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansXSellTitle_Android);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in loans dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_loans_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in loans dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerLoansXSellDescription_Android);
	Thread.sleep(2000);
	CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.bannerBack_Android);
	Thread.sleep(10000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    //--------------------------------End of Loans --------------------------------------------------------------------------------------------//
   
    //--------------------------------Deposit --------------------------------------------------------------------------------------------------//
    
   
    
    @And("^To verify whether the required mandatory field cross sell ghost deposit banner offer title in mrc dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_ghost_deposit_banner_offer_title_in_mrc_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell ghost deposit banner offer title in mrc dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell ghost deposit banner offer description in mrc dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_ghost_deposit_banner_offer_description_in_mrc_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell ghost deposit banner offer description in mrc dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    @And("^To verify whether the required mandatory field deposits category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_deposits_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field deposits category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for deposit category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_deposit_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for deposit category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on deposit category to expand to show all deposit products$")
    public void to_verify_whether_on_clicking_on_deposit_category_to_expand_to_show_all_deposit_products() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on deposit category to expand to show all deposit products";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for deposit product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_deposit_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for deposit product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button under deposit product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_under_deposit_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button under deposit product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    

    @And("^To verify whether the required mandatory field sp upsell banner offer title in deposit category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_deposit_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in deposit category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in deposit category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_deposit_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in deposit category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on the selected deposit which has speak label is navigating to the deposit dashboard$")
    public void to_verify_whether_on_clicking_on_the_selected_deposit_which_has_speak_label_is_navigating_to_the_deposit_dashboard() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on the selected deposit which has speak label is navigating to the deposit dashboard";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for deposit in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_deposit_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for deposit in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

   
    @And("^To verify whether the required mandatory field sp upsell banner offer title in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer title in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_title_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer title in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer description in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_description_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer description in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer title in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in deposit dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_deposit_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in deposit dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    //--------------------------------End of Deposit -------------------------------------------------------------------------------------------//
    
    
    //-------------------------------Investment -----------------------------------------------------------------------------------------------//
    
    @And("^To verify whether the required mandatory field investment category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_investment_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field investment category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for investment category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_investment_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for investment category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on investment category to expand to show all investment products$")
    public void to_verify_whether_on_clicking_on_investment_category_to_expand_to_show_all_investment_products() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on investment category to expand to show all investment products";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for investment product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_investment_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for investment product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button under investment product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_under_investment_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button under investment product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

   

    @And("^To verify whether the required mandatory field sp upsell banner offer title in investment category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_investment_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in investment category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in investment category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_investment_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in investment category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on the selected investment which has speak label is navigating to the investment dashboard$")
    public void to_verify_whether_on_clicking_on_the_selected_investment_which_has_speak_label_is_navigating_to_the_investment_dashboard() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on the selected investment which has speak label is navigating to the investment dashboard";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for investment in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_investment_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for investment in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

   

    @And("^To verify whether the required mandatory field sp upsell banner offer title in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer title in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in investment dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_investment_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in investment dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    //--------------------------------------End of Investment -------------------------------------------------------------------------------------------//
    
    
    //----------------------------------------Insurance -------------------------------------------------------------------------------------------------//
    
    @And("^To verify whether the required mandatory field insurance category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_insurance_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field insurance category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for insurance category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_insurance_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for insurance category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on insurance category to expand to show all insurance products$")
    public void to_verify_whether_on_clicking_on_insurance_category_to_expand_to_show_all_insurance_products() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on insurance category to expand to show all insurance products";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for insurance product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_insurance_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for insurance product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button under insurance product is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_under_insurance_product_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button under insurance product is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    

    @And("^To verify whether the required mandatory field sp upsell banner offer title in insurance category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_insurance_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in insurance category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in insurance category is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_insurance_category_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in insurance category is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether on clicking on the selected insurance which has speak label is navigating to the insurance dashboard$")
    public void to_verify_whether_on_clicking_on_the_selected_insurance_which_has_speak_label_is_navigating_to_the_insurance_dashboard() throws Throwable {
       ExtentReport.stepName = "To verify whether on clicking on the selected insurance which has speak label is navigating to the insurance dashboard";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label for insurance in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_insurance_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for insurance in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

   

    @And("^To verify whether the required mandatory field sp upsell banner offer title in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer title in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in insurance dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_insurance_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in insurance dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    
    //---------------------------------------End of Insurance ------------------------------------------------------------------------------------------//
   
    
    //-------------------------------------Retail ------------------------------------------------------------------------------------------------------//
    
    @And("^To verify whether the required mandatory field speak label for retail in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_for_retail_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label for retail in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field speak label action button in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_speak_label_action_button_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field speak label action button in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

  

    @And("^To verify whether the required mandatory field sp upsell banner offer title in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_title_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer title in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field sp upsell banner offer description in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_sp_upsell_banner_offer_description_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field sp upsell banner offer description in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer title in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_title_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer title in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field tj upsell banner offer description in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_tj_upsell_banner_offer_description_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field tj upsell banner offer description in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer title in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_title_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer title in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }

    @And("^To verify whether the required mandatory field cross sell banner offer description in retail dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_cross_sell_banner_offer_description_in_retail_dashboard_is_getting_displayed() throws Throwable {
       ExtentReport.stepName = "To verify whether the required mandatory field cross sell banner offer description in retail dashboard is getting displayed";
	if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
	else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
	CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
	Thread.sleep(2000);
	}
    }
    
    //------------------------------------------------End of Retail ------------------------------------------------------------------------------------------------//
    
	@After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
