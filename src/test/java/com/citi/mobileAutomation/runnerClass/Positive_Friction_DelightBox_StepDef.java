package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.Positive_Friction_Banner_Objects;
import com.citi.mobileAutomation.objectRepository.QuickCash_Objects;
import com.citi.mobileAutomation.objectRepository.MRC_Dashboard_Objects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class Positive_Friction_DelightBox_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;

	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	

	/*
	 * @Author:Dilipkumar
	 * Product :Positive Friction Banner-Delight Box
	 * Domain :Grow
	 * Platform:Android
	 * 
	 */
	

	/*-------------------------------------------------Delight Box --------------------------------------------------------------------------------------------------*/
	
	@And("^To Verify whether the required mandatory Field Delight Box offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens$")
    public void to_verify_whether_the_required_mandatory_field_delight_box_offer_title_is_getting_displayed_as_per_the_condition_delightbox_widget_is_only_applicable_for_mrc_dashboard_screensno_ada() throws Throwable {
		 ExtentReport.stepName = "To Verify whether the required mandatory Field Delight Box offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightX_Android);	
			Thread.sleep(5000);	
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field Delight Box offer thumbnail is getting displayed as per the condition Optional Image icon$")
    public void to_verify_whether_the_required_mandatory_field_delight_box_offer_thumbnail_is_getting_displayed_as_per_the_condition_optional_image_icon() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Delight Box offer thumbnail is getting displayed as per the condition Optional Image icon";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightThumbnail_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field Delight Box offer decscription is getting displayed as per the condition Array containing all UDF variable keys$")
    public void to_verify_whether_the_required_mandatory_field_delight_box_offer_variables_is_getting_displayed_as_per_the_condition_array_containing_all_udf_variable_keys() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field Delight Box offer decscription is getting displayed as per the condition Array containing all UDF variable keys";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightDescriptionMRC_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field Delight Box offer right chevron in dashboard is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_delight_box_offer_right_chevron_in_dashboard_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field Delight Box offer right chevron in dashboard is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightRightChevron_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking STP Journey CTA Action pointed to Delight Box offer which is navigating to expected STP Journey Screen$")
    public void to_verify_whether_on_clicking_stp_journey_cta_action_pointed_to_delight_box_offer_which_is_navigating_to_expected_stp_journey_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STP Journey CTA Action pointed to Delight Box offer which is navigating to expected STP Journey Screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field expected STP screen offer title is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_expected_stp_screen_offer_title_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected STP screen offer title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightSTPTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking back button in STP screen which is navigate back to dashboard screen$")
    public void to_verify_whether_on_clicking_back_button_in_stp_screen_which_is_navigate_back_to_dashboard_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in STP screen which is navigate back to dashboard screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightSTPBackButton_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking STO Journey CTA Action pointed to Delight Box offer which is navigating to expected STO Journey Screen$")
    public void to_verify_whether_on_clicking_sto_journey_cta_action_pointed_to_delight_box_offer_which_is_navigating_to_expected_sto_journey_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking STO Journey CTA Action pointed to Delight Box offer which is navigating to expected STO Journey Screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightTitle_Android);
			Thread.sleep(60000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field expected STO screen offer title is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_expected_sto_screen_offer_title_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected STO screen offer title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightSTOTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking back button in STO screen which is navigate back to dashboard screen$")
    public void to_verify_whether_on_clicking_back_button_in_sto_screen_which_is_navigate_back_to_dashboard_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in STO screen which is navigate back to dashboard screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightSTOCloseButton_Android);
			Thread.sleep(20000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking CVP CTA Action pointed to Delight Box offer which is navigating to CVP Screen$")
    public void to_verify_whether_on_clicking_cvp_cta_action_pointed_to_delight_box_offer_which_is_navigating_to_cvp_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking CVP CTA Action pointed to Delight Box offer which is navigating to CVP Screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field expected CVP screen offer title is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_expected_cvp_screen_offer_title_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected CVP screen offer title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightCVPTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen$")
    public void to_verify_whether_on_clicking_back_button_in_cvp_screen_which_is_navigate_back_to_dashboard_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightCVPTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field Delight Box offer right chevron in dashboard should not getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_delight_box_offer_right_chevron_in_dashboard_should_not_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field Delight Box offer right chevron in dashboard should not getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking No Action CTA Action pointed to Delight Box offer which should not navigating to any screen$")
    public void to_verify_whether_on_clicking_no_action_cta_action_pointed_to_delight_box_offer_which_should_not_navigating_to_any_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking No Action CTA Action pointed to Delight Box offer which should not navigating to any screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether on clicking WebView CTA Action pointed to Delight Box offer which is navigating to expected Webview Screen$")
    public void to_verify_whether_on_clicking_webview_cta_action_pointed_to_delight_box_offer_which_is_navigating_to_expected_webview_screen() throws Throwable {
        ExtentReport.stepName = "To verify whether on clicking WebView CTA Action pointed to Delight Box offer which is navigating to expected Webview Screen";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To verify whether the required mandatory field expected WebView screen offer title is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_expected_webview_screen_offer_title_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To verify whether the required mandatory field expected WebView screen offer title is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVTitle_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_YES is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctayes_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_YES is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVYesCTA_Android);
			Thread.sleep(10000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }
    
    @And("^To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer$")
    public void to_veirfy_whetther_on_clicking_ctayes_is_navigating_to_ctayessuccessheader_drawer() throws Throwable {
    	 ExtentReport.stepName = "To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightWBVYesCTA_Android);
			Thread.sleep(10000);

			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctayessuccessheader_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVYesHeader_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctayessuccessbodycontent_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVYesBody_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_NO is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctano_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_NO is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVNoCTA_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }
    
    @And("^To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer$")
    public void to_veirfy_whetther_on_clicking_ctano_is_navigating_to_ctanosuccessheader_drawer() throws Throwable {
    	 ExtentReport.stepName = "To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightWBVNoCTA_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctanosuccessheader_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVNoHeader_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }
    @And("^To Verify whether the required mandatory Field CTA_OK_HEADER is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctaokheader_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_OK_HEADER is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVOKHeader_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctanosuccessbodycontent_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVNoBody_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_OK is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctaok_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_OK is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVOKCTA_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer$")
    public void to_veirfy_whetther_on_clicking_ctaok_is_navigating_to_ctaokbodycontent_drawer() throws Throwable {
    	   ExtentReport.stepName = "To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			Thread.sleep(5000);	
			CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightWBVOKCTA_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }
    
    @And("^To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctaokbodycontent_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVOKBody_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed$")
    public void to_verify_whether_the_required_mandatory_field_ctaerrorheader_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVErrorHeader_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed")
    public void to_verify_whether_the_required_mandatory_field_ctaerrorbodycontent_is_getting_displayed() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVErrorBody_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }

    @And("^To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always$")
    public void to_verify_whether_the_required_mandatory_field_ctaclose_is_getting_displayed_as_per_the_condition_always() throws Throwable {
        ExtentReport.stepName = "To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.delightWBVCloseCTA_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
			Thread.sleep(2000);
			}
    }
    
    @And("^To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen$")
    public void to_verify_whether_on_clicking_ctaclose_is_closing_the_drawer() throws Throwable {
    	ExtentReport.stepName = "To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen";
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		CommonMethods.clickMethodMultipleSession(Positive_Friction_Banner_Objects.delightWBVCloseCTA_Android);
		Thread.sleep(5000);
		}
		else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
		CommonMethods.isElementPresent(Positive_Friction_Banner_Objects.bannerCardCategory_And);
		Thread.sleep(2000);
		}
    }
	
	
	
	/*-------------------------------------------------End of Delight Box --------------------------------------------------------------------------------------------------*/
	
  
		    @After
			public void updateExcelStatus(Scenario scenario) throws FilloException {

				ExtentReport.updateExcelStatus(scenario);

			}

			@AfterStep
			public static void getResult() throws Exception {

				ExtentReport.getResult();

			}

}
