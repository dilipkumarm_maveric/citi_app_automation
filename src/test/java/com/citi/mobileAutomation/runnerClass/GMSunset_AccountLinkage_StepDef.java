package com.citi.mobileAutomation.runnerClass;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.dataDriven.Xls_Reader_Path;
import com.citi.mobileAutomation.objectRepository.GMSunsetObjects;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;
import com.codoid.products.exception.FilloException;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.StepDefinition;
import io.cucumber.java.AfterStep;

public class GMSunset_AccountLinkage_StepDef {

	private static Logger logger = Logger.getLogger(StepDefinition.class);
	public static String status = "pass";
	public static WebDriver driver;
	

	/*
	 * @Author:Dilipkumar
	 * Product :GM Sunset - Account Linkage 
	 * Platform:Android
	 * 
	 */

	
	@Before
	public void getScenarioName(Scenario scene) throws FilloException {
		ExtentReport.getScenarioName(scene);

	}
	
	 @And("^To verify whether the required mandatory field Profile & Settings icon in Dashboard is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_profile_settings_icon_in_dashboard_is_getting_displayed() throws Throwable {
		 ExtentReport.stepName = "To verify whether the required mandatory field Profile & Settings icon in Dashboard is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmProfileAndSettings_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking Profile & Settings icon is navigate to Settings page$")
	    public void to_verify_whether_on_clicking_profile_settings_icon_is_navigate_to_settings_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Profile & Settings icon is navigate to Settings page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmProfileAndSettings_Android);
			Thread.sleep(15000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field GM Refresh in settigs page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_gm_refresh_in_settigs_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field GM Refresh in settigs page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmGMRefresh_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking GM Refresh is navigate to New Profile & Settings page$")
	    public void to_verify_whether_on_clicking_gm_refresh_is_navigate_to_new_profile_settings_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking GM Refresh is navigate to New Profile & Settings page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmGMRefresh_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Debit card is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_debit_card_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Debit card is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmDebitCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Accounts is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_accounts_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Accounts is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAccounts_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking Debit card is navigate to Manage cards & accounts page$")
	    public void to_verify_whether_on_clicking_debit_card_is_navigate_to_manage_cards_accounts_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Debit card is navigate to Manage cards & accounts page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmDebitCard_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Account Linkage is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_account_linkage_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Account Linkage is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAccountLinkage_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking Account Linkage is navigate to Debit card linkage page$")
	    public void to_verify_whether_on_clicking_account_linkage_is_navigate_to_debit_card_linkage_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking Account Linkage is navigate to Debit card linkage page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmAccountLinkage_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }
	    
	    @And("^To verify whether the required mandatory field select debit card in select card page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_select_debit_card_in_select_card_page_is_getting_displayed() throws Throwable {
	        ExtentReport.stepName = "To verify whether the required mandatory field select debit card in select card page is getting displayed";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.isElementPresent(GMSunsetObjects.gmALSelectDebitcard_Android);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
				Thread.sleep(2000);
				}
	    }

	    @And("^To verify whether on clicking on any one of the debit card in select card page is navigate to Debit card linkage page$")
	    public void to_verify_whether_on_clicking_on_any_one_of_the_debit_card_in_select_card_page_is_navigate_to_debit_card_linkage_page() throws Throwable {
	        ExtentReport.stepName = "To verify whether on clicking on any one of the debit card in select card page is navigate to Debit card linkage page";
				if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
				CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmALDebitcardNo_Android);
				Thread.sleep(5000);
				}
				else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
				CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
				Thread.sleep(2000);
				}
	    }


	    @And("^To verify whether the required mandatory field back arrow in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_back_arrow_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field back arrow in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200_BackChevronAndroid);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Debit card details in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_debit_card_details_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Debit card details in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200DebitcardNoLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Currently linked label in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_currently_linked_label_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Currently linked label in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Other Accounts label in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_other_accounts_label_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Other Accounts label in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200OtherAccountLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field currently linked account details in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_currently_linked_account_details_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field currently linked account details in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200prevLinkedCardNo_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field currently linked account radio button in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_currently_linked_account_radio_button_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field currently linked account radio button in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200prevLinkedCardNo_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field other account details in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_other_account_details_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field other account details in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200NewlinkedCardNo_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field other account radio button in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_other_account_radio_button_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field other account radio button in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200NewlinkedCardNo_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking any one of the other account to link debit card for selection$")
	    public void to_verify_whether_on_clicking_any_one_of_the_other_account_to_link_debit_card_for_selection() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking any one of the other account to link debit card for selection";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmAL200NewlinkedCardNo_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Disclaimer in linkage page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_disclaimer_in_linkage_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Disclaimer in linkage page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200Discliamer_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field Update button is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_update_button_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field Update button is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200Update_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking update button for is navigate to linkage 300 page$")
	    public void to_verify_whether_on_clicking_update_button_for_is_navigate_to_linkage_300_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking update button for is navigate to linkage 300 page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmAL200Update_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field tick image in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_tick_image_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field tick image in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300tickmark_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field successful message in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_successful_message_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field successful message in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300SuccessMesaage_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field card number label in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_card_number_label_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field card number label in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300CardNoLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field card number value in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_card_number_value_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field card number value in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300prevlinkedno_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field account linked label in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_account_linked_label_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field account linked label in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300AccountlinkedLabel_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field account linked details in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_account_linked_details_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field account linked details in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300linkedaccno_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether the required mandatory field done button in linkage 300 page is getting displayed$")
	    public void to_verify_whether_the_required_mandatory_field_done_button_in_linkage_300_page_is_getting_displayed() throws Throwable {
	           ExtentReport.stepName = "To verify whether the required mandatory field done button in linkage 300 page is getting displayed";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL300Done_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }

	    @And("^To verify whether on clicking done button for is navigate to Manage cards & accounts page$")
	    public void to_verify_whether_on_clicking_done_button_for_is_navigate_to_manage_cards_accounts_page() throws Throwable {
	           ExtentReport.stepName = "To verify whether on clicking done button for is navigate to Manage cards & accounts page";
			if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			CommonMethods.clickMethodMultipleSession(GMSunsetObjects.gmAL300Done_Android);
			Thread.sleep(5000);
			}
			else if (Xls_Reader.platformToBeTested.equalsIgnoreCase("iOS")){
			CommonMethods.isElementPresent(GMSunsetObjects.gmAL200currentlyLinkedLabel_Android);
			Thread.sleep(2000);
			}
	    }
	
		    
    @After
	public void updateExcelStatus(Scenario scenario) throws FilloException {

		ExtentReport.updateExcelStatus(scenario);

	}

	@AfterStep
	public static void getResult() throws Exception {

		ExtentReport.getResult();

	}

}
