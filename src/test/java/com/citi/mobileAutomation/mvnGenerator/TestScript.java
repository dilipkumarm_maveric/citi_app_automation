package com.citi.mobileAutomation.mvnGenerator;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.junit.Test;

import com.citi.mobileAutomation.autoInvoking.OpeningAppiumServer;
import com.citi.mobileAutomation.runnerClass.RunnerFile;

public class TestScript {

	public static int iExitValue;
	public static String sCommandString;
	public static String finalRun = "";

	public static void runScript(String command) {
		sCommandString = command;
		CommandLine oCmdLine = CommandLine.parse(sCommandString);
		DefaultExecutor oDefaultExecutor = new DefaultExecutor();
		oDefaultExecutor.setExitValue(0);

		try {
			iExitValue = oDefaultExecutor.execute(oCmdLine);
		} catch (ExecuteException e) {
			System.err.println("Execution failed.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("permission denied.");
			e.printStackTrace();
		}
	}

	@Test
	public void main() throws IOException, InterruptedException {

		CheckStatus.readStatusMethod();
		System.out.println(CheckStatus.runFile);
		TestScript testScript = new TestScript();
		// testScript.runScript("/Users/apple/Downloads/com.citi.babu/src/test/java/com/citi/babu/Myscript.sh");
		for (String s : CheckStatus.arr)
			testScript.runScript(s);

	}

}
