package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class LoginObjects {
	
	/*
	 * @Author:Dilipkumar
	 * Functionlity:Login
	 * Platform:Android
	 */
	
	
	public static final By coutinue_Android=By.xpath("//android.widget.TextView[@text='Continue']");
	public static final By english_Android=By.xpath("//*[@text='English']");
	public static final By accept_Android=By.xpath("//*[@text='Accept']");
	public static final By loginwithExistAccount_Android=By.xpath("//*[contains(@text,'Log in')]");
	public static final By username_Android=By.xpath("//*[contains(@text,'User ID')]");
	public static final By password_Android=By.xpath("//*[contains(@text,'Password')]");
	public static final By loginButton_Android=By.xpath("//*[@text='Log in']");
	public static final By getStarted_Android=By.xpath("//*[@text='Get started']");
	public static final By enableButton_Android=By.xpath("//*[@text='Enable']");
	public static final By alert_Ok_Btn_Android=By.xpath("//*[@text='OK/YES']");
	public static final By dontAllow_Android=By.id("com.citi.mobile.sg.dit:id/cancel_button");
	public static final By experienceCitiMobile_Android=By.xpath("//android.widget.TextView[contains(@text,'Experience Citi Mobile')]");
	
	/*
	 * @Author:Dilipkumar
	 * Functionlity:Login
	 * Platform:iOS
	 */
	
	
	public static final By coutinue_iOS=By.xpath("//XCUIElementTypeButton[@name='Continue']");
	public static final By english_iOS=By.xpath("//*[@name='English']");
	public static final By loginwithExistAccount_iOS=By.xpath("//XCUIElementTypeButton[@name='Log in with existing account']");
	public static final By username_iOS=By.xpath("//XCUIElementTypeStaticText[@name='User ID']");
	public static final By password_iOS=By.xpath("//XCUIElementTypeStaticText[@name='Password']");
	public static final By loginButton_iOS=By.xpath("//XCUIElementTypeButton[@name='Btn_Signon_230_login']");
	public static final By getStarted_iOS=By.xpath("//XCUIElementTypeButton[@name='good_button']");
	public static final By enableButton_iOS=By.xpath("//XCUIElementTypeButton[@name='good_button']");
	public static final By experienceCitiMobile_iOS=By.xpath("//XCUIElementTypeButton[contains(@name,'Experience Citi Mobile')]");

			
	}
