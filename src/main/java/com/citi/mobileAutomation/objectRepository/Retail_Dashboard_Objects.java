package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class Retail_Dashboard_Objects {
	
	/*
	 * @Author:Dilipkumar
	 * Functionality :Retail Dashboard
	 * Platform:Android
	 */
    
	public static final By retailcheckingsAccountName_And= By.xpath("(//*[contains(@text,'Checking')])[1]");
	public static final By retailcheckingsAccountNumber_And= By.xpath("(//*[contains(@text,'2578')])[2]");
	public static final By retailcheckingsAccountBalanceCCY_And= By.id("//*[contains(@text,'AUD 106,341.89')]");
	public static final By retailcheckingsAccountBalanceValue_And = By.xpath("//*[contains(@text,'AUD 106,341.89')]"); 
	public static final By retailcheckingsRightChevron = By.xpath("(//android.widget.Image)[5]"); 

	public static final By retailsavingsAccountName_And= By.xpath("(//*[contains(@text,'Savings')])[1]");
	public static final By retailsavingsAccountNumber_And= By.xpath("(//*[contains(@text,'2955')])[1]");
	public static final By retailsavingsAccountBalanceCCY_And= By.id("//*[contains(@text,'AUD 106,483.06')]");
	public static final By retailsavingsAccountBalanceValue_And = By.xpath( "//*[contains(@text,'AUD 106,483.06')]"); 
	public static final By retailsavingsRightChevron = By.xpath("(//android.widget.Image)[17]"); 
	
	
	public static final By retailtimedepositAccountName_And= By.xpath("//*[contains(@text,'Time Deposit')]");
	public static final By retailtimedepositAccountNumber_And= By.xpath("//*[contains(@text,'0929')]");
	public static final By retailtimedepositAccountBalanceCCY_And= By.id("//*[contains(@text,'SGD 10,000.00')]");
	public static final By retailtimedepositAccountBalanceValue_And = By.xpath("//*[contains(@text,'SGD 10,000.00')]"); 
	public static final By retailtimedepositRightChevron = By.xpath("(//android.widget.Image)[21]");  
	
	
	public static final By retailcheckingsADAcheckingLabel = By.xpath("(//*[contains(@text,'Checking')])[8]"); 
	public static final By retailcheckingsADAcheckingAccNumber = By.xpath("//*[contains(@text,'3777')]"); 
	public static final By retailcheckingsADAavailableNowLabel = By.xpath("//*[contains(@text,'Available now')]"); 
	public static final By retailcheckingsADAavailableNowValue = By.xpath("//*[contains(@text,'USD 87,676.54')]"); 
	public static final By retailcheckingsADAavailableNowRightChevron = By.xpath("(//android.widget.Image)[2]"); 
	public static final By retailcheckingsADAMFACTA = By.xpath("//*[contains(@text,'Make a transfer')]"); 
	public static final By retailcheckingsADAGWIcon = By.xpath("(//android.widget.Image)[3]"); 
	public static final By retailcheckingsADAGWLabel = By.xpath("//*[contains(@text,'Citibank Global Wallet')]"); 
	public static final By retailcheckingsADAGWDescrip = By.xpath("//*[contains(@text,'Spend like a local in 16 currencies at exchange rates you favor')]"); 
	public static final By retailcheckingsADAGWRightChevron = By.xpath("(//android.widget.Image)[4]"); 
	public static final By retailcheckingsADAStatementIcon = By.xpath("(//android.widget.Image)[5]"); 
	public static final By retailcheckingsADAStatementLabel = By.xpath("//*[contains(@text,'Statement')]"); 
	public static final By retailcheckingsADAStatementValue = By.xpath("//*[contains(@text,'February 2028')]"); 
	public static final By retailcheckingsADAStatementRightChevron = By.xpath("(//android.widget.Image)[6]"); 
	public static final By retailcheckingsADAlastTransactions = By.xpath("//*[contains(@text,'LAST TRANSACTIONS')]"); 
	public static final By retailcheckingsADAsearch = By.xpath("//*[contains(@text,'Search')]"); 
	public static final By retailcheckingsADAfirstTransMatureDate = By.xpath("(//*[contains(@text,'30 JUL 2021')])[1]"); 
	public static final By retailcheckingsADAfirstTransStatus = By.xpath("//*[contains(@text,'PENDING')]"); 
	public static final By retailcheckingsADAfirstTransAmount = By.xpath("(//*[contains(@text,'SGD 10.00')])[1]"); 
	public static final By retailcheckingsADAfirstTransDescrip = By.xpath("//*[contains(@text,'BILL PAYMENT PT3Payee xxxxxx7899')]"); 
	public static final By retailcheckingsADAfirstTransRightChevron = By.xpath("(//android.widget.Image)[7]"); 
	public static final By retailcheckingsADAViewallTransIcon = By.xpath("(//android.widget.Image)[12]"); 
	public static final By retailcheckingsADAViewAllTransactionLabel = By.xpath("//*[contains(@text,'View all transactions')]"); 
	public static final By retailcheckingsADAViewAllTransactionRightChevron = By.xpath("(//android.widget.Image)[13]"); 


	public static final By retailsavingsADAsavingsLabel = By.xpath("(//*[contains(@text,'Savings Account')])[2]"); 
	public static final By retailsavingsADAsavingsAccNo = By.xpath("(//*[contains(@text,'1105')])[2]"); 
	public static final By retailsavingsADAavailableNowLabel = By.xpath("//*[contains(@text,'Available now')]"); 
	public static final By retailsavingsADAavailableNowValue = By.xpath("//*[contains(@text,'SGD 12,405.22')]"); 
	public static final By retailsavingsADAavailableRightChevron = By.xpath("(//android.widget.Image)[2]"); 
	public static final By retailsavingsADAManageCTA = By.xpath("(//*[contains(@text,'Manage')])[2]"); 
	public static final By retailsavingsADAMFACTA = By.xpath("//*[contains(@text,'Make a transfer')]"); 
	public static final By retailsavingsADAStatementIcon = By.xpath("(//android.widget.Image)[3]"); 
	public static final By retailsavingsADAStatementLabel = By.xpath("//*[contains(@text,'Statement')]"); 
	public static final By retailsavingsADAStatementValue = By.xpath("//*[contains(@text,'November 2028')]"); 
	public static final By retailsavingsADAStatementRightChevron = By.xpath("(//android.widget.Image)[4]"); 
	public static final By retailsavingsADALastTransaction = By.xpath("//*[contains(@text,'LAST TRANSACTIONS')]"); 
	public static final By retailsavingsADAsearch = By.xpath("//*[contains(@text,'Search')]"); 
	public static final By retailsavingsADAfirstTransMatureDate = By.xpath("(//*[contains(@text,'30 JUL 2021')])[1]"); 
	public static final By retailsavingsADAfirstTransStatus = By.xpath("//*[contains(@text,'PENDING')]"); 
	public static final By retailsavingsADAfirstTransAmount = By.xpath("(//*[contains(@text,'SGD 10.00')])[1]"); 
	public static final By retailsavingsADAfirstDescrip = By.xpath("//*[contains(@text,'BILL PAYMENT PT3Payee xxxxxx7899')]"); 
	public static final By retailsavingsADAfirstTransRightChevron = By.xpath("(//android.widget.Image)[8]"); 
	public static final By retailsavingsADAViewAllTransactionIcon = By.xpath("(//android.widget.Image)[12]"); 
	public static final By retailsavingsADAViewAllTransactionLabel = By.xpath("//*[contains(@text,'View all transactions')]"); 
	public static final By retailsavingsADAViewAllTransactionRightChevron = By.xpath("(//android.widget.Image)[13]"); 

	

	public static final By retailtimedepositADAtimedepositLabel = By.xpath("(//*[contains(@text,'Time Deposit')])[2]");  
	public static final By retailtimedepositADAtimedepositAccNo = By.xpath("(//*[contains(@text,'6846')])[8]");  
	public static final By retailtimedepositADAtotalprincipalLabel = By.xpath("//*[contains(@text,'Total principal')]");  
	public static final By retailtimedepositADAtotalprincipalValue = By.xpath("(//*[contains(@text,'SGD 40,829.94')])[2]");  
	public static final By retailtimedepositADAfirsttimedepositLabel = By.xpath("//*[contains(@text,'Time Deposit 000042')]"); 
	public static final By retailtimedepositADAfirsttimedepositplacementNo = By.xpath("//*[contains(@text,'Time Deposit 000042')]"); 
	public static final By retailtimedepositADAfirsttimedepositMatureDate = By.xpath("//*[contains(@text,'Maturing on 31 Mar 2022')]"); 
	public static final By retailtimedepositADAfirsttimedepositRightChevron = By.xpath("(//android.widget.Image)[12]"); 
	public static final By retailtimedepositADAfirsttimedepositAmount = By.xpath("(//*[contains(@text,'SGD 40,829.94')])[3]"); 
	public static final By retailtimedepositADAOTDCTA = By.xpath("//*[contains(@text,'Open Time Deposit')]"); 
	public static final By retailtimedepositADAStatementLabel = By.xpath("//*[contains(@text,'Statement')]"); 
	public static final By retailtimedepositADAStatementRightChevron = By.xpath("(//android.widget.Image)[19]"); 	
	
}
