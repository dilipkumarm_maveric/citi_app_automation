package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class GMSunsetObjects {
	
	/*
	 * @Author:Dilipkumar
	 * Product :GM Sunset - Account Linkage
	 * Platform:Android
	 */
		
	public static final By gmProfileAndSettings_Android=By.xpath("(//android.widget.Image)[1]");
	public static final By gmGMRefresh_Android=By.xpath("//*[@text='GM Refresh']");
	public static final By gmDebitCard_Android=By.xpath("//*[@text='Debit Card']");
	public static final By gmAccounts_Android=By.xpath("//*[@text='Accounts']");
	public static final By gmAccountLinkage_Android=By.xpath("//*[@text='Account Linkage']");
	
	public static final By gmALSelectDebitcard_Android=By.xpath("//*[@text='Select debit card']");
	public static final By gmALDebitcardNo_Android=By.xpath("//*[@text='Debit Card **** 3136']");
	
	public static final By gmAL200DebitcardNoLabel_Android=By.xpath("(//*[@text='Debit Card **** 3136'])[2]");
	public static final By gmAL200_BackChevronAndroid=By.xpath("(//android.widget.Image)[48]");
	public static final By gmAL200currentlyLinkedLabel_Android=By.xpath("//*[@text='Currently Linked']");
	public static final By gmAL200prevLinkedCardNo_Android=By.xpath("//*[contains(@text,'3136')]");
	public static final By gmAL200OtherAccountLabel_Android=By.xpath("//*[@text='Other Accounts']");
	public static final By gmAL200NewlinkedCardNo_Android=By.xpath("//*[contains(@text,'0823')]");
	public static final By gmAL200Discliamer_Android=By.xpath("//*[@text='Disclaimer Content']");
	public static final By gmAL200Update_Android=By.xpath("//*[@text='Update']");
	
	public static final By gmAL300tickmark_Android=By.xpath("(//android.widget.Image)[49]");
	public static final By gmAL300SuccessMesaage_Android=By.xpath("//*[@text='Successful message']");
	public static final By gmAL300CardNoLabel_Android=By.xpath("//*[@text='Card number']");
	public static final By gmAL300prevlinkedno_Android=By.xpath("(//*[contains(@text,'3136')])[2]");
	public static final By gmAL300AccountlinkedLabel_Android=By.xpath("//*[@text='Account Linked']");
	public static final By gmAL300linkedaccno_Android=By.xpath("(//*[contains(@text,'0823')])[2]");
	public static final By gmAL300Done_Android=By.xpath("//*[@text='Done']");
		
			
	}
