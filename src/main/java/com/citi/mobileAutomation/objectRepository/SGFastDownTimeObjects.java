package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class SGFastDownTimeObjects {
	
	/*
	 * @Author:Dilipkumar
	 * Product :SG Fast Down Time - PT2
	 * Platform:Android
	 */
		
	public static final By sgfdt_PaymentsMenu=By.xpath("//*[@text='Payments']");
	public static final By sgfdt_Transfers=By.xpath("//*[@text='Transfers']");
	public static final By sgfdt_PT2Payee=By.xpath("//*[contains(@text,'7337')]");
	public static final By sgfdt_AmountEntry200=By.xpath("//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText");
	public static final By sgfdt_NextButton=By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout");
	public static final By sgfdt_FastTooltip=By.xpath("//*[@text='GIRO transfers will take 2-3 working days from this date']");
	public static final By sgfdt_PayButton=By.xpath("//*[@text='Pay']");
	public static final By sgfdt_RedExlamationMark=By.xpath("//android.widget.ImageView");
	public static final By sgfdt_FastDowntimeHeader=By.xpath("//*[@text='FAST transfer is unavailable at the moment']");
	public static final By sgfdt_FastDowntimeDescription=By.xpath("//*[contains(@text,'We will submit your transfer request via GIRO')]");
	public static final By sgfdt_PayWithGIRO=By.xpath("//*[@text='Pay with GIRO']");
	public static final By sgfdt_Cancel=By.xpath("//*[@text='Cancel']");

	public static final By sgfdtAdhoc_NewPayee=By.xpath("//*[@text='New payee']");
	public static final By sgfdtAdhoc_NewPayeeHeader=By.xpath("(//*[@text='New payee'])[2]");
	public static final By sgfdtAdhoc_Country=By.xpath("//*[@text='SINGAPORE']");
	public static final By sgfdtAdhoc_Name=By.xpath("//*[@text='Name']");
	public static final By sgfdtAdhoc_Bank=By.xpath("(//*[contains(@text,'Bank')])[2]");
	public static final By sgfdtAdhoc_SelectedBank=By.xpath("//*[contains(@text,'ANZBSGSXXXX')]");
	public static final By sgfdtAdhoc_AccNumber=By.xpath("//*[contains(@text,'Account number')]");
	public static final By sgfdtAdhoc_NextButton=By.xpath("//android.widget.RelativeLayout[@content-desc=\"Next Arrow\"]/android.widget.RelativeLayout/android.widget.FrameLayout");
	public static final By sgfdtAdhoc_IknowthisPayee=By.xpath("//*[contains(@text,'I know this payee')]");

			
	}
