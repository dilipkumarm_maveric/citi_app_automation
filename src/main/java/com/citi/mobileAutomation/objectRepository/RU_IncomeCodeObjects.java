package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class RU_IncomeCodeObjects {
	
	/*
	 * @Author:Dilipkumar
	 * Product :RU PT2 Payment - Income code type field
	 * Platform:Android
	 */
		
	public static final By ruic_PaymentsMenu=By.xpath("//*[@text='Payments']");
	public static final By ruic_Byrequisties=By.xpath("//*[@text='By requisites']");
	public static final By ruic_PT2=By.xpath("//*[contains(@text,'Pt2')]");
	public static final By ruic_Amount200=By.xpath("//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.EditText");
	public static final By ruic_Next200=By.xpath("//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout");
	public static final By ruic_IncomeCodeLabel=By.xpath("(//*[contains(@text,'Income type code')])[1]");
	public static final By ruic_InlineTextIncomeCodeLabel=By.xpath("//*[contains(@text,'Select Income type code')]");
	public static final By ruic_SelectBtn=By.xpath("//*[@text='Select']");
	public static final By ruic_ChooseIncomeLabel=By.xpath("//*[@text='Choose income type']");
	public static final By ruic_ICValue1=By.xpath("//*[@text='1']");
	public static final By ruic_ICValue2=By.xpath("//*[@text='2']");
	public static final By ruic_ICValue3=By.xpath("//*[@text='3']");
	public static final By ruic_PayBtn=By.xpath("//*[@text='Pay']");
	public static final By ruic_IncomeLabel700=By.xpath("//*[@text='Income type code']");
	public static final By ruic_IncomeValue700=By.xpath("(//*[@text='2'])[2]");

	
	public static final By ruicAdhoc_NewPayee=By.xpath("//*[@text='New payee']");
	public static final By ruicAdhoc_AccNo=By.xpath("//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText");
	public static final By ruicAdhoc_Bank=By.xpath("(//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText)[2]");
	public static final By ruicAdhoc_BankName=By.xpath("//android.widget.EditText[contains(@text,'Bank')]");
	public static final By ruicAdhoc_SelectBankPT2=By.xpath("//*[contains(@text,'HSBC5101')]");
	public static final By ruicAdhoc_BeneficiaryName=By.xpath("//android.widget.EditText[contains(@text,'Beneficiary Name')]");
	public static final By ruicAdhoc_NextArrowPayee=By.xpath("//android.widget.RelativeLayout[@content-desc='Next Arrow']/android.widget.RelativeLayout/android.widget.FrameLayout");
	public static final By ruicAdhoc_Amount200=By.xpath("//*[@text='']");
	public static final By ruicAdhoc_Next200=By.xpath("//*[@text='']");

			
	}
