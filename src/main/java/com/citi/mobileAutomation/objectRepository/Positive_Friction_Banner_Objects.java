package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class Positive_Friction_Banner_Objects {

	/*
	 * @Author:Dilipkumar
	 * Functionality :Positive Friction Banner
	 * Platform:Android
	 */
	
	public static final By bannerCardCategory_And=By.xpath("//*[contains(@text,'Credit cards')]");
	public static final By bannerCardCategoryClick_And=By.xpath("//*[contains(@text,'4578')]");
	public static final By bannerCardCategorySpeaklabel_Android=By.xpath("//*[contains(@text,'Cards Category - New customer offer')]");
	public static final By bannerCardDashboardSpeaklabel_Android=By.xpath("//*[contains(@text,'Cards Dashboard - Festive Offer')]");
	public static final By banner_CCNo_Android=By.xpath("(//android.widget.Image)[10]");
	public static final By bannerCardCategorySpeaklabelActionButton_Android=By.xpath("//*[contains(@text,'Activate your CC Card Offers')]");
	public static final By bannerCardDashboardSpeaklabelActionButton_Android=By.xpath("//*[contains(@text,'Activate your festival offer')]");
	public static final By bannerCardsCategoryToDoMRCTitle_Android=By.xpath("//*[contains(@text,'Card dashboard - Apply Pay Later')]");
	public static final By bannerCardsCategoryToDoMRCDescription_Android=By.xpath("//*[contains(@text,'Apply now and get cash back on Quick Cash Offer')]");
	public static final By bannerCardDashboardToDoMRCActionButton_Android=By.xpath("//*[contains(@text,'Update CTA')]");
	public static final By bannerCardsCategorySPThumbnail_Android=By.xpath("(//android.widget.Image)[11]");
	public static final By bannerCardsCategorySPTitle_Android=By.xpath("//*[contains(@text,'EMGM Upsell offer Cards')]");
	public static final By bannerCardsCategorySPDescription_Android=By.xpath("//*[contains(@text,'Refer a friend and earn credits for LOP Offer')]");
	public static final By bannerCardsToDoTitle_Android=By.xpath("//*[contains(@text,'Card dashboard - Apply Pay Later')]");
	public static final By bannerCardsToDoDescriptionAndroid=By.xpath("//*[contains(@text,'Apply now and get cash back on Pay Later')]");
	public static final By bannerCardDashboardToDoActionButton_Android=By.xpath("//*[contains(@text,'Activate card [#UDF_ToDoCTA#]')]");//By.xpath("//*[contains(@text,'Offer right now')]");
	public static final By bannerCardsSPThumbnail_Android=By.xpath("(//android.widget.Image)[43]");
	public static final By bannerCardsSPTitle_Android=By.xpath("//*[contains(@text,'Quick Cash Offer')]");
	public static final By bannerCardsSPDescriptionAndroid=By.xpath("//*[contains(@text,'Here is a short description about SP Offer')]");
	public static final By bannerCardsTJThumbnail_Android=By.xpath("(//android.widget.Image)[46]");
	public static final By bannerCardsTJTitle_Android=By.xpath("//*[contains(@text,'EPP offer for this transaction')]");
	public static final By bannerCardsTJDescription_Android=By.xpath("//*[contains(@text,'TJ Card offer alignment test')]");
	public static final By bannerCardsXSellThumbnail_Android=By.xpath("(//android.widget.Image)[48]");
	public static final By bannerCardsXSellTitle_Android=By.xpath("//*[contains(@text,'X-GHOST Card Rent Offer')]");
	public static final By bannerCardsXSellDescription_Android=By.xpath("//*[contains(@text,'Ghost offer for a card')]");
	public static final By bannerBack_Android=By.xpath("(//android.widget.Image)[35]");
	
	
	public static final By bannerDepositCategory_Android=By.xpath("//android.view.View[contains(@text,'Deposits')]");
	public static final By bannerDepositCategorySpeaklabel_Android=By.xpath("//android.view.View[contains(@text,'Deposits category MRC Dashboard - Account level css')]");
	public static final By bannerDepositDashboardSpeaklabel_Android=By.xpath("//android.view.View[contains(@text,'CASA_TXN_Details - speak')]");
	public static final By banner_DepositAccNo_Android=By.xpath("//android.view.View[contains(@text,'9402')]");
	public static final By bannerDepositCategorySpeaklabelActionButton_Android=By.xpath("//android.widget.Button[contains(@text,'Activate card [#dynamic value#]')]");
	public static final By bannerDepositDashboardSpeaklabelActionButton_Android=By.xpath("//android.widget.Button[contains(@text,'Activate card')]");
	public static final By bannerDepositCategorySPTitle_Android=By.xpath("//*contains(@text,'Upsell Deposits eBRK')");
	public static final By bannerDepositCategorySPDescription_Android=By.xpath("//*contains(@text,'Its Cross category designed in MRC Dashboard,Its Cross category designed in MRC Dashboard')");
	public static final By bannerDepositSPTitle_Android=By.xpath("//*contains(@text,'This is an S&P upsell test')");
	public static final By bannerDepositSPDescriptionAndroid=By.xpath("//*contains(@text,'Here is the description content for the S and P upsell')");
	public static final By bannerDepositTJTitle_Android=By.xpath("//*contains(@text,'TJOFFER2')");
	public static final By bannerDepositTJDescription_Android=By.xpath("//*contains(@text,'This is an TJ Offer,Its TJ category designed')");
	public static final By bannerDepositXSellTitle_Android=By.xpath("//*contains(@text,'X-GHOST Deposit')");
	public static final By bannerDepositXSellDescription_Android=By.xpath("//*contains(@text,'This is an Ghost Offer,Its Ghost category [#dynamic#] designed in MRC Dashboard')");
	public static final By bannerDepositGhostXSellTitle_Android=By.xpath("//*contains(@text,'X-GHOST Deposit')");
	public static final By bannerDepositGhostXSellDescription_Android=By.xpath("//*contains(@text,'This is an Ghost Offer,Its Ghost category [#dynamic#] designed in MRC Dashboard')");

	public static final By bannerLoansCategory_Android=By.xpath("//android.view.View[contains(@text,'Loans')]");
	public static final By bannerLoansCategorySpeaklabel_Android=By.xpath("//android.view.View[contains(@text,'Loans Category - SPEAK')]");
	public static final By bannerLoansDashboardSpeaklabel_Android=By.xpath("//android.view.View[contains(@text,'Loan Dashboard -SPEAK')]");
	public static final By banner_LoansAccNo_Android=By.xpath("//android.view.View[contains(@text,'9979')]");
	public static final By bannerLoansCategorySpeaklabelActionButton_Android=By.xpath("//android.widget.Button[contains(@text,'Loans CTA')]");
	public static final By bannerLoansDashboardSpeaklabelActionButton_Android=By.xpath("//android.widget.Button[contains(@text,'LOAN CTA')]");
	public static final By bannerLoansCategorySPTitle_Android=By.xpath("//*contains(@text,'EMGM Loan UPSELL OFFER')");
	public static final By bannerLoansCategorySPDescription_Android=By.xpath("//*contains(@text,'Refer a friend and earn credits')");
	public static final By bannerLoansSPTitle_Android=By.xpath("//*contains(@text,'Upsell')");
	public static final By bannerLoansSPDescriptionAndroid=By.xpath("//*contains(@text,'LThis is an Upsell Offer,Its Upsell category designed in MRC Dashboard')");
	public static final By bannerLoansTJTitle_Android=By.xpath("//*contains(@text,'TJ LOANS')");
	public static final By bannerLoansTJDescription_Android=By.xpath("//*contains(@text,'This is an TJ Offer for Loans')");
	public static final By bannerLoansXSellTitle_Android=By.xpath("//*contains(@text,'X-GHOST Loan')");
	public static final By bannerLoansXSellDescription_Android=By.xpath("//*contains(@text,'This is a Ghost offer designed for loans')");
	
	public static final By delightX_Android=By.xpath("(//android.view.View[@text='X'])[1]");
	public static final By delightTitle_Android=By.xpath("//*[contains(@text,'The annual fee for your Citi ultima card has been waived!')]");
	public static final By delightThumbnail_Android=By.xpath("(//android.widget.Image)[4]");
	public static final By delightRightChevron_Android=By.xpath("(//android.widget.Image)[5]");
	public static final By delightDescriptionMRC_Android=By.xpath("//*[contains(@text,'View Details - MRC')]");
	public static final By delightDescriptionCards_Android=By.xpath("//*[contains(@text,'View Details - Cards')]");
	public static final By delightDescriptionRetail_Android=By.xpath("//*[contains(@text,'View Details - Deposits')]");
	public static final By delightCVPTitle_Android=By.xpath("//*[contains(@text,'2 line Product')]");
	public static final By delightCVPBackChevron_Android=By.xpath("(//android.widget.Image)[37]");
	public static final By delightWBVTitle_Android=By.xpath("//*[contains(@text,'Webview')]");
	public static final By delightWBVYesCTA_Android=By.xpath("//*[contains(@text,'Yes')]");
	public static final By delightWBVNoCTA_Android=By.xpath("//*[contains(@text,'No')]");
	public static final By delightWBVOKCTA_Android=By.xpath("//*[contains(@text,'Okay')]");
	public static final By delightWBVYesHeader_Android=By.xpath("//*[contains(@text,'Thank you')]");
	public static final By delightWBVYesBody_Android=By.xpath("//*[contains(@text,'We will be in touch shortly.')]");
	public static final By delightWBVNoHeader_Android=By.xpath("//*[contains(@text,'Thank you')]");
	public static final By delightWBVNoBody_Android=By.xpath("//*[contains(@text,'We will be in touch shortly.')]");
	public static final By delightWBVOKHeader_Android=By.xpath("//*[contains(@text,'Thank you')]");
	public static final By delightWBVOKBody_Android=By.xpath("//*[contains(@text,'We will be in touch shortly.')]");
	public static final By delightWBVErrorHeader_Android=By.xpath("//*[contains(@text,'Error Header')]");
	public static final By delightWBVErrorBody_Android=By.xpath("//*[contains(@text,'Error Body')]");
	public static final By delightWBVCloseCTA_Android=By.xpath("//*[contains(@text,'Close')]");
	public static final By delightSTPTitle_Android=By.xpath("//*[contains(@text,'Profile & Settings')]");
	public static final By delightSTPBackButton_Android=By.xpath("(//android.widget.ImageView)[1]");
	public static final By delightSTOTitle_Android=By.xpath("//*[contains(@text,'Something went wrong')]");
	public static final By delightSTOCloseButton_Android=By.xpath("//*[contains(@text,'Close')]");
	
	
	public static final By nextStepCardsCategory_Android=By.xpath("//*[contains(@text,'Credit cards')]");
	public static final By nextStepCCcardNo_Android=By.xpath("//*[contains(@text,'Octopus Credit Card')]");
	public static final By nextStepViewCardBenefits_Android=By.xpath("//*[contains(@text,'View card benefits')]");
	public static final By nextStepAppleWallet_Android=By.xpath("//*[contains(@text,'Add to Apple Wallet')]");
	public static final By nextStepPayall_Android=By.xpath("//*[contains(@text,'Pay bills with Payall')]");
	public static final By nextStepCVPTitle_Android=By.xpath("//*[contains(@text,'50% off your ?rst year premium!')]");
	public static final By nextStepCVPRemindCTA_Android=By.xpath("//*[contains(@text,'Remind')]");
	public static final By nextStepSTPTitle_Android=By.xpath("//*[contains(@text,'Big rewards with Citi PayAll')]");
	public static final By nextStepXCTA_Android=By.xpath("(//android.view.View)[48]");
	
	
	public static final By cardTrackerDesc_Android=By.xpath("//*[contains(@text,'Estimated arrival on 01-Dec-2025')]");
	public static final By cardTrackerNeedHelpCTALabel_Android=By.xpath("//*[contains(@text,'Need help?')]");
	public static final By cardTrackerCP_Android=By.xpath("//*[contains(@text,'CARD PRINTED')]");
	public static final By cardTrackerCD_Android=By.xpath("//*[contains(@text,'DISPATCHED')]");
	public static final By cardTrackerCA_Android=By.xpath("//*[contains(@text,'ARRIVED')]");
	public static final By cardTrackerSTPTitle_Android=By.xpath("//*[contains(@text,'Card settings')]");
	public static final By cardTrackerCVPTitle_Android=By.xpath("//*[contains(@text,'When will my card arrive?')]");
	public static final By cardTrackerCVPDismissCTA_Android=By.xpath("//*[contains(@text,'Dismiss')]");

	public static final By gmppGetMore_Android=By.xpath("//*[contains(@text,'Get More')]");
	public static final By gmppGetMoreHeader_Android=By.xpath("(//*[contains(@text,'Get More')])[2]");
	public static final By gmppPaleteeTitle_Android=By.xpath("//*[@text='Looking for Something Specific']");
	public static final By gmppCategory1Label_Android=By.xpath("//*[@text='Category1']");
	public static final By gmppCategory2Label_Android=By.xpath("//*[@text='Category2']");
	public static final By gmppCategory3Label_Android=By.xpath("//*[@text='Category3']");
	public static final By gmppCategory4Label_Android=By.xpath("//*[@text='Category4']");
	public static final By gmppCategory5Label_Android=By.xpath("//*[@text='Category5']");
	public static final By gmppCategory6Label_Android=By.xpath("//*[@text='Category6']");
	public static final By gmppViewAllProductLabel_Android=By.xpath("//*[@text='View all Products']");
	public static final By gmppWebviewTitle_Android=By.xpath("//*[contains(@text,'Page Title')]");
	public static final By gmppPriceTagIcon_Android=By.xpath("(//android.widget.Image)[52]");
	public static final By gmppCategory1Icon_Android=By.xpath("(//android.widget.Image)[53]");
	public static final By gmppCategory2Icon_Android=By.xpath("(//android.widget.Image)[54]");
	public static final By gmppCategory3Icon_Android=By.xpath("(//android.widget.Image)[55]");
	public static final By gmppCategory4Icon_Android=By.xpath("(//android.widget.Image)[56]");
	public static final By gmppCategory5Icon_Android=By.xpath("(//android.widget.Image)[57]");
	public static final By gmppCategory6Icon_Android=By.xpath("(//android.widget.Image)[58]");
	public static final By gmppWVBack_Android=By.xpath("(//android.widget.ImageView)");
	
	public static final By swfIcon_Android=By.xpath("(//android.widget.Image)[47]");
	public static final By swfTitle_Android=By.xpath("//*[@text='Tell your friends about flash sale']");
	public static final By swfDescription_Android=By.xpath("//*[contains(@text,'Share the joy')]");
	public static final By swfRightChevron_Android=By.xpath("(//android.widget.Image)[48]");
	
	
	public static final By atscActReplaceCard_Android=By.xpath("//*[@text='Activate Replacement Card']");
	public static final By atscYes_Android=By.xpath("//*[@text='Yes']");
	public static final By atscEnterCVVLabel_Android=By.xpath("//*[@text='Enter the CVV of your new card.']");
	public static final By atscCVV_Android=By.xpath("//*[@text='CVV']");
	public static final By atscActivate_Android=By.xpath("//*[@text='Activate']");
	public static final By atscYourCardLabel_Android=By.xpath("//*[@text='Your card is activated']");
	public static final By atscAddSuppCardLabel_Android=By.xpath("//*[@text='Add Supplementary Card']");
	public static final By atscAddSuppCardDescrip_Android=By.xpath("//*[@text='Enable or disable overseas ATM cash withdrawals and in-store purchases']");
	public static final By atscSTO3_Android=By.xpath("//*[@text='Something went wrong']");
	public static final By atscSTO4_Android=By.xpath("//*[@text='Please try again later']");
	public static final By atscSTO5_Android=By.xpath("//*[@text='Close']");
	public static final By atscSTO1_Android=By.xpath("(//android.widget.ImageView)[1]");
	public static final By atscSTO2_Android=By.xpath("(//android.widget.ImageView)[2]");

		
}

