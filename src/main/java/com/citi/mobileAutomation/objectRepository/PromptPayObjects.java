package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class PromptPayObjects {
	
	/*
	 * @Author:Dilipkumar
	 * Product :TH PromptPay
	 * Platform:Android
	 */
		
	public static final By promptpay_paymentsMenu=By.xpath("//*[@text='Payments']");
	public static final By promptpay_promptPayCTA=By.xpath("//*[contains(@text,'Promptpay')]");
	//100
	public static final By promptpay_transferToLabel_100=By.xpath("//*[@text='Transfer to']");
	public static final By promptpay_all_100=By.xpath("//*[@text='All']");
	public static final By promptpay_accountNo_100=By.xpath("//*[@text='Account no.']");
	public static final By promptpay_billPayment_100=By.xpath("//*[@text='Bill Payment']");
	public static final By promptpay_mobileNo_100=By.xpath("//*[@text='Mobile no.']");
	public static final By promptpay_search_100=By.xpath("//*[@text='Search']");
	public static final By promptpay_clearButton_100=By.xpath("//*[@text='Clear (x) button in Search box']");
	public static final By promptpay_newPayee_100=By.xpath("//*[@text='New payee']");
	public static final By promptpay_savedPayee_100=By.xpath("//*[@text='SAVED PAYEE']");
	public static final By promptpay_closeIcon_100=By.xpath("//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.ImageView");

	//300 default
	public static final By promptpay_newPayeeHeader_300=By.xpath("(//*[@text='New payee'])[2]");
	public static final By promptpay_newPayeeBackButton_300=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/headerLeftActionIcon']");
	public static final By promptpay_accNo_300=By.xpath("//*[@text='Account no. transfer']");
	public static final By promptpay_bankName_300=By.xpath("//*[@text='Bank Name']");
	public static final By promptpay_accountNumber_300=By.xpath("//*[@text='Account number']");
	
	public static final By promptpay_selbank_title_310=By.xpath("//*[@text='Recipient bank']");
	public static final By promptpay_selbank_citibank_310=By.xpath("//*[contains(@text,'Citibank')]");
	public static final By promptpay_selbank_hsbcbank_310=By.xpath("//*[contains(@text,'HSBC5101')]");
	
	//310
	public static final By promptpay_selectTransferTypeHeader_310=By.xpath("//*[@text='Select transfer type']");
	public static final By promptpay_mobileNumber_310=By.xpath("//*[@text='Mobile number']");
	public static final By promptpay_nationalID_310=By.xpath("//*[@text='National I.D']");
	public static final By promptpay_billPayment_310=By.xpath("//*[@text='Bill payment']");
	public static final By promptpay_accountNo_310=By.xpath("//*[@text='Account no. transfer']");
	public static final By promptpay_topup_310=By.xpath("//*[@text='Top-up PromptPay']");
	public static final By promptpay_dowmArrow_310=By.xpath("//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView");
		
	//300
	public static final By promptpay_selectedType_300=By.xpath("//*[@text='Top-up PromptPay']");
	public static final By promptpay_selectTypeDownArrow_300=By.id("rightImageView");
	public static final By promptpay_eWalletNo_300=By.xpath("//*[@text='e-Wallet No.']");
	public static final By promptpay_NextArrow_300=By.id("next_button");
	
	//200
	public static final By promptpay_amountLabel_200=By.id("tv_trans_amt_title");
	public static final By promptpay_ccy_200=By.id("tv_trans_amt_currency");
	public static final By promptpay_amountEditField_200=By.id("amountEditText");
	public static final By promptpay_fromAccountLabel_200=By.id("tv_title");
	public static final By promptpay_defaultSourceAccountName_200=By.id("tv_desc1");
	public static final By promptpay_availableBalance_200=By.id("tv_desc2");
	public static final By promptpay_nextArrow_200=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/next_button'])[1]");	
	public static final By promptpay_backButton_200=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/headerLeftActionIcon'])[1]");	
	public static final By promptpay_sourceAccountDownChevron_200=By.xpath("iv_right_icon");
	
	//210
	public static final By promptpay_transferFromLabel_210=By.xpath("//*[@text='Transfer from']");
	public static final By promptpay_downArrow_210=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/headerLeftActionIcon']");
	public static final By promptpay_cofSrcAccount_210=By.xpath("(//*[@text='Personal Checking Account **** XXXXXX2364'])[4]");
	public static final By promptpay_availableBalance_210=By.xpath("(//*[@text='Available balance THB 50,000.25'])[4]");
	
	//600
	public static final By promptpay_backButton_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/headerLeftActionIcon'])[2]");
	public static final By promptpay_reviewAndConfirm_600=By.xpath("//*[@text='Review & Confirm']");
	public static final By promptpay_close_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/headerRightActionIcon'])[2]");
	public static final By promptpay_amountLabel_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/tv_trans_amt_title'])[2]");
	public static final By promptpay_ccy_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/tv_trans_amt_currency'])[2]");
	public static final By promptpay_amount_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/amountEditText'])[2]");
	public static final By promptpay_fromLabel_600=By.xpath("//*[@text='From']");
	public static final By promptpay_selectedSrcAccount_600=By.xpath("(//*[@text='Personal Checking Account **** XXXXXX2364'])[1]");//(//*[@resource-id='com.citi.mobile.th.sit:id/tv_desc1'])[2]
	public static final By promptpay_toLabel_600=By.xpath("//*[@text='To']");
	public static final By promptpay_eWalletNo_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/tv_desc1'])[3]");
	public static final By promptpay_eWalletBank=By.xpath("//*[contains(@text,'AXIS')]");
	public static final By promptpay_saveAsPayeeList_600=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/tv_refresh_title']");
	public static final By promptpay_saveAsPayeeToggle_600=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/toggle_switch_1']");
	public static final By promptpay_nicknameLabel_600=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/noteText']");
	public static final By promptpay_nicknameValue_600=By.xpath("//*[@resource-id='com.citi.mobile.th.sit:id/editText1']");						
	public static final By promptpay_beneficiaryName_600=By.xpath("//*[@text='Beneficiary name']");
	public static final By promptpay_beneficiaryNameValue_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/tv_desc1'])[4]");
	public static final By promptpay_beneficiaryBankNameLabel_600=By.xpath("//*[@text='Beneficiary name in bank']");
	public static final By promptpay_beneficiaryBankNameValue_600=By.xpath("(//*[@resource-id='com.citi.mobile.th.sit:id/tv_desc1'])[5]");
	public static final By promptpay_when_600=By.xpath("//*[contains(@text,'When')]");
	public static final By promptpay_date_600=By.xpath("//*[contains(@text,'Today')]");
	public static final By promptpay_transferInstantly_600=By.xpath("//*[contains(@text,'Transfer will be made instantly')]");
	public static final By promptpay_payButton_600=By.id("component_progress_button_text_view");
	public static final By promptpay_disclaimer600=By.id("refreshImportantNoticeDesc");
	
	//700
	public static final By promptpay_transferSubmittedLabel_700=By.id("tv_transfer_submitted_lbl");
	public static final By promptpay_transferDescription_700=By.xpath("//*[contains(@text,'shortly')]");
	public static final By promptpay_transactionId_700=By.xpath("//*[contains(@text,'Transaction ID')]");
	public static final By promptpay_recipientDetailsLabel_700=By.xpath("//*[contains(@text,'Recipient Details')]");
	public static final By promptpay_recipientDetailsValue_700=By.xpath("//*[contains(@text,'AXIS |')]");
	public static final By promptpay_transferDateAndTypeLabel_700=By.xpath("//*[contains(@text,'Transfer date & type')]");
	public static final By promptpay_date_700=By.xpath("(//*[contains(@text,'Today')])[1]");
	public static final By promptpay_transferInstantly_700=By.xpath("//*[@text='Transfer will be made instantly']");
	public static final By promptpay_senderNameLabel_700=By.xpath("//*[contains(@text,'Sender name')]");
	public static final By promptpay_senderNameValue_700=By.xpath("//*[contains(@text,'RYAN COLLIN')]");
	public static final By promptpay_beneficiaryName_700=By.xpath("(//*[@text='Beneficiary name'])[1]");
	public static final By promptpay_beneficiaryNameValue_700=By.xpath("(//*[@text='Thai'])[1]");
	public static final By promptpay_beneficiaryBankNameLabel_700=By.xpath("(//*[@text='Beneficiary name in bank'])[1]");
	public static final By promptpay_beneficiaryBankNameValue_700=By.xpath("(//*[@text='CHERRY S'])[1]");
	public static final By promptpay_fromLabel_700=By.xpath("(//*[@text='From'])[1]");
	public static final By promptpay_selectedSrcAccount_700=By.xpath("(//*[@text='Personal Checking Account **** XXXXXX2364'])[1]");
	public static final By promptpay_saveToPayeeListAs_700=By.xpath("//*[@text='Saved to your payee list as']");
	public static final By promptpay_savedPayeeName_700=By.xpath("//*[@text='Johnson']");
	public static final By promptpay_share_700=By.xpath("//*[@text='Share this transaction']");
	public static final By promptpay_done_700=By.xpath("//*[@text='Done']");
	
			
	}
