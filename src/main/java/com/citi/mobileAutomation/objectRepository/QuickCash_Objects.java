package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

import com.citi.mobileAutomation.dataDriven.Xls_Reader;

public class QuickCash_Objects {
	
	/*
	 * @Author:Dilipkumar
	 * Functionality :Quick Cash 
	 * Platform:Android
	 */

	public static final By creditCard_Android=By.xpath("//*[contains(@text,'Credit Cards')]");
	public static final By loans_Android=By.xpath("//*[contains(@text,'Loans')]");
	public static final By quickCash_Android=By.xpath("//*[contains(@text,'Available Citi Quick Cash from your Credit Card')]");

	public static final By quickCash_200_Android=By.xpath("//*[@text='Citi Quick Cash']");
	public static final By next_Android=By.xpath("(//android.widget.Button)[5]");
	public static final By sendQuickCash_Android=By.xpath("//*[contains(@text,'Send Citi Quick Cash to')]");
	public static final By pt0Account_Android=By.xpath("//*[contains(@text,'7008')]");
	public static final By pt1Account_Android=By.xpath("(//*[contains(@text,'CITIBANK SINGAPORE LIMITED')])[1]");
	public static final By pt2Account_Android=By.xpath("(//*[contains(@text,'DEVELOPMENT BANK OF SINGAPORE LTD')])[1]");

	public static final By myAccountwithOtherBanks_Android=By.xpath("//*[contains(@text,'My account with other banks')]");
	public static final By otherBankAccount_Android=By.xpath("//*[@text='Other bank accounts']");
	public static final By bankName_Android=By.xpath("//android.widget.Button[contains(@text,'Bank name')]");
	public static final By accountNumber_Android=By.xpath("//android.view.View[4]/android.view.View[4]/android.view.View/android.view.View[3]");
	public static final By next_420_Android=By.xpath("(//android.widget.Button)[7]");
	
	public static final By reviewAndConfirm_Android=By.xpath("//*[contains(@text,'Review & Confirm')]");
	public static final By getCashNow_Android=By.xpath("//android.widget.Button[@text='Get cash now']");
	public static final By done_Android=By.xpath("//android.widget.Button[@text='Done']");
	
	//Quick cash 100	
	public static final By whatisQuickCash_Android=By.xpath("//*[contains(@text,'What is Citi Quick Cash')]");
	public static final By weConvert_Android=By.xpath("//*[contains(@text,'We convert your available credit limit into cash')]");
	public static final By yourBenefits_Android=By.xpath("//*[contains(@text,'YOUR BENEFITS')]");
	public static final By noDocumentLabel_Android=By.xpath("//*[contains(@text,'No document required')]");
	public static final By noDocumentIcon_Android=By.xpath("(//android.widget.Image)[44]");
	public static final By noDocumentDescription_Android=By.xpath("//*[contains(@text,'No credit checks, instant approval')]");
	public static final By hassleLabel_Android=By.xpath("//*[contains(@text,'Hassle-free process')]");
	public static final By hassleIcon_Android=By.xpath("(//android.widget.Image)[45]");
	public static final By hassleDescription_Android=By.xpath("//*[contains(@text,'Done in 60 secs')]");
	public static final By noProcessingLabel_Android=By.xpath("//*[contains(@text,'No processing fees')]");
	public static final By noProcessingIcon_Android=By.xpath("(//android.widget.Image)[46]");
	public static final By noProcessingDescription_Android=By.xpath("//*[contains(@text,'Zero hidden costs')]");
	public static final By get_Started_Android=By.xpath("//*[contains(@text,'Get Started')]");
	
	//Quick Cash 200
	public static final By amount_200_Android=By.xpath("//*[@text='Amount']");
	public static final By qcAmount_200_Android=By.xpath("//*[@text='25,000']");
	public static final By qcMin_200_Android=By.xpath("//*[@text='SGD 1,000']");
	public static final By qcMax_200_Android=By.xpath("//*[@text='SGD 25,000']");
	public static final By qcMRP_200_Android=By.xpath("//*[@text='Monthly repayment LOWEST SGD 101.10 for 60 Months']");
	public static final By qcNI_200_Android=By.xpath("//*[@text='Nominal Interest Rate 0% p.a. | E.I.R 12.99% p.a.']");
	public static final By qcQuestions_200_Android=By.xpath("//*[@text='Questions']");
	public static final By editButton_200_Android=By.xpath("(//android.widget.Image)[45]");
	
	//Quick Cash 600 Review and Confirm	
	public static final By qcLoanDetails_600_Android=By.xpath("//*[@text='LOAN DETAILS']");
	public static final By qcAmount_600_Android=By.xpath("(//*[@text='Amount'])[2]");
	public static final By qcLoanAmount_600_Android=By.xpath("(//*[@text='SGD 25,000.00'])[2]");
	public static final By qcMRP_600_Android=By.xpath("//*[@text='Monthly repayment']");
	public static final By qcMRPAmount_600_Android=By.xpath("//*[@text='SGD 101.10 for 60 Months']");
	public static final By qcNI_600_Android=By.xpath("(//*[@text='Nominal Interest Rate 0% p.a. | E.I.R 12.99% p.a.'])[2]");
	public static final By qcRS_600_Android=By.xpath("//*[@text='Repayment schedule']");
	public static final By qcAccDetails_600_Android=By.xpath("//*[@text='ACCOUNT DETAILS']");
	public static final By qcChargedTo_600_Android=By.xpath("//*[@text='Charged to']");
	public static final By qcChargedtoAcc_600_Android=By.xpath("//*[@text='Gold Card •••• 5175']");
	public static final By qcReceive_600_Android=By.xpath("//*[@text='Receive Citi Quick Cash at']");
	public static final By qcSelAcc600_Android=By.xpath("//*[@text='Citibank, ТЕКУЩИЙ СЧЕТ В РУБЛЯХ РФ •••• 7008']");
	public static final By qcTerms_600_Android=By.xpath("//*[@text='Terms and Conditions']");
	public static final By qcTap_600_Android=By.xpath("//*[@text='Tap \"Get cash now\" to proceed with your selected repayment plan, which will be subjected to']");
	
	//Quick Cash Repayment Schedule
	public static final By qcRPSDetails_RPS_Android=By.xpath("//*[@text='REPAYMENT DETAILS']");
	public static final By qcAmount_RPS_Android=By.xpath("//*[@text='Your Citi Quick Cash amount']");
	public static final By qcAmountValue_RPS_Android=By.xpath("(//*[@text='SGD 25,000.00'])[3]");
	public static final By qcAccount_RPS_Android=By.xpath("//*[@text='on Gold Card •••• 5175']");
	public static final By qcFMI_RPS_Android=By.xpath("//*[@text='First month instalment']");
	public static final By qcFMIV_RPS_Android=By.xpath("//*[@text='SGD 854.22']");
	public static final By qcSMI_RPS_Android=By.xpath("//*[@text='Subsequent monthly instalments']");
	public static final By qcSMIV_RPS_Android=By.xpath("//*[@text='SGD 22.37']");
	public static final By qcREPSC_RPS_Android=By.xpath("//*[@text='REPAYMENT SCHEDULE']");
	public static final By qcInstalment_RPS_Android=By.xpath("//*[@text='Instalment']");
	public static final By qcPrincipal_RPS_Android=By.xpath("//*[@text='Principal']");
	public static final By qcInterest_RPS_Android=By.xpath("//*[@text='Interest']");
	public static final By qcInstalmentValue_RPS_Android=By.xpath("//*[@text='1 of 60']");
	public static final By qcPrincipalValue_RPS_Android=By.xpath("//*[@text='SGD 12.16']");
	public static final By qcInterestValue_RPS_Android=By.xpath("//*[@text='SGD 842.06']");
	
	//Quick Cash 700 Success 
	public static final By qcAllDone_700_Android=By.xpath("//*[@text='All done!']");
	public static final By qcYourQuickCash_700_Android=By.xpath("//*[@text='Your Citi Quick Cash of SGD 25,000.00 should be available in your Citibank account instantly']");
	public static final By qcViewSummary_700_Android=By.xpath("//*[@text='View Summary']");
	public static final By qcFirstMI_700_Android=By.xpath("//*[@text='First month instalment']");
	public static final By qcFirstMIV_700_Android=By.xpath("//*[@text='SGD 854.22']");
	public static final By qcSMI_700_Android=By.xpath("//*[@text='Subsequent monthly instalments']");
	public static final By qcSMIV_700_Android=By.xpath("//*[@text='SGD 22.37']");
	public static final By qcRepayPeriod_700_Android=By.xpath("//*[@text='Repayment period']");
	public static final By qcRPSValue_700_Android=By.xpath("//*[@text='60 Months']");
	public static final By qcNIR_700_Android=By.xpath("//*[@text='Nominal Interest Rate']");
	public static final By qcNIRValue_700_Android=By.xpath("//*[@text='0% p.a. | E.I.R 12.99% p.a.']");
	public static final By qcRPS_700_Android=By.xpath("//*[@text='Repayment schedule']");
	
			
	}
