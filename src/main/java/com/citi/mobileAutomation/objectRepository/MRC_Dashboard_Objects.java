package com.citi.mobileAutomation.objectRepository;

import org.openqa.selenium.By;

public class MRC_Dashboard_Objects {
	
	/*
	 * @Author:Dilipkumar
	 * Functionality :MRC Dashboard
	 * Platform:Android
	 */
	
    public static final By depositsCategoryLabel_And= By.xpath("//*[contains(@text,'Deposits')]");
    public static final By depositsCategoryAmount_And= By.xpath("//*[contains(@text,'PHP 1,023,156.92')]"); 
    public static final By depositCategoryDownChevron_And= By.xpath("(//android.widget.Image)[4]");
    
	public static final By checkingsAccountName_And= By.xpath("(//*[contains(@text,'Checking')])[2]");
	public static final By checkingsAccountNumber_And= By.xpath("//*[contains(@text,'3026')]");
	public static final By checkingsAccountBalanceCCY_And= By.xpath("//*[contains(@text,'PHP 9,475.92')]");
	public static final By checkingsAccountBalanceValue_And = By.xpath("//*[contains(@text,'PHP 9,475.92')]"); 
	public static final By checkingsRightChevron = By.xpath("(//android.widget.Image)[6]"); 

	public static final By savingsAccountName_And= By.xpath("(//*[contains(@text,'Savings')])[1]");
	public static final By savingsAccountNumber_And= By.xpath("(//*[contains(@text,'2955')])[1]");
	public static final By savingsAccountBalanceCCY_And= By.xpath("//*[contains(@text,'AUD 106,483.06')]");
	public static final By savingsAccountBalanceValue_And = By.xpath( "//*[contains(@text,'AUD 106,483.06')]"); 
	public static final By savingsRightChevron = By.xpath("(//android.widget.Image)[17]");  
	
	public static final By timedepositAccountName_And= By.xpath("//*[contains(@text,'Time Deposit')]");
	public static final By timedepositAccountNumber_And= By.xpath("//*[contains(@text,'6846')]");
	public static final By timedepositAccountBalanceCCY_And= By.xpath("//*[contains(@text,'PHP 11,546.30')]");
	public static final By timedepositAccountBalanceValue_And = By.xpath("//*[contains(@text,'PHP 11,546.30')]"); 
	public static final By timedepositRightChevron = By.xpath("(//android.widget.Image)[7]");  
	
	public static final By creditCardCategoryLabel_And= By.xpath("//*[contains(@text,'Credit Cards')]");
	public static final By creditCardCategoryAmount_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[1]"); 
	public static final By creditCardCategoryDownChevron_And= By.xpath("(//android.widget.Image)[22]");

	public static final By creditCardName_And= By.xpath("(//*[contains(@text,'Gold Card')])[2]");
	public static final By creditCardNumber_And= By.xpath("(//*[contains(@text,'5175')])[2]");
	public static final By creditCardBalanceCCY_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[2]");
	public static final By creditCardBalanceValue_And = By.xpath("(//*[contains(@text,'SGD 0.00')])[2]"); 
	public static final By creditCardRightChevron = By.xpath("(//android.widget.Image)[23]");  
	public static final By creditCardPayAllTitle = By.xpath("//*[contains(@text,'Citi PayAll')]"); 
	public static final By creditCardPayAllDesc = By.xpath("//*[contains(@text,'Earn Miles or Points when you pay for your rent, education, taxes and bills')]"); 
	public static final By creditCardPayAllRightChevron = By.xpath("(//android.widget.Image)[27]"); 
	
    public static final By investmentsCategoryLabel_And= By.xpath("//*[contains(@text,'Investments')]"); 
    public static final By investmentsCategoryAmount_And= By.xpath("//*[(@text='SGD 3.43')]");  
    public static final By investmentsCategoryDownChevron_And= By.xpath("(//android.widget.Image)[28]");
	
	public static final By mfAccountName_And= By.xpath("(//*[contains(@text,'Unit Trusts')])[1]");
	public static final By mfAccountNumber_And= By.xpath("(//*[contains(@text,'6001')])[2]");
	public static final By mfAccountBalanceCCY_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[3]");
	public static final By mfAccountBalanceValue_And = By.xpath("(//*[contains(@text,'SGD 0.00')])[3]"); 
	public static final By mfightChevron = By.xpath("(//android.widget.Image)[31]"); 
	
	public static final By bondsAccountName_And= By.xpath("(//*[contains(@text,'Brokerage')])[1]");
	public static final By bondsAccountNumber_And= By.xpath("//*[contains(@text,'6006')]");
	public static final By bondsAccountBalanceCCY_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[6]");
	public static final By bondsAccountBalanceValue_And = By.xpath("(//*[contains(@text,'SGD 0.00')])[6]"); 
	public static final By bondsRightChevron = By.xpath("(//android.widget.Image)[34]"); 
	
    public static final By loansCategoryLabel_And= By.xpath("//*[contains(@text,'Loans')]");
    public static final By loansCategoryAmount_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[1]"); 
    public static final By loansCategoryDownChevron_And= By.xpath("(//android.widget.Image)[34]");
	
	public static final By housingLoanAccountName_And= By.xpath("//*[contains(@text,'Housing Loan')]");
	public static final By housingLoanAccountNumber_And= By.xpath("//*[contains(@text,'7526')]");
	public static final By housingLoanAccountBalanceCCY_And= By.xpath("(//*[contains(@text,'SGD 0.00')])[2]");
	public static final By housingLoanAccountBalanceValue_And = By.xpath("(//*[contains(@text,'SGD 0.00')])[2]"); 
	public static final By housingLoanRightChevron = By.xpath("(//android.widget.Image)[35]"); 
	

	
	
}
