package com.citi.mobileAutomation.autoInvoking;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class AndroidCalcAppExample {
	
	WebDriver driver;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;
    
	@Test
	public void setUp() throws MalformedURLException, InterruptedException {
		
        appiumService = AppiumDriverLocalService.buildDefaultService();
		appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        System.out.println("Appium Service Address : - "+ appiumServiceUrl);
        
     /* Thread.sleep(2000);
		
        DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName","emulator-5554");
		capabilities.setCapability(CapabilityType.VERSION, "8.1.0");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appPackage", "com.citi.mobile.au.dit");
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability("appActivity", "com.citi.mobile.pt3.starter.authentication.view.activity.LoginActivity");
		
		driver = new AndroidDriver(new URL(appiumServiceUrl), capabilities);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); */
	}


	@Test
	public void End() {
		appiumService = AppiumDriverLocalService.buildDefaultService();
		System.out.println("Stop driver");
//		driver.quit();
		System.out.println("Stop appium service");
		appiumService.stop();
	}

}
