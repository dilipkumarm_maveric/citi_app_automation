package com.citi.mobileAutomation.capabilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.citi.mobileAutomation.autoInvoking.InvokingEmulator;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.reusableFunctions.CommonMethods;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class DesiredCap {
	public static AppiumDriver driver;
	private static AppiumServiceBuilder builder;

	protected static AppiumDriverLocalService service;

	public static void intializeAndroid() throws InterruptedException, IOException {

		Xls_Reader.readPreReqMethod();
		InvokingEmulator.startAvd();
		System.out.println("" + Xls_Reader.deviceName + "");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "" + Xls_Reader.deviceName + "");
		capabilities.setCapability(CapabilityType.VERSION, "" + Xls_Reader.platformVersion + "");
		capabilities.setCapability("platformName", "" + Xls_Reader.platformName + "");
		capabilities.setCapability("appPackage", "" + Xls_Reader.appPackage + "");
		capabilities.setCapability("autoGrantPermissions", true);
		// capabilities.setCapability("noReset", false);
		capabilities.setCapability("appActivity", "" + Xls_Reader.appActivity + "");
		capabilities.setCapability("app", "" + Xls_Reader.APK_PATH + "");
		//capabilities.setCapability("androidInstallTimeout", 150000);
		capabilities.setCapability("autoGrantPermissions", true);
		driver = new AppiumDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		CommonMethods.wait = new WebDriverWait(DesiredCap.driver, 1000);
		System.out.println("Connected");

	}

	public static void initilizeiOS() throws MalformedURLException {
		
		Xls_Reader.readPreReqMethod();
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "" + Xls_Reader.deviceNameIOS + "");
		caps.setCapability("platformVersion", "" + Xls_Reader.platformVersionIOS + "");
		caps.setCapability("platformName", "" + Xls_Reader.platformNameIOS + "");
		caps.setCapability("bundleId", "" + Xls_Reader.bundleID + "");
		caps.setCapability("skipUnlock", "true");
		caps.setCapability("udid", "" + Xls_Reader.udidIOS + "");
		caps.setCapability("newCommandTimeout", 60 * 50);
		caps.setCapability("autoAcceptAlerts", true);
		caps.setCapability("noReset", "false");
		caps.setCapability("app", "" + Xls_Reader.APP_PATH + "");
		//Below ones comes from appPathFromCmd
		//caps.setCapability("app", "" + Xls_Reader.appPathFromCmd+ "");	
		driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		CommonMethods.wait = new WebDriverWait(DesiredCap.driver, 1000);

	}

	public static void main(String[] args) throws InterruptedException, IOException {

		intializeAndroid();
	}
}
