package com.citi.mobileAutomation.dataDriven;

import java.io.File;

import com.codoid.products.exception.FilloException;

public class DirectoryForReportAndScreenshots {
	
	
	 public static File file;
	
	public static void createDirectory() throws FilloException  {
	      
		//path comes from mvn cmd
		 // Xls_Reader.readReportPathFromMvn();
		  
		 //path comes from Excel
		   Xls_Reader.readReportPath();
		  
		   Xls_Reader.reportPath=Xls_Reader.reportPath+"//"+"Consolidated_Report";
		   
	    
	     file = new File(Xls_Reader.reportPath);
	      //Creating the directory for report
	      boolean bool1 = file.mkdir();
	      if(bool1){
	         System.out.println("Directory created successfully");
	         Xls_Reader.reportPath=Xls_Reader.reportPath+"//";
	         System.out.println(Xls_Reader.reportPath);
	      }
	      else
	      {
	    	  System.out.println("Directory not created ");
	    	  Xls_Reader.reportPath=Xls_Reader.reportPath+"//";
	    	  System.out.println(Xls_Reader.reportPath);
	      }
	      
	      
	      
	    //Creating the directory for screenshots
	      Xls_Reader.screenshotPath=Xls_Reader.screenshotPath+"//"+"ScreenShotsFolder";
	      file = new File(Xls_Reader.screenshotPath);
	      boolean bool2 = file.mkdir();
	      if(bool2){
	         System.out.println("Directory created successfully Screen");
	         Xls_Reader.screenshotPath=Xls_Reader.screenshotPath+"//";
	         System.out.println(Xls_Reader.screenshotPath);
	      }
	      else
	      {
	    	  System.out.println("Directory not created screen ");
	    	  Xls_Reader.screenshotPath=Xls_Reader.screenshotPath+"//";
	    	  System.out.println(Xls_Reader.screenshotPath);
	      }
	     
	   }
	public static void main(String[] args) throws FilloException {
		createDirectory();
	}
	

}
