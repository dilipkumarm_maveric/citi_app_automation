package com.citi.mobileAutomation.dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xls_Reader_Path {

	public String path = Xls_Reader_Key_Value.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
	public FileInputStream fis = null;
	public FileOutputStream fileOut = null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;

	public Xls_Reader_Path() {

	}

	public Xls_Reader_Path(String path) {
		this.path = path;
		try {
			fis = new FileInputStream(new File(path));
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(0);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String mapReturnStringForKey(String key, String sheetName) throws IOException {
		Xls_Reader_Path read = new Xls_Reader_Path();
		System.out.println(read.path);
		read = new Xls_Reader_Path(read.path);
		Map<String, String> map = read.setMapData(sheetName);
		String value = map.get(key);
		return value;
	}

	public Map<String, String> setMapData(String sheetName) throws IOException {
		Map<String, String> excelFileMap = new HashMap<String, String>();
		fis = new FileInputStream(path);
		workbook = new XSSFWorkbook(fis);
		sheet = workbook.getSheet(sheetName);
		int rowNum = sheet.getLastRowNum();
		for (int i = 0; i <= rowNum; i++) {
			String key = sheet.getRow(i).getCell(0).toString().trim();
			Cell cell = sheet.getRow(i).getCell(1);
			String value = "";
			cell.setCellType(Cell.CELL_TYPE_STRING);
			value = "" + cell.getStringCellValue().toString();
			try {
				excelFileMap.put(key, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return excelFileMap;
	}

	public static void main(String[] args) throws IOException {
		try {
			String value = mapReturnStringForKey("ENTERLOGINUSERNAME_PAYMENTS_XPATH", "TEST_DATA");
			System.out.println(value);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}