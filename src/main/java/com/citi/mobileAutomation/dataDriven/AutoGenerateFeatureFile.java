package com.citi.mobileAutomation.dataDriven;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class AutoGenerateFeatureFile {
	
	static Fillo fillo=new Fillo();
	static Connection connection;
	static Recordset recordset=null;
	public static String xlsPath="";
	
	public static String scenarioName="";
	public static String scenarioTagName="";
	public static String stepKeyword="";
	public static String stepDescription="";
	public static int count=0;
	
	public static void readTestSteps()
	{
		try 
		{
			File fileFeature = new File("/Users/apple/Desktop/Babu/SDET_Cucumber_Fillo_Framework/com.citi.cucumber.fillo/Resources/TWA_iPadAir/AutoFeat.feature");
			FileWriter fwrite = new FileWriter(fileFeature,true);
			BufferedWriter bwrite= new BufferedWriter(fwrite);  
	          //Create the file
	          if (fileFeature.createNewFile())
	          {
	            System.out.println("File is created!");
	          }
	          else
	          {
	            System.out.println("File already exists.");
	          }
			
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection=fillo.getConnection(xlsPath);
			
			
			String strQuery="Select * from TEST_CASES WHERE Mandatory='Yes'";
			recordset=connection.executeQuery(strQuery);
			 
			while(recordset.next())
			{
				if(count==0)
				{
					if(recordset.getField("Feature Tag").toString().equalsIgnoreCase("Yes"))
					{
						bwrite.append("@all");
						bwrite.newLine();
						bwrite.append("Feature: "+recordset.getField("Feature Name"));
						bwrite.newLine();
						
						count++;
					}
					else
						System.out.println("No feature tag");
				}
				
					if(recordset.getField("Scenario Tag Required").toString().equalsIgnoreCase("Yes"))
					{
						bwrite.append("@"+recordset.getField("Scenario Tag Name"));
						bwrite.newLine();
					}
					else
						System.out.println("scenario tag name NOT found");
					
					if(! recordset.getField("Scenario Name").toString().isEmpty())
					{
						bwrite.append("Scenario: "+recordset.getField("Scenario Name"));
						bwrite.newLine();
						
					}
					else
						System.out.println("scenario name NOT found");
					
					bwrite.append(recordset.getField("Steps")+" "+recordset.getField("TestCases"));
					bwrite.newLine();
					bwrite.flush();
			
			}
			recordset.close();
			connection.close();
			bwrite.close();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public static void main(String[] args) {
		readTestSteps();
		
	}
				 

}
