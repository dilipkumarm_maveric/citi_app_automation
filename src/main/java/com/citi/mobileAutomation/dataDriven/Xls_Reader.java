package com.citi.mobileAutomation.dataDriven;

import java.io.IOException;

import org.junit.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Xls_Reader {

	public static String deviceNameIOS = "";
	public static String platformNameIOS = "";
	public static String platformVersionIOS = "";
	public static String udidIOS = "";
	public static String bundleID = "";
	public static String APP_PATH = "";

	public static String deviceName = "";
	public static String platformName = "";
	public static String platformVersion = "";
	public static String appPackage = "";
	public static String appActivity = "";

	public static String platformToBeTested = "";

	public static String AVD_NAME = "";
	public static String AVD_PATH = "";
	public static String AVD_Emulator_PATH = "";
	public static String APK_PATH = "";

	public static String xlsPath = "";

	public static String currentPassword = "";
	public static String userID = "";
	public static String password = "";
	public static String OTP = "";
	public static String cus_Number = "";
	
	public static String acct_Number = "";
	public static String loan_amount = "";
	public static String pt0_customer_no = "";
	public static String pt1_bank_name = "";
	public static String pt2_bank_name = "";
	public static String other_bank_acc_no = "";
	
	public static String GW_TURN_ON = "";
	public static String Add_Dolar="";
	public static String Select_CCY="";
	public static String Valid_Dollar="";
	
	public static String Cards_ProductName ="";
	public static String Cards_DisplayAccNo ="";
	public static String Cards_CurrencyCode = "";
	public static String Cards_Amount ="";
	public static String Cards_L2Amount ="";
	public static String CurrentBalance_Amount ="";
	public static String CardsStatement_Amount = "";
	public static String AvailableCredit_Amount ="";
	public static String QuickCash_Amount ="";
	public static String SeeTransaction_Amount = "";
	public static String CardsStatement_L3Amount = "";
	
	public static String Deposit_CheckingProductName ="";
	public static String Deposit_CheckingDisplayAccNo ="";
	public static String Deposit_CheckingCurrencyCode = "";
	public static String Deposit_TotalBalance ="";
	public static String Deposit_AvailableBalance ="";
	public static String Deposit_StatementDate = "";
	public static String Deposit_LastTransactionDate ="";
	public static String Deposit_LastTransactionAmount = "";
	
	
	public static String Investments_MFProductName ="";
	public static String Investments_MFDisplayAccNo ="";
	public static String Investments_TotalAmount ="";
	public static String Investments_MFCurrentBalance ="";
	public static String Investments_MFUnrealisedAmount ="";
	public static String Investments_MFInvestedAmount ="";
	public static String Investments_MFTotalAmount ="";

	
	public static String Loans_HLProductName ="";
	public static String Loans_HLDisplayAccNo ="";
	public static String Loans_HLCurrencyCode = "";
	public static String Loans_TotalAmount ="";
	public static String Loans_OutstandingAmount ="";
	public static String Loans_NextPaymentAmount = "";
	public static String Loans_NextPaymentDate = "";

	public static String reportPath = "";
	public static String screenshotPath = "";

	static Fillo fillo = new Fillo();
	static Connection connection;
	static Recordset recordset = null;
	
	public static String appPathFromCmd = "";
	
	
	public static void readReportPathFromMvn() throws FilloException {
	
		appPathFromCmd=System.getProperty("apk_PATH");
		
		reportPath=System.getProperty("report_Path");

		screenshotPath=System.getProperty("screen_Shot_Path");
	
	}

	public static void readReportPath() throws FilloException {
		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String strQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Report_Location'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				reportPath = recordset.getField("VALUES");
				System.out.println(reportPath);
				break;
			}

			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String screenQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Screenshots_Location'";
			recordset = connection.executeQuery(screenQuery);

			while (recordset.next()) {
				screenshotPath = recordset.getField("VALUES");
				System.out.println(screenshotPath);
				break;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void readPreReqMethod() {

		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String userQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='UserID'";
			recordset = connection.executeQuery(userQuery);

			while (recordset.next()) {
				userID = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String passwordQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='Password'";
			recordset = connection.executeQuery(passwordQuery);

			while (recordset.next()) {
				password = recordset.getField("VALUES");

			}

			 xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			 connection = fillo.getConnection(xlsPath);
			 String acctNumQuery = "Select * from Epp_CardNumber WHERE PARAMETERS ='Epp_CardNum'";
			 recordset = connection.executeQuery(acctNumQuery);

			 while (recordset.next()) {
				 acct_Number = recordset.getField("VALUES");

			 }

			recordset.close();
			connection.close();

		} catch (Exception e) {

		}
		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String loanAmountQuery = "Select * from QC_DATA WHERE PARAMETERS='Loan_Amount'";
			recordset = connection.executeQuery(loanAmountQuery);

			while (recordset.next()) {
				loan_amount = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String pt0customernoQuery = "Select * from QC_DATA WHERE PARAMETERS='PT0_Customer_Number'";
			recordset = connection.executeQuery(pt0customernoQuery);

			while (recordset.next()) {
				pt0_customer_no = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String pt1banknameQuery = "Select * from QC_DATA WHERE PARAMETERS='PT1_Bank_Name'";
			recordset = connection.executeQuery(pt1banknameQuery);

			while (recordset.next()) {
				pt1_bank_name = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String pt2banknameQuery = "Select * from QC_DATA WHERE PARAMETERS='PT2_Bank_Name'";
			recordset = connection.executeQuery(pt2banknameQuery);

			while (recordset.next()) {
				pt2_bank_name = recordset.getField("VALUES");

			}
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String otherbankaccnoQuery = "Select * from QC_DATA WHERE PARAMETERS='Other_Bank_Acc_Number'";
			recordset = connection.executeQuery(otherbankaccnoQuery);

			while (recordset.next()) {
				other_bank_acc_no = recordset.getField("VALUES");

			}
			recordset.close();
			connection.close();

		} catch (Exception e) {

		}
		try {
			xlsPath = CommomReadFunction.returnPreviousFolderOfProjectDirectory("/Data_table_SG.XLSX");
			connection = fillo.getConnection(xlsPath);
			String strQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformToBeTested'";
			recordset = connection.executeQuery(strQuery);

			while (recordset.next()) {
				platformToBeTested = recordset.getField("VALUES");

			}

			if (platformToBeTested.equalsIgnoreCase("Android")) {

				String deviceQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='deviceName'";
				recordset = connection.executeQuery(deviceQuery);
				while (recordset.next()) {

					deviceName = recordset.getField("values");

				}

				String platformQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformName'";
				recordset = connection.executeQuery(platformQuery);
				while (recordset.next()) {

					platformName = recordset.getField("values");

				}

				String platformVersionQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformVersion'";
				recordset = connection.executeQuery(platformVersionQuery);
				while (recordset.next()) {

					platformVersion = recordset.getField("values");

				}

				String actQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='appActivity'";
				recordset = connection.executeQuery(actQuery);
				while (recordset.next()) {
					System.out.println(recordset.getField("values"));
					appActivity = recordset.getField("values");

				}
				String packQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='appPackage '";
				recordset = connection.executeQuery(packQuery);
				while (recordset.next()) {

					appPackage = recordset.getField("values");

				}

				String avdNameQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_NAME'";
				recordset = connection.executeQuery(avdNameQuery);
				while (recordset.next()) {

					AVD_NAME = recordset.getField("values");

				}
				String avdPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_PATH'";
				recordset = connection.executeQuery(avdPathQuery);
				while (recordset.next()) {

					AVD_PATH = recordset.getField("values");
				}

				String avdEmuPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='AVD_Emulator_PATH'";
				recordset = connection.executeQuery(avdEmuPathQuery);
				while (recordset.next()) {

					AVD_Emulator_PATH = recordset.getField("values");
				}
				
				String apkPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='APK_PATH'";
				recordset = connection.executeQuery(apkPathQuery);
				while (recordset.next()) {

					APK_PATH = recordset.getField("values");
				}
				
				String apkTurnOn = "Select * from Global_wallet WHERE PARAMETERS='Turn_On_Flow'";
				recordset = connection.executeQuery(apkTurnOn);
				while (recordset.next()) {

					GW_TURN_ON = recordset.getField("values");
					
				}
				
				String AddDollarWallet = "Select * from Global_wallet WHERE PARAMETERS='Add_Dollar_GW'";
				recordset = connection.executeQuery(AddDollarWallet);
				while (recordset.next()) {

					Add_Dolar = recordset.getField("values");
				}
				
				String SelectCurrencyA = "Select * from Global_wallet WHERE PARAMETERS='Selectcurrency'";
				recordset = connection.executeQuery(SelectCurrencyA);
				while (recordset.next()) {

					Select_CCY = recordset.getField("values");
				}
				
				String validatecurrency = "Select * from Global_wallet WHERE PARAMETERS='ValidateCurrency'";
				recordset = connection.executeQuery(validatecurrency);
				while (recordset.next()) {

					Valid_Dollar = recordset.getField("values");
				}

				recordset.close();
				connection.close();
			}

			else {

				String deviceQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='deviceNameIOS'";
				recordset = connection.executeQuery(deviceQuery);
				while (recordset.next()) {

					deviceNameIOS = recordset.getField("values");

				}

				String platformQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformNameIOS'";
				recordset = connection.executeQuery(platformQuery);
				while (recordset.next()) {

					platformNameIOS = recordset.getField("values");

				}

				String platformVersionQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='platformVersionIOS'";
				recordset = connection.executeQuery(platformVersionQuery);
				while (recordset.next()) {

					platformVersionIOS = recordset.getField("values");

				}

				String actQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='udidIOS'";
				recordset = connection.executeQuery(actQuery);
				while (recordset.next()) {

					udidIOS = recordset.getField("values");

				}
				String packQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='bundleID'";
				recordset = connection.executeQuery(packQuery);
				while (recordset.next()) {

					bundleID = recordset.getField("values");

				}
				String appPathQuery = "Select * from PRE_REQ_FOR_TEST WHERE PARAMETERS='APP_PATH'";
				recordset = connection.executeQuery(appPathQuery);
				while (recordset.next()) {

					APP_PATH = recordset.getField("values");

				}
				recordset.close();
				connection.close();
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void main(String[] args) throws IOException, FilloException {
		readPreReqMethod();
	}

}