package com.citi.mobileAutomation.reusableFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.Interaction;
import org.openqa.selenium.interactions.Pause;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.PointerInput.Kind;
import org.openqa.selenium.interactions.PointerInput.MouseButton;
import org.openqa.selenium.interactions.PointerInput.Origin;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.citi.mobileAutomation.capabilities.DesiredCap;
import com.citi.mobileAutomation.dataDriven.Xls_Reader;
import com.citi.mobileAutomation.reportGeneration.ExtentReport;
import com.google.common.collect.ImmutableList;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class CommonMethods {

	public static AppiumDriver driver;

	public static WebDriverWait wait;


	// ==================New Resuable functions===================================
	/*
	 * To send text to any field This function needs two parameters Element locator
	 * i.e xpath and Text you want to pass If element isn't found,It will wait 10sec
	 * to find the element
	 */

	public static void sendKeysTextBox(By by, String text) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
		ele.sendKeys(text);

	}

	public static void sendKeysTextBoxMultipleSession(By by, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.sendKeys(text);
			ExtentReport.getResult();
		}

		else {
			System.out.println("Element is Absent");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;

		}
	}

	/*
	 * To send text to any field which already has some text This method will first
	 * clear the field, then it will send text This function needs two parameters
	 * Element locator i.e xpath and Text you want to pass If element isn't found,It
	 * will wait 10sec to find the element
	 */

	public static void clearAndSendKeys(By by, String text) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		ele.clear();
		ele.sendKeys(text);
	}

	public static void clearAndSendKeysMultipleSession(By by, String text) throws Exception {
		Thread.sleep(5000);

		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.clear();
			ele.sendKeys(text);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Preesent");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;

		}
	}

	/*
	 * To click on any element This function needs one parameters Element locator
	 * i.e xpath If element isn't found,It will wait 10sec to find the element
	 */

	public static void clickMethod(By by) throws IOException {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
		ele.click();
	}

	public static void clickMethodMultipleSession(By by) throws Exception {
		Thread.sleep(5000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.click();
			Thread.sleep(5000);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
			int x = 5;
			x = x / 0;
		}

	}

	/*
	 * To Check whether the element is present or not This function needs one
	 * parameters Element locator i.e xpath If element isn't found,It will wait
	 * 10sec to find the element
	 */

	public static void isElementPresent(By by) throws Exception {
		Thread.sleep(5000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();

		}

	}
	public static boolean isElementPresentCondition(By by) throws Exception {
		Thread.sleep(5000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			ExtentReport.getResult();
			return true;
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "pass";
			ExtentReport.getResult();
			return false;
		}

	}
	public static void VerifyingAccountsAndroidMRC(String accNumber, String productName, String accNumber1,
			String productName1) {
		String getAcc = driver.findElementByXPath("//*[contains(@text,'" + accNumber + "')]").getAttribute("text");
		System.out.println("Get Acc :" + getAcc);
		if (getAcc.contains(productName)) {
			boolean b = getAcc.contains(accNumber1);
			System.out.println("Product name:" + b);
			boolean b1 = getAcc.contains(productName1);
			System.out.println("Display Account No:" + b1);
			System.out.println("Assertion Passed");
		} else {
			System.out.println("Not Found");

		}

	}

	public static void VerifyingAccountsClickAndroid(String accNumber){
		DesiredCap.driver.findElementByXPath("//*[contains(@text,'" + accNumber + "')]").click();

	}
	
	public static void VerifyingAccountsClickiOS(String accNumber){
		DesiredCap.driver.findElementByXPath("//*[contains(@name,'" + accNumber + "')]").click();

	}
	public static void VerifyingAccountsAndroid(String accNumber,String productName,String accNumber1,String productName1)
    {
	   System.out.println("Excel Read Values:"+accNumber+","+productName+","+accNumber1+","+productName1);
	   String getAcc=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").getAttribute("text");
		System.out.println("Get Acc :"+getAcc);
		if(getAcc.contains(productName))
		{
			boolean b= getAcc.contains(accNumber1);
			System.out.println("Product name:"+b);
			boolean b1= getAcc.contains(productName1);
			System.out.println("Display Account No:"+b1);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsCCYAndroid(String totalCurrency)
    {
	   System.out.println("Excel Read Values:"+totalCurrency+",");
	   String getCCY=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+totalCurrency+"')]").getAttribute("text");
		System.out.println("Get Total Deposit :"+getCCY);
		if(getCCY.contains(totalCurrency))
		{
			boolean b= getCCY.contains(totalCurrency);
			System.out.println("Total currency:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsAvlbBalAndroid(String availableCurrency)
    {
	   System.out.println("Excel Read Values:"+availableCurrency+",");
	   String getAvlbBal=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+availableCurrency+"')]").getAttribute("text");
		System.out.println("Get Available Balance :"+getAvlbBal);
		if(getAvlbBal.contains(availableCurrency))
		{
			boolean b= getAvlbBal.contains(availableCurrency);
			System.out.println("Available currency:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsStmtDateAndroid(String statementDate)
    {
	   System.out.println("Excel Read Values:"+statementDate+",");
	   String getStmtDate=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+statementDate+"')]").getAttribute("text");
		System.out.println("Get Statement Date :"+getStmtDate);
		if(getStmtDate.contains(statementDate))
		{
			boolean b= getStmtDate.contains(statementDate);
			System.out.println("Statement Date:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsLastTransDateAndroid(String lastTransDate)
    {
	   System.out.println("Excel Read Values:"+lastTransDate+",");
	   String getLastTransDate=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+lastTransDate+"')]").getAttribute("text");
		System.out.println("Get Last Transaction Date :"+getLastTransDate);
		if(getLastTransDate.contains(lastTransDate))
		{
			boolean b= getLastTransDate.contains(lastTransDate);
			System.out.println("Last Transaction Date:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsLastTransAmountAndroid(String lastTransAmount)
    {
	   System.out.println("Excel Read Values:"+lastTransAmount+",");
	   String getLastTransAmt=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+lastTransAmount+"')]").getAttribute("text");
		System.out.println("Get Last Transaction Date :"+getLastTransAmt);
		if(getLastTransAmt.contains(lastTransAmount))
		{
			boolean b= getLastTransAmt.contains(lastTransAmount);
			System.out.println("Last Transaction Date:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingProductNameAndroid(String productName)
    {
	   System.out.println("Excel Read Values:"+productName+",");
	   String getProdName=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+productName+"')]").getAttribute("text");
		System.out.println("Get Product Name :"+getProdName);
		if(getProdName.contains(productName))
		{
			boolean b= getProdName.contains(productName);
			System.out.println("Product Name:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingAccountsLastTransAmountiOS(String lastTransAmount)
    {
	   System.out.println("Excel Read Values:"+lastTransAmount+",");
	   String getLastTransAmt=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+lastTransAmount+"')]").getAttribute("name");
		System.out.println("Get Last Transaction Date :"+getLastTransAmt);
		if(getLastTransAmt.contains(lastTransAmount))
		{
			boolean b= getLastTransAmt.contains(lastTransAmount);
			System.out.println("Last Transaction Amount:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingAccountsLastTransDateiOS(String lastTransDate)
    {
	   System.out.println("Excel Read Values:"+lastTransDate+",");
	   String getLastTransDate=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+lastTransDate+"')]").getAttribute("name");
		System.out.println("Get Last Transaction Date :"+getLastTransDate);
		if(getLastTransDate.contains(lastTransDate))
		{
			boolean b= getLastTransDate.contains(lastTransDate);
			System.out.println("Last Transaction Date:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingAccountsStmtDateiOS(String statementDate)
    {
	   System.out.println("Excel Read Values:"+statementDate+",");
	   String getStmtDate=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+statementDate+"')]").getAttribute("name");
		System.out.println("Get Statement Date :"+getStmtDate);
		if(getStmtDate.contains(statementDate))
		{
			boolean b= getStmtDate.contains(statementDate);
			System.out.println("Statement Date:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingAccountsCCYiOS(String totalCurrency)
    {
	   System.out.println("Excel Read Values:"+totalCurrency+",");
	   String getCCY=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+totalCurrency+"')]").getAttribute("name");
		System.out.println("Get Total Deposit amount :"+getCCY);
		if(getCCY.contains(totalCurrency))
		{
			boolean b= getCCY.contains(totalCurrency);
			System.out.println("Total currency:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingAccountsAvlbBaliOS(String availableCurrency)
    {
	   System.out.println("Excel Read Values:"+availableCurrency+",");
	   String getAvlbBal=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+availableCurrency+"')]").getAttribute("name");
		System.out.println("Get Available Balance :"+getAvlbBal);
		if(getAvlbBal.contains(availableCurrency))
		{
			boolean b= getAvlbBal.contains(availableCurrency);
			System.out.println("Available Balance:"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingProductNameiOS(String productName)
    {
	   System.out.println("Excel Read Values:"+productName+",");
	   String getProdName=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+productName+"')]").getAttribute("name");
		System.out.println("Get Product Name :"+getProdName);
		if(getProdName.contains(productName))
		{
			boolean b= getProdName.contains(productName);
			System.out.println("Product Name :"+b);
			
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }
   public static void VerifyingAccountsiOS(String accNumber,String productName,String accNumber1,String productName1)
    {
	   System.out.println("Excel Read Values:"+accNumber+","+productName+","+accNumber1+","+productName1);
	   String getAcc=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").getAttribute("name");
		System.out.println("Get Acc :"+getAcc);
		if(getAcc.contains(productName))
		{
			boolean b= getAcc.contains(accNumber1);
			System.out.println("Product name:"+b);
			boolean b1= getAcc.contains(productName1);
			System.out.println("Display Account No:"+b1);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
     }

   public static void VerifyingL2AccountsAndroid(String accNumber)
    {
	   String getAcc=DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").getAttribute("text");
		System.out.println("Get Acc :"+getAcc);
		
		if(getAcc.contains(accNumber))
		{
			boolean b= getAcc.contains(accNumber);
			System.out.println("acc Number:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   public static void VerifyingL2AccountsiOS(String accNumber)
    {
	   String getAcc=DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").getAttribute("name");
		System.out.println("Get Acc :"+getAcc);
		if(getAcc.contains(accNumber))
		{
			boolean b= getAcc.contains(accNumber);
			System.out.println("acc Number:"+b);
			System.out.println("Assertion Passed");
		}
		
		else
		{
			System.out.println("Not Found");
		}	   
	
     }
   

   public static void VerifyingProductNameClickAndroid(String productName)
    {
	   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+productName+"')]").click();
   
    }
   public static void VerifyingProductNameClickiOS(String productName)
    {
	   DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+productName+"')]").click();
   
    }
   public static void VerifyingAccountsClickAndroidMRC(String accNumber) throws Exception
    {
	   
	//   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").click();
	   if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

		   Thread.sleep(5000);

		   if (DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").isDisplayed()) {

		   System.out.println("Element is Present");

		   ExtentReport.status = "pass";

		   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')").click();

		   Thread.sleep(5000);

		   ExtentReport.getResult();

		   } else {

		    System.out.println("Element is Not Present");

		   ExtentReport.status = "fail";

		   ExtentReport.getResult();

		   }

		   }

		   else {

		   Thread.sleep(5000);

		   if (DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").isDisplayed()) {

		   System.out.println("Element is Present");

		   ExtentReport.status = "pass";

		   DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").click();

		   Thread.sleep(5000);

		   ExtentReport.getResult();

		   } else {

		    System.out.println("Element is Not Present");

		   ExtentReport.status = "fail";

		   ExtentReport.getResult();

		   }

		   }
   
    }
   public static void VerifyingProductAccNoClickAndroid(String productName, String accNumber)
   {
	   
	   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"') and contains(@text,'"+productName+"')]").click();
  
   }
  public static void VerifyingProductAccNoClickiOS(String productName, String accNumber)
   {
	   
	   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"') and contains(@text,'"+productName+"')]").click();
  
   }
 
  public static void VerifyingAccountCCYClickAndroid(String totalBal)
   {
	   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+totalBal+"')]").click();
  
   }
  public static void VerifyingAccountCCYClickiOS(String totalBal)
   {
	   DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+totalBal+"')]").click();
  
   }
  public static void VerifyElementClick(By by) throws Exception {
		Thread.sleep(5000);
		{
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.click();
			Thread.sleep(5000);
		}
		else {
			System.out.println("Element not Present");

		}
		}
	}
	
  public static void verifyStringValueAndClick(String accNo, String productName) throws Exception{

	   if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {

	   Thread.sleep(5000);

	   if (DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNo+"') and contains(@text,'"+productName+"')]").isDisplayed()) {

	   System.out.println("Element is Present");

	   ExtentReport.status = "pass";

	   DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNo+"') and contains(@text,'"+productName+"')]").click();

	   Thread.sleep(5000);

	   ExtentReport.getResult();

	   } else {

	    System.out.println("Element is Not Present");

	   ExtentReport.status = "fail";

	   ExtentReport.getResult();

	   }

	   }

	   else {

	   Thread.sleep(5000);

	   if (DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNo+"')]").isDisplayed()) {

	   System.out.println("Element is Present");

	   ExtentReport.status = "pass";

	   DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNo+"')]").click();

	   Thread.sleep(5000);

	   ExtentReport.getResult();

	   } else {

	    System.out.println("Element is Not Present");

	   ExtentReport.status = "fail";

	   ExtentReport.getResult();

	   }

	   }
	   }
	

	/*
	 * To get text from some element This function needs one parameter Element
	 * locator i.e xpath
	 */
	// ==============For IOS Device====================
	public static String getTextElementBy(By by) {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		return ele.getAttribute("name");

	}

	/*
	 * first param ---> locator i.e xpath we can set below values according to your
	 * platform Second param --->IOS ----> name, value Second Param --- >Android -->
	 * text
	 * 
	 */
	// ==============For IOS and Android Device====================

	public static void getTextElementByMultipleSessioniOS(By by, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			ele.getAttribute(text);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
		}

	}

	/*
	 * To get text from some element This function needs one parameter Element
	 * locator i.e xpath
	 */
	// ==============For Android Device====================
	public static String getTextElementByText(By by) {
		MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
		return ele.getAttribute("text");

	}

	/*
	 * To assert This function needs Three parameters actual and testing texts first
	 * param --> locator i.e xpath Second param ---> your expected value we can set
	 * below values according to your platform Third param --->IOS ----> name, value
	 * Third Param --- >Android --> text
	 * 
	 */

	public static void myAssertion(By by, String expected, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			String actualvalue = ele.getAttribute(text);
			Assert.assertTrue(actualvalue.equalsIgnoreCase(expected));
			System.out.println("Assertion is passed");
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			System.out.println("Assertion is failed");
			ExtentReport.getResult();

		}
	}
	
	public static void myAssertionContains(By by, String expected, String text) throws Exception {
		Thread.sleep(3000);
		if (DesiredCap.driver.findElements(by).size() > 0) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			String actualvalue = ele.getAttribute(text);
			Assert.assertTrue(actualvalue.contains(expected));
			System.out.println("Assertion is passed");
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			System.out.println("Assertion is failed");
			ExtentReport.getResult();

		}
	}

	/*
	 * To tap on device's screen at any location This function needs two parameters
	 * x and y ordinates
	 */

	public static void tapMethod(int x, int y) throws IOException, InterruptedException {
		TouchAction touchAction = new TouchAction(DesiredCap.driver);
		PointOption points = new PointOption();
		touchAction.tap(points.point(x, y)).perform();
		System.out.println("Tap performed");
		Thread.sleep(5000);
	}
	
	 public static void scrollAndroid() {
		    //MobileElement element = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
	    	TouchActions touchAction = new TouchActions(DesiredCap.driver);
	    	touchAction.scroll(0, 30);
	    	touchAction.perform();
	    	//element.click();
	    }
	 
	 public static String swipeAction(int startx, int starty, int endx, int endy, int duration) {
	        return executeAsString("adb shell input touchscreen swipe "+startx+" "+starty+" "+endx+" "+endy+" "+duration);
	    }
	   private static String executeAsString(String command) {
	        try {
	            Process pr = execute(command);
	            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
	            StringBuilder sb = new StringBuilder();
	            String line;
	            while ((line = input.readLine()) != null) {
	                if (!line.isEmpty()) {
	                    sb.append(line);
	                }
	            }
	            input.close();
	            pr.destroy();
	            return sb.toString();
	        } catch (Exception e) {
	            throw new RuntimeException("Execution error while executing command" + command, e);
	        }
	    } 
	   
	   private static Process execute(String command) throws IOException, InterruptedException {
	        List<String> commandP = new ArrayList<>();
	        String[] com = command.split(" ");
	        for (int i = 0; i < com.length; i++) {
	            commandP.add(com[i]);
	        }
	        ProcessBuilder prb = new ProcessBuilder(commandP);
	        Process pr = prb.start();
	        pr.waitFor(10, TimeUnit.SECONDS);
	        return pr;
	    }
	   
	/*
	 * To tap on any element This function needs one parameter Element locator i.e
	 * xpath
	 */

	public static void tapByElement(By by) throws IOException, InterruptedException {
		try {
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			System.out.println("element located");
			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			ElementOption eleOP = new ElementOption();
			PointOption points = new PointOption();
			TapOptions tapOptions = new TapOptions().withElement(eleOP.withElement(ele));
			touchAction.tap(tapOptions).perform();
			System.out.println("Tap performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}
	public static void tapElement(String value) throws IOException, InterruptedException {
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			try {
				MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[(@text='"+value+"')]"))));
				System.out.println("element located");
				TouchAction touchAction = new TouchAction(DesiredCap.driver);
				ElementOption eleOP = new ElementOption();
				PointOption points = new PointOption();
				TapOptions tapOptions = new TapOptions().withElement(eleOP.withElement(ele));
				touchAction.tap(tapOptions).perform();
				System.out.println("Tap performed");
				Thread.sleep(5000);
			} catch (Exception e) {
				System.out.println("Unable to Tap");
			}
		}
		else {
		try {
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[(@name='"+value+"')]"))));
			System.out.println("element located");
			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			ElementOption eleOP = new ElementOption();
			PointOption points = new PointOption();
			TapOptions tapOptions = new TapOptions().withElement(eleOP.withElement(ele));
			touchAction.tap(tapOptions).perform();
			System.out.println("Tap performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}
		}

	}

	/*
	 * To LongPress on element This function needs one parameter Element locator i.e
	 * xpath
	 */

	public static void longPressOnElement(By by) throws IOException, InterruptedException {
		try {
			MobileElement ele = (MobileElement) (wait.until(ExpectedConditions.presenceOfElementLocated(by)));
			System.out.println("element located");
			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			LongPressOptions longpress = new LongPressOptions();
			ElementOption eleOpt = new ElementOption();
			touchAction.longPress(longpress.withElement(eleOpt.withElement(ele))).perform();
			System.out.println("LONG PRESS performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}

	/*
	 * To LongPress on element This function needs two parameters x and y ordinates
	 */

	public static void longPressOnElementCor(int x, int y) throws IOException, InterruptedException {
		try {

			TouchAction touchAction = new TouchAction(DesiredCap.driver);
			LongPressOptions longpress = new LongPressOptions();
			ElementOption eleOpt = new ElementOption();
			PointOption point = new PointOption();
			touchAction.longPress(longpress.withPosition(point.withCoordinates(x, y))).perform();
			System.out.println("LONG PRESS performed");
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Unable to Tap");
		}

	}

	/*
	 * To Scroll
	 */

	public static void scrollByJavaScriptExe() throws IOException, InterruptedException {

		PointOption points = new PointOption();
		TouchAction action = new TouchAction(DesiredCap.driver);
		action.press(points.point(501, 451)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
				.moveTo(points.point(501, 200)).release().perform();
		Thread.sleep(3000);
	}

	/*
	 * To Scroll in all directions This function needs four parameters x and y
	 * ordinates one integer value one character value i.e for up/down/left/right
	 */
	public static void scrollByJavaScriptExe(int x, int y, int coordinate, char c) throws IOException {
		PointOption points = new PointOption();
		TouchAction action = new TouchAction(DesiredCap.driver);

		switch (c) {
		case '-':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x, y + c + coordinate)).release().perform();
			break;
		case '+':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x, y + c + coordinate)).release().perform();
			break;
		case 'L':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(coordinate, y)).release().perform();
			break;
		case 'R':
			action.press(points.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
					.moveTo(points.point(x + coordinate, y)).release().perform();
			break;
		default:
			System.out.println("Scroll cant perform on that direction");
			break;
		}

	}

	/*
	 * To Switch from Native view to web view
	 * 
	 */
	public static void Switch_Native_To_Hybrid() {

		Set<String> contextNames = DesiredCap.driver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
		DesiredCap.driver.context(contextNames.toArray()[1].toString());

	}

	/*
	 * To Switch from web view to Native view
	 * 
	 */
	public static void Switch_Hybrid_To_Native() {

		Set<String> contextNames = DesiredCap.driver.getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName);
		}
		DesiredCap.driver.context(contextNames.toArray()[0].toString());

	}

	/*
	 * To take Screenshots
	 */
	public static void screenShots() throws IOException {
		TakesScreenshot shots = (TakesScreenshot) DesiredCap.driver;
		File src = shots.getScreenshotAs(OutputType.FILE);
		Date dt = new Date();
		File dest = new File(
				"C:\\Users\\HP\\eclipse-workspace\\BABU\\Myntra\\src\\main\\resource\\Screenshot\\babu.png");
		FileHandler.copy(src, dest);
	}

	/*
	 * To Check element is present not This function needs one parameter Element
	 * locator i.e xpath
	 */
	public static boolean elementNotExist(By by) throws InterruptedException {
		return DesiredCap.driver.findElements(by).size() > 0;

	}

	public static void verifyStringValue(String accNumber, String productName, String accNumber1,
			String productName1) throws Exception {
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
			Thread.sleep(5000);
			if (DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").isDisplayed()) {
			String getAcc = DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+accNumber+"')]").getAttribute("text");
			System.out.println("Get Acc :" + getAcc);
		if (getAcc.contains(productName)) {
			boolean b = getAcc.contains(accNumber1);
			System.out.println("Product name:" + b);
			boolean b1 = getAcc.contains(productName1);
			System.out.println("Display Account No:" + b1);
			System.out.println("Assertion Passed");
			ExtentReport.status = "pass";
			Thread.sleep(5000);
			ExtentReport.getResult();
		} else {
			System.out.println("Not Found");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
		}
		}
			}
		else 
		{
			Thread.sleep(5000);
			if (DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").isDisplayed()) {
			String getAcc = DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+accNumber+"')]").getAttribute("name");
			System.out.println("Get Acc :" + getAcc);
		if (getAcc.contains(productName)) {
			boolean b = getAcc.contains(accNumber1);
			System.out.println("Product name:" + b);
			boolean b1 = getAcc.contains(productName1);
			System.out.println("Display Account No:" + b1);
			System.out.println("Assertion Passed");
			ExtentReport.status = "pass";
			Thread.sleep(5000);
			ExtentReport.getResult();
		} else {
			System.out.println("Not Found");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
		}	
		}
		}
	}
	
	public static void verifyStringValueAndClick(String value) throws Exception{
		if (Xls_Reader.platformToBeTested.equalsIgnoreCase("Android")) {
		Thread.sleep(5000);
		if (DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+value+"')]").isDisplayed()) {
			System.out.println("Element is Present");
			ExtentReport.status = "pass";
			DesiredCap.driver.findElementByXPath("//*[contains(@text,'"+value+"')]").click();
			Thread.sleep(5000);
			ExtentReport.getResult();
		} else {
			System.out.println("Element is Not Present");
			ExtentReport.status = "fail";
			ExtentReport.getResult();
		}
		}
		else {
			Thread.sleep(5000);
			if (DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+value+"')]").isDisplayed()) {
				System.out.println("Element is Present");
				ExtentReport.status = "pass";
				DesiredCap.driver.findElementByXPath("//*[contains(@name,'"+value+"')]").click();
				Thread.sleep(5000);
				ExtentReport.getResult();
			} else {
				System.out.println("Element is Not Present");
				ExtentReport.status = "fail";
				ExtentReport.getResult();
			}
		}

	}

		// ====================Old Reusable Functions================================

	public static boolean isExist(String Id) {
		try {
			DesiredCap.driver.findElement(By.id(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistXpath(String Id) {
		try {
			DesiredCap.driver.findElement(By.xpath(Id)).click();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isExistSend(String Id, String keysToSend) {
		try {
			DesiredCap.driver.findElement(By.id(Id)).sendKeys(keysToSend);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static void checkForTheElementIsPresentInDOM(String className) {
		List<MobileElement> inputs;
		Long startTime = System.currentTimeMillis();
		do {
			inputs = (List<MobileElement>) DesiredCap.driver.findElements(MobileBy.className(className));
			if (!inputs.isEmpty()) {
				System.out.println("FOUND!");
				break;
			} else {
				System.out.println("NoT Found..!!");
			}
		} while ((System.currentTimeMillis() - startTime) / 1000 < 100); // 100
																			// sec
																			// wait
	}

	public static void clearForWaitByID(String value) throws InterruptedException {
		Thread.sleep(4000);
		DesiredCap.driver.findElement(By.id(value)).clear();
	}

	public static void clcikByID(String value) {
		try {
			DesiredCap.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			DesiredCap.driver.findElement(By.id(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clickByXpath(String value) {
		try {
			DesiredCap.driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			DesiredCap.driver.findElement(By.xpath(value)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clickForWaitByXpath(String value) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(DesiredCap.driver, 60);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(value))).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Set<String> findDuplicates(List<String> listContainingDuplicates) {
		final Set<String> setToReturn = new HashSet<String>();
		final Set<String> set1 = new HashSet<String>();
		for (String yourInt : listContainingDuplicates) {
			if (!set1.add(yourInt)) {
				setToReturn.add(yourInt);
			}
		}
		return setToReturn;
	}

	public static boolean sortOrder(List<String> value) {
		String previous = "";
		value = new ArrayList<String>();
		for (final String current : value) {
			value.add(current);
			if (current.compareTo(previous) < 0)
				return false;
			previous = current;
		}
		return true;
	}

	public static void clickByClassName(String elements, int index) throws InterruptedException {
		List<MobileElement> elementClick = DesiredCap.driver.findElementsByClassName(elements);
		elementClick.get(index).click();
	}

	public static void scrolliOS(String text) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<Object, Object> scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "value == '" + text + "'");
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath(text)).click();

	}
	
	 public static void scrollSwipeVerticalAndroid() {
		 
	        PointerInput finger = new PointerInput(Kind.TOUCH, "finger");
	        Interaction moveToStart = finger.createPointerMove(Duration.ZERO, Origin.viewport(), 468, 1796);
	        Interaction pressDown = finger.createPointerDown(MouseButton.LEFT.asArg());
	        Interaction moveToEnd = finger.createPointerMove(Duration.ofMillis(5000), Origin.viewport(), 468, 290);
	        Interaction pressUp = finger.createPointerUp(MouseButton.LEFT.asArg());

	        Sequence swipe = new Sequence(finger, 0);
	        swipe.addAction(moveToStart);
	        swipe.addAction(pressDown);
	        swipe.addAction(moveToEnd);
	        swipe.addAction(pressUp);
	        
	        DesiredCap.driver.perform(Arrays.asList(swipe));
	    }
	
	
 
        /*
         
        public static void scroll(ScrollDirection dir, double distance) {
        if (distance < 0 || distance > 1) {
            throw new Error("Scroll distance must be between 0 and 1");
        }
        
        //  Dimension size = getWindowSize();
        Point midPoint = new Point((int) (1440 * 0.5), (int) (2960 * 0.5));
        int top = midPoint.y - (int) ((2960 * distance) * 0.5);
        int bottom = midPoint.y + (int) ((2960 * distance) * 0.5);
        int left = midPoint.x - (int) ((1440 * distance) * 0.5);
        int right = midPoint.x + (int) ((1440 * distance) * 0.5);
        if (dir == ScrollDirection.UP) {
            swipe(new Point(midPoint.x, top), new Point(midPoint.x, bottom), SCROLL_DUR);
        } else if (dir == ScrollDirection.DOWN) {
            swipe(new Point(midPoint.x, bottom), new Point(midPoint.x, top), SCROLL_DUR);
        } else if (dir == ScrollDirection.LEFT) {
            swipe(new Point(left, midPoint.y), new Point(right, midPoint.y), SCROLL_DUR);
        } else {
            swipe(new Point(right, midPoint.y), new Point(left, midPoint.y), SCROLL_DUR);
        }
    }
	
    public static Dimension getWindowSize() {
        if (windowSize == null) {
            windowSize = driver.manage().window().getSize();
        }
        return windowSize;
    }
	
    public static enum ScrollDirection {
        UP, DOWN, LEFT, RIGHT
    }

    public static void swipe(Point start, Point end, Duration duration) {

        boolean isAndroid = driver instanceof AndroidDriver<?>;
        PointerInput input = new PointerInput(PointerInput.Kind.TOUCH, "finger1");
        Sequence swipe = new Sequence(input, 0);
        swipe.addAction(input.createPointerMove(Duration.ZERO, PointerInput.Origin.viewport(), start.x, start.y));
        swipe.addAction(input.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
        if (isAndroid) {
            duration = duration.dividedBy(ANDROID_SCROLL_DIVISOR);
        } else {
            swipe.addAction(new Pause(input, duration));
            duration = Duration.ZERO;
        }
        swipe.addAction(input.createPointerMove(duration, PointerInput.Origin.viewport(), end.x, end.y));
        swipe.addAction(input.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
        }

*/

}

