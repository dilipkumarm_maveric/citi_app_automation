@All
Feature: Test Positive Friction Banner Cards Onboarding for HK country

#Author:Dilipkumar
#Functionality:Positive Friction Banner - Cards Onboarding - Next Step,Cards Delivery Status Tracker
#Domain:Grow
#Platform:Android

@Banner_MRCNextStepCVP
Scenario: Verify Cards Onboarding Next Step CVP Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking CVP CTA Action pointed to offer label which is navigating to CVP Screen for Cards Onboarding Next Step
And To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Next Step
And To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Next Step

@Banner_MRCNextStepSTP
Scenario: Verify Cards Onboarding Next Step STP Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking STP Journey CTA Action pointed to offer label which is navigating to expected STP Journey Screen for Cards Onboarding Next Step
And To verify whether the required mandatory field expected STP screen offer title is getting displayed for Cards Onboarding Next Step
And To verify whether on clicking back button in STP screen which is navigate back to dashboard screen for Cards Onboarding Next Step


@Banner_MRCNextStepSTO
Scenario: Verify Cards Onboarding Next Step STO Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking STO Journey CTA Action pointed to offer label which is navigating to expected STO Journey Screen for Cards Onboarding Next Step

@Banner_MRCNextStepWebviewBoth
Scenario: Verify Cards Onboarding Next Step Webview Both Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCNextStepWebviewYes
Scenario: Verify Cards Onboarding Next Step Webview Yes Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen


@Banner_MRCNextStepWebviewNo
Scenario: Verify Cards Onboarding Next Step Webview No Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCNextStepWebviewOK
Scenario: Verify Cards Onboarding Next Step Webview OK Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_OK is getting displayed
And To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer
And To Verify whether the required mandatory Field CTA_OK_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen


@Banner_MRCNextStepWebviewError
Scenario: Verify Cards Onboarding Next Step Webview Error Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step view card benefits label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step add to apple wallet label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills thumbnail is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Next Step pay bills label is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Onboarding Next Step view card benefits label which is navigating to expected Webview Screen
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCTrackerSTP
Scenario: Verify Cards Onboarding Card Delivery Tracker STP Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking STP Journey CTA Action pointed to need help label which is navigating to expected STP Journey Screen for Cards Onboarding Card Delivery Tracker

@Banner_MRCTrackerCVP
Scenario: Verify Cards Onboarding Card Delivery Tracker CVP Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking CVP CTA Action pointed to offer label which is navigating to CVP Screen for Cards Onboarding Card Delivery Tracker
And To verify whether the required mandatory field expected CVP screen offer title is getting displayed for Cards Onboarding Card Delivery Tracker
And To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen for Cards Onboarding Card Delivery Tracker


@Banner_MRCTrackerSTO
Scenario: Verify Cards Onboarding Card Delivery Tracker STO Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking STO Journey CTA Action pointed to need help label which is navigating to expected STO Journey Screen for Cards Onboarding Card Delivery Tracker


@Banner_MRCTrackerWebviewBoth
Scenario: Verify Cards Onboarding Card Delivery Tracker Webview Both Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen


@Banner_MRCTrackerWebviewYes
Scenario: Verify Cards Onboarding Card Delivery Tracker Webview Yes Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen


@Banner_MRCTrackerWebviewNo
Scenario: Verify Cards Onboarding Card Delivery Tracker Webview No Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen


@Banner_MRCTrackerWebviewOK
Scenario: Verify Cards Onboarding Card Delivery Tracker Webview OK Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_OK is getting displayed
And To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer
And To Verify whether the required mandatory Field CTA_OK_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCTrackerWebviewError
Scenario: Verify Cards Onboarding Card Delivery Tracker Webview Error Navigation

Given Login with username and password
And To verify whether on clicking on credit cards category which is expanding screen for Cards Onboarding Next Step
And To verify whether on clicking on credit cards right chevron which is navigating screen to cards ada screen for Cards Onboarding Next Step
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker description is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker need help label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card printed label is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card dispatched is getting displayed
And To Verify whether the required mandatory Field Cards Onboarding Card Delivery Tracker card arrived is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Cards Card Delivery Tracker need help label which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen