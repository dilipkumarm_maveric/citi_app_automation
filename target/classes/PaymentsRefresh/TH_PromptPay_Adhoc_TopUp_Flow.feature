@All
Feature: Test Prompt Pay Adhoc Topup functionality for TH country

@Adhoc_TopUp_SOFChanged
Scenario: To verify Ad-hoc Topup flow carried out with SOF changed in amount entry screen

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu for promptpay is getting displayed
When To verify whether on clicking Payments in footer menu for promptpay is navigate to payments drawer
Then To verify whether the required mandatory field PromptPay in payments page is getting displayed
When To verify whether on clicking PromptPay in payments drawer is navigate to transfer to page
Then To verify required Transfer to Label is getting displayed as per the condition Always
And To verify required All Label is getting displayed as per the condition in 100 screen
And To verify required Account no Label is getting displayed as per the condition in 100 screen
And To verify required Bill Payment Label is getting displayed as per the condition in 100 screen
And To verify required Mobile no Label is getting displayed as per the condition in 100 screen
And To verify required Search Magnifier Label is getting displayed as per the condition Always
And To verify required Search Label is getting displayed as per the condition NA
When To verify whether the application is getting navigated to the required Search Transition Move screen up without showing tabs on clicking Search Label
Then To verify required Clear button in Search box Label is getting displayed as per the condition Upon customer entering atleast 1 char
When To verify whether the application is getting navigated to the required Display default screen state same as when user first enters this screen on clicking Clear button in Search box Label
And To verify required Plus icon Label is getting displayed as per the condition Always
And To verify required New payee Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking New payee Label
Then To verify required New payee Prompt Pay TH Business to check Header is getting displayed as per the condition Always
And To verify required Transfer type Label is getting displayed as per the condition Always
And To verify required Account No transfer transfer type value Label is getting displayed as per the condition Always this default proxy will be shown
And To verify required Down Arrow Label is getting displayed as per the condition Always for default 300 screen
And To verify required Bank name Label is getting displayed as per the condition for bank selection refer to Prompt_BankSel_330
And To verify required Account number Label is getting displayed as per the condition When Account Number Option is selected in Transfer Type
When To verify whether the application is getting navigated to the required PromptPay_TransferType_320 on clicking Transfer type value Label
Then To verify required Down Arrow Label is getting displayed as per the condition Always
And To verify required Select transfer type Label is getting displayed as per the condition Always
And To verify required List of transfer type Label is getting displayed as per the condition Below are the dropdown values which need to be shown to customer
And To verify required Mobile number Label is getting displayed as per the condition Always
And To verify required National ID Label is getting displayed as per the condition Always
And To verify required Bill payment Label is getting displayed as per the condition Always
And To verify required Account no transfer Label is getting displayed as per the condition Always
And To verify required Top up PromptPay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Top up PromptPay Label
Then To verify required Top up PromptPay Label in 300 screen is getting displayed as per the condition Always
And To verify required e Wallet No Label is getting displayed as per the condition Always
Then While entering required e Wallet No Value in e Wallet field in the new payee 300 screen
And To verify required Next Arrow Button is getting displayed as per the condition in 300 screen
When To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Next Arrow Button
Then To verify required Currency Label is getting displayed as per the condition Always
And To verify required Amount Value Label is getting displayed as per the condition
And To verify required Down Arrow Label is getting displayed as per the condition If there is more than one SOF available
And To verify required From account Label is getting displayed as per the condition Always
And To verify required Account Name Label is getting displayed as per the condition Always
And To verify required Account number Label is getting displayed as per the condition Always Display Last 4 digits of Account Number
And To verify required Available balance Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required CCY Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Balance Amount Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Down Arrow Label is getting displayed as per the condition Optional To be display only if it satisfy below two conditions If user entered amount is less than minimum amount required If transaction is not LCY to LCY min transaction amount should be 0 dot 01 THB
And To verify whether the application is getting navigated to the required PromptPay_SOF_210 on clicking Down Arrow Label
And To verify required Transfer from Label is getting displayed as per the condition NA
And To verify required Account Name Nickname in 210 screen if available Label is getting displayed as per the condition NA
And To verify required Account number Label in 210 screen is getting displayed as per the condition NA
And To verify required Available balance Label in 210 screen is getting displayed as per the condition NA
And To verify required CCY Label in 210 screen is getting displayed as per the condition NA
And To verify required Balance Amount Label in 210 screen is getting displayed as per the condition NA
When To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking any available source account
Then While entering required Amount Value in Amount field in the amount entry 200 screen
And To verify required Next Arrow Label is getting displayed as per the condition Always in 200 screen
When To verify whether the application is getting navigated to the required PromptPay_Review_600 on clicking Next Arrow Label
And To verify required Review & Confirm Label is getting displayed as per the condition NA
And To verify required Amount Label is getting displayed as per the condition NA
And To verify required CCY Label is getting displayed as per the condition NA
And To verify required Transaction amount Label is getting displayed as per the condition NA
And To verify required From Label is getting displayed as per the condition NA
And To verify required Account Name Nickname if available Label is getting displayed as per the condition NA
And To verify required Account number Label is getting displayed as per the condition NA
And To verify required To Label is getting displayed as per the condition NA
And To verify required eWallet No Label is getting displayed as per the condition
And To verify required eWallet Bank Name Label is getting displayed as per the condition
And To verify required Beneficiary name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in review screen is getting displayed as per the condition Always
And To verify required When Label is getting displayed as per the condition NA
And To verify required Day Date Label is getting displayed as per the condition NA
And To verify required Transfer will be made instantly Label in 600 screen is getting displayed as per the condition NA
And To verify required Date Display Tool Tip Label is getting displayed as per the condition NA
And To verify required By tapping something you have read and understood the PDF Image Terms & Conditions Label is getting displayed as per the condition NA
And To verify required Pay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_Confirm_700 on clicking Pay Label
Then To verify required Tick Mark Label is getting displayed as per the condition NA
And To verify required Transfer submitted Label is getting displayed as per the condition NA
And To verify required 700 screen ccy Label is getting displayed as per the condition NA
And To verify required 700 screen amount Label is getting displayed as per the condition NA
And To verify required will reach Label is getting displayed as per the condition NA
And To verify required First name of beneficiary Label is getting displayed as per the condition NA
And To verify required shortly Label is getting displayed as per the condition NA
And To verify required Transaction ID Label is getting displayed as per the condition NA
And To verify required Transaction ID value Label is getting displayed as per the condition NA
And To verify required Recipient Details Label is getting displayed as per the condition NA
And To verify required eWallet No and Bank Name Label is getting displayed as per the condition
And To verify required Transfer date & type Label is getting displayed as per the condition always
And To verify required Day, Date Label is getting displayed as per the condition Always
And To verify required Transfer will be made instantly Label is getting displayed as per the condition Always
And To verify required Beneficiary name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in confirmation screen is getting displayed as per the condition Always
And To verify required Sender name Label is getting displayed as per the condition Always
And To verify required Sender name value Label is getting displayed as per the condition Always
And To verify required From Label in 700 screen is getting displayed as per the condition Always
And To verify required Account Name Nickname in 700 screen if available Label is getting displayed as per the condition Always
And To verify required Account number Label in 700 screen is getting displayed as per the condition Always
And To verify required Share this transaction Label is getting displayed as per the condition Always
And To verify required Done Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required Payments Menu Screen From where TH Prompt Pay is started on clicking Done Label


@Adhoc_TopUp_Done
Scenario: To verify Ad-hoc Topup flow carried out with default SOF in amount entry screen with same date transfer

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu for promptpay is getting displayed
When To verify whether on clicking Payments in footer menu for promptpay is navigate to payments drawer
Then To verify whether the required mandatory field PromptPay in payments page is getting displayed
When To verify whether on clicking PromptPay in payments drawer is navigate to transfer to page
Then To verify required Transfer to Label is getting displayed as per the condition Always
And To verify required All Label is getting displayed as per the condition in 100 screen
And To verify required Account no Label is getting displayed as per the condition in 100 screen
And To verify required Bill Payment Label is getting displayed as per the condition in 100 screen
And To verify required Mobile no Label is getting displayed as per the condition in 100 screen
And To verify required Search Magnifier Label is getting displayed as per the condition Always
And To verify required Search Label is getting displayed as per the condition NA
When To verify whether the application is getting navigated to the required Search Transition Move screen up without showing tabs on clicking Search Label
Then To verify required Clear button in Search box Label is getting displayed as per the condition Upon customer entering atleast 1 char
When To verify whether the application is getting navigated to the required Display default screen state same as when user first enters this screen on clicking Clear button in Search box Label
And To verify required Plus icon Label is getting displayed as per the condition Always
And To verify required New payee Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking New payee Label
Then To verify required New payee Prompt Pay TH Business to check Header is getting displayed as per the condition Always
And To verify required Transfer type Label is getting displayed as per the condition Always
And To verify required Account No transfer transfer type value Label is getting displayed as per the condition Always this default proxy will be shown
And To verify required Down Arrow Label is getting displayed as per the condition Always for default 300 screen
And To verify required Bank name Label is getting displayed as per the condition for bank selection refer to Prompt_BankSel_330
And To verify required Account number Label is getting displayed as per the condition When Account Number Option is selected in Transfer Type
When To verify whether the application is getting navigated to the required PromptPay_TransferType_320 on clicking Transfer type value Label
Then To verify required Down Arrow Label is getting displayed as per the condition Always
And To verify required Select transfer type Label is getting displayed as per the condition Always
And To verify required List of transfer type Label is getting displayed as per the condition Below are the dropdown values which need to be shown to customer
And To verify required Mobile number Label is getting displayed as per the condition Always
And To verify required National ID Label is getting displayed as per the condition Always
And To verify required Bill payment Label is getting displayed as per the condition Always
And To verify required Account no transfer Label is getting displayed as per the condition Always
And To verify required Top up PromptPay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Top up PromptPay Label
Then To verify required Top up PromptPay Label in 300 screen is getting displayed as per the condition Always
And To verify required e Wallet No Label is getting displayed as per the condition Always
Then While entering required e Wallet No Value in e Wallet field in the new payee 300 screen
And To verify required Next Arrow Button is getting displayed as per the condition in 300 screen
When To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Next Arrow Button
Then To verify required Currency Label is getting displayed as per the condition Always
And To verify required Amount Value Label is getting displayed as per the condition
And To verify required Down Arrow Label is getting displayed as per the condition If there is more than one SOF available
And To verify required From account Label is getting displayed as per the condition Always
And To verify required Account Name Label is getting displayed as per the condition Always
And To verify required Account number Label is getting displayed as per the condition Always Display Last 4 digits of Account Number
And To verify required Available balance Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required CCY Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Balance Amount Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Down Arrow Label is getting displayed as per the condition Optional To be display only if it satisfy below two conditions If user entered amount is less than minimum amount required If transaction is not LCY to LCY min transaction amount should be 0 dot 01 THB
Then While entering required Amount Value in Amount field in the amount entry 200 screen
And To verify required Next Arrow Label is getting displayed as per the condition Always in 200 screen
When To verify whether the application is getting navigated to the required PromptPay_Review_600 on clicking Next Arrow Label
And To verify required Review & Confirm Label is getting displayed as per the condition NA
And To verify required Amount Label is getting displayed as per the condition NA
And To verify required CCY Label is getting displayed as per the condition NA
And To verify required Transaction amount Label is getting displayed as per the condition NA
And To verify required From Label is getting displayed as per the condition NA
And To verify required Account Name Nickname if available Label is getting displayed as per the condition NA
And To verify required Account number Label is getting displayed as per the condition NA
And To verify required To Label is getting displayed as per the condition NA
And To verify required eWallet No Label is getting displayed as per the condition
And To verify required eWallet Bank Name Label is getting displayed as per the condition
And To verify required Beneficiary name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in review screen is getting displayed as per the condition Always
And To verify required When Label is getting displayed as per the condition NA
And To verify required Day Date Label is getting displayed as per the condition NA
And To verify required Transfer will be made instantly Label is getting displayed as per the condition NA
And To verify required Date Display Tool Tip Label is getting displayed as per the condition NA
And To verify required By tapping something you have read and understood the PDF Image Terms & Conditions Label is getting displayed as per the condition NA
And To verify required Pay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_Confirm_700 on clicking Pay Label
Then To verify required Tick Mark Label is getting displayed as per the condition NA
And To verify required Transfer submitted Label is getting displayed as per the condition NA
And To verify required 700 screen ccy Label is getting displayed as per the condition NA
And To verify required 700 screen amount Label is getting displayed as per the condition NA
And To verify required will reach Label is getting displayed as per the condition NA
And To verify required First name of beneficiary Label is getting displayed as per the condition NA
And To verify required shortly Label is getting displayed as per the condition NA
And To verify required Transaction ID Label is getting displayed as per the condition NA
And To verify required Transaction ID value Label is getting displayed as per the condition NA
And To verify required Recipient Details Label is getting displayed as per the condition NA
And To verify required eWallet No and Bank Name Label is getting displayed as per the condition
And To verify required Transfer date & type Label is getting displayed as per the condition always
And To verify required Day, Date Label is getting displayed as per the condition Always
And To verify required Transfer will be made instantly Label is getting displayed as per the condition Always
And To verify required Beneficiary name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in confirmation screen is getting displayed as per the condition Always
And To verify required Sender name Label is getting displayed as per the condition Always
And To verify required Sender name value Label is getting displayed as per the condition Always
And To verify required From Label in 700 screen is getting displayed as per the condition Always
And To verify required Account Name Nickname in 700 screen if available Label is getting displayed as per the condition Always
And To verify required Account number Label in 700 screen is getting displayed as per the condition Always
And To verify required Share this transaction Label is getting displayed as per the condition Always
And To verify required Done Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required Payments Menu Screen From where TH Prompt Pay is started on clicking Done Label


@Adhoc_TopUp_ShareThisTransactions
Scenario: To verify Ad-hoc Topup flow upto Share transfer details for same date transfer

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu for promptpay is getting displayed
When To verify whether on clicking Payments in footer menu for promptpay is navigate to payments drawer
Then To verify whether the required mandatory field PromptPay in payments page is getting displayed
When To verify whether on clicking PromptPay in payments drawer is navigate to transfer to page
Then To verify required Transfer to Label is getting displayed as per the condition Always
And To verify required Close Label in transfer to screen is getting displayed as per the condition Always
And To verify required All Label is getting displayed as per the condition in 100 screen
And To verify required Account no Label is getting displayed as per the condition in 100 screen
And To verify required Bill Payment Label is getting displayed as per the condition in 100 screen
And To verify required Mobile no Label is getting displayed as per the condition in 100 screen
And To verify required Search Magnifier Label is getting displayed as per the condition Always
And To verify required Search Label is getting displayed as per the condition NA
When To verify whether the application is getting navigated to the required Search Transition Move screen up without showing tabs on clicking Search Label
Then To verify required Clear button in Search box Label is getting displayed as per the condition Upon customer entering atleast 1 char
When To verify whether the application is getting navigated to the required Display default screen state same as when user first enters this screen on clicking Clear button in Search box Label
And To verify required Plus icon Label is getting displayed as per the condition Always
And To verify required New payee Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking New payee Label
Then To verify required New payee Prompt Pay TH Business to check Header is getting displayed as per the condition Always
And To verify required Back button in new payee screen is getting displayed as per the condition Always
And To verify required Transfer type Label is getting displayed as per the condition Always
And To verify required Account No transfer transfer type value Label is getting displayed as per the condition Always this default proxy will be shown
And To verify required Down Arrow Label is getting displayed as per the condition Always for default 300 screen
And To verify required Bank name Label is getting displayed as per the condition for bank selection refer to Prompt_BankSel_330
And To verify required Account number Label is getting displayed as per the condition When Account Number Option is selected in Transfer Type
When To verify whether the application is getting navigated to the required PromptPay_TransferType_320 on clicking Transfer type value Label
Then To verify required Down Arrow Label is getting displayed as per the condition Always
And To verify required Select transfer type Label is getting displayed as per the condition Always
And To verify required List of transfer type Label is getting displayed as per the condition Below are the dropdown values which need to be shown to customer
And To verify required Mobile number Label is getting displayed as per the condition Always
And To verify required National ID Label is getting displayed as per the condition Always
And To verify required Bill payment Label is getting displayed as per the condition Always
And To verify required Account no transfer Label is getting displayed as per the condition Always
And To verify required Top up PromptPay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_NewPayee_300 on clicking Top up PromptPay Label
#Then To verify required Top up PromptPay Label in 300 screen is getting displayed as per the condition Always
And To verify required e Wallet No Label is getting displayed as per the condition Always
Then While entering required e Wallet No Value in e Wallet field in the new payee 300 screen
And To verify required Next Arrow Button is getting displayed as per the condition in 300 screen
When To verify whether the application is getting navigated to the required PromptPay_Amount_200 on clicking Next Arrow Button
Then To verify required Currency Label is getting displayed as per the condition Always
And To verify required Back button in amount entry screen is getting displayed as per the condition Always
And To verify required Amount Value Label is getting displayed as per the condition
And To verify required Down Arrow Label is getting displayed as per the condition If there is more than one SOF available
And To verify required From account Label is getting displayed as per the condition Always
And To verify required Account Name Label is getting displayed as per the condition Always
And To verify required Account number Label is getting displayed as per the condition Always Display Last 4 digits of Account Number
And To verify required Available balance Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required CCY Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Balance Amount Label is getting displayed as per the condition Always Display Available balance for CASA accounts & Ready Credit Account
And To verify required Down Arrow Label is getting displayed as per the condition Optional To be display only if it satisfy below two conditions If user entered amount is less than minimum amount required If transaction is not LCY to LCY min transaction amount should be 0 dot 01 THB
Then While entering required Amount Value in Amount field in the amount entry 200 screen
And To verify required Next Arrow Label is getting displayed as per the condition Always in 200 screen
When To verify whether the application is getting navigated to the required PromptPay_Review_600 on clicking Next Arrow Label
Then To verify required Back Button in review screen is getting displayed as per the condition NA
And To verify required Review & Confirm Label is getting displayed as per the condition NA
And To verify required Close button in review screen is getting displayed as per the condition NA
And To verify required Amount Label is getting displayed as per the condition NA
And To verify required CCY Label is getting displayed as per the condition NA
And To verify required Transaction amount Label is getting displayed as per the condition NA
And To verify required From Label is getting displayed as per the condition NA
And To verify required Account Name Nickname if available Label is getting displayed as per the condition NA
And To verify required Account number Label is getting displayed as per the condition NA
And To verify required To Label is getting displayed as per the condition NA
And To verify required eWallet No Label is getting displayed as per the condition
And To verify required eWallet Bank Name Label is getting displayed as per the condition
And To verify required Beneficiary name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in review screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in review screen is getting displayed as per the condition Always
And To verify required When Label is getting displayed as per the condition NA
And To verify required Day Date Label is getting displayed as per the condition NA
And To verify required Transfer will be made instantly Label is getting displayed as per the condition NA
And To verify required Date Display Tool Tip Label is getting displayed as per the condition NA
And To verify required By tapping something you have read and understood the PDF Image Terms & Conditions Label is getting displayed as per the condition NA
And To verify required Pay Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required PromptPay_Confirm_700 on clicking Pay Label
Then To verify required Tick Mark Label is getting displayed as per the condition NA
And To verify required Transfer submitted Label is getting displayed as per the condition NA
And To verify required 700 screen ccy Label is getting displayed as per the condition NA
And To verify required 700 screen amount Label is getting displayed as per the condition NA
And To verify required will reach Label is getting displayed as per the condition NA
And To verify required First name of beneficiary Label is getting displayed as per the condition NA
And To verify required shortly Label is getting displayed as per the condition NA
And To verify required Transaction ID Label is getting displayed as per the condition NA
And To verify required Transaction ID value Label is getting displayed as per the condition NA
And To verify required Recipient Details Label is getting displayed as per the condition NA
And To verify required eWallet No and Bank Name Label is getting displayed as per the condition
And To verify required Transfer date & type Label is getting displayed as per the condition always
And To verify required Day, Date Label is getting displayed as per the condition Always
And To verify required Transfer will be made instantly Label is getting displayed as per the condition Always
And To verify required Beneficiary name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary name value in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name Label in confirmation screen is getting displayed as per the condition Always
And To verify required Beneficiary bank name value in confirmation screen is getting displayed as per the condition Always
And To verify required Sender name Label is getting displayed as per the condition Always
And To verify required Sender name value Label is getting displayed as per the condition Always
And To verify required From Label in 700 screen is getting displayed as per the condition Always
And To verify required Account Name Nickname in 700 screen if available Label is getting displayed as per the condition Always
And To verify required Account number Label in 700 screen is getting displayed as per the condition Always
And To verify required Share this transaction Label is getting displayed as per the condition Always
And To verify required Done Label is getting displayed as per the condition Always
When To verify whether the application is getting navigated to the required It would take the screenshot of confirmation screen along with Citi Logo and customer can share it to any other person on clicking Share this transaction Label


