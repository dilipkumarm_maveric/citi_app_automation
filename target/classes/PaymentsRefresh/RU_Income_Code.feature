@All
Feature: Test RU Income Code functionality for RU country

@RUIncome_Prdefined
Scenario: Verify RU Income Code Predefined flow functionality

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu for RU Income code is getting displayed
And To verify whether on clicking Payments in footer menu for RU Income code is navigate to payments page
And To verify whether the required mandatory field By requisites in payments drawer for RU Income code is getting displayed
And To verify whether on clicking By requisites in payments drawer for RU Income code is navigate to transfer to page
And To verify whether on clicking PT2 payee for RU Income code is navigate to amount entry screen
And To verify whether the entering the amount value in amount field for RU Income code
And To verify whether the required mandatory field next button for RU Income code is getting displayed
And To verify whether on clicking next button for RU Income code is navigate to review and confirm screen
And To verify whether the required mandatory field Income type code label for RU Income code is getting displayed
And To verify whether the required mandatory field Select button for RU Income code is getting displayed
And To verify whether on clicking Select button for RU Income code is navigate to choose income type screen
And To verify whether the required mandatory field income code value1 for RU Income code is getting displayed
And To verify whether the required mandatory field income code value2 for RU Income code is getting displayed
And To verify whether the required mandatory field income code value3 for RU Income code fast down time is getting displayed
And To verify whether on clicking income code value1 for RU Income code is navigate back to review and confirm screen
And To verify whether on clicking pay button is navigate to payment confirmation screen
And To verify whether the required mandatory field Income type code label for RU Income code in confirmation screen is getting displayed
And To verify whether the required mandatory field Income type code value for RU Income code in confirmation screen is getting displayed



@RUIncome_Adhoc
Scenario: Verify RU Income Code Adhoc flow functionality

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu for RU Income code is getting displayed
And To verify whether on clicking Payments in footer menu for RU Income code  is navigate to payments page
And To verify whether the required mandatory field By requisites in payments drawer for RU Income code  is getting displayed
And To verify whether on clicking By requisites in payments drawer for RU Income code is navigate to transfer to page

And To verify whether the required mandatory field New Payee for RU Income code in Transfer to screen is getting displayed
And To verify whether on clicking New Payee for RU Income code in Transfer to screen is navigate to transfer to New Payee page
And To verify whether the entering account number for RU Income code in Account number field
And To verify whether on clicking Bank field in New Payee screen for RU Income code is navigate to transfer to Recipient bank page
And To verify whether on clicking selected bank for RU Income code is navigate back to transfer to New Payee page
And To verify whether the entering the beneficiary name in Name field for RU Income code
And To verify whether on clicking next arrow for RU Income code is navigate back to transfer to review and confirm screen
And To verify whether the entering the amount value in amount field in adhoc flow for RU Income code
And To verify whether the required mandatory field next button is getting displayed in adhoc flow for RU Income code
And To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow for RU Income code

And To verify whether the required mandatory field Income type code label for RU Income code is getting displayed
And To verify whether the required mandatory field Select button for RU Income code is getting displayed
And To verify whether on clicking Select button for RU Income code is navigate to choose income type screen
And To verify whether the required mandatory field income code value1 for RU Income code is getting displayed
And To verify whether the required mandatory field income code value2 for RU Income code is getting displayed
And To verify whether the required mandatory field income code value3 for RU Income code fast down time is getting displayed
And To verify whether on clicking income code value1 for RU Income code is navigate back to review and confirm screen
And To verify whether on clicking pay button is navigate to payment confirmation screen
And To verify whether the required mandatory field Income type code label for RU Income code in confirmation screen is getting displayed
And To verify whether the required mandatory field Income type code value for RU Income code in confirmation screen is getting displayed



