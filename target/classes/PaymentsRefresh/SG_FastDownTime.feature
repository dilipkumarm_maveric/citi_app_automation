@All
Feature: Test SG Fast Downtime functionality for SG country

@SGFast_Prdefined
Scenario: Verify SG Fast Downtime Predefined flow functionality

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu is getting displayed
And To verify whether on clicking Payments in footer menu is navigate to payments page
And To verify whether the required mandatory field Transfer in payments page is getting displayed
And To verify whether on clicking Transfer in payments page is navigate to transfer to page
And To verify whether the required mandatory field PT2 payee is getting displayed
And To verify whether on clicking PT2 payee is navigate to amount entry screen
And To verify whether the entering the amount value in amount field
And To verify whether the required mandatory field next button is getting displayed
And To verify whether on clicking next button is navigate to review and confirm screen
And To verify whether the required mandatory field fast time transfer instantly description is getting displayed
And To verify whether the required mandatory field pay button is getting displayed
And To verify whether on clicking pay button is navigate to fast down time drawer screen
And To verify whether the required mandatory field red exclaimation mark in fast down time is getting displayed
And To verify whether the required mandatory field fast down time transfer header in fast down time is getting displayed
And To verify whether the required mandatory field fast down time transfer description in fast down time is getting displayed
And To verify whether the required mandatory field pay with giro button in fast down time is getting displayed
And To verify whether the required mandatory field cancel button in fast down time is getting displayed
And To verify whether on clicking cancel button is navigate to stay on same screen
And To verify whether on clicking pay button is navigate to fast down time drawer screen
And To verify whether on clicking pay with giro button in fast down time is navigate to fast down time drawer screen



@SGFast_Adhoc
Scenario: Verify SG Fast Downtime Adhoc flow functionality

Given Login with username and password
And To verify whether the required mandatory field Payments in footer menu is getting displayed
And To verify whether on clicking Payments in footer menu is navigate to payments page
And To verify whether the required mandatory field Transfer in payments page is getting displayed
And To verify whether on clicking Transfer in payments page is navigate to transfer to page

And To verify whether the required mandatory field New Payee in Transfer to screen is getting displayed
And To verify whether on clicking New Payee in Transfer to screen is navigate to transfer to New Payee page
And To verify whether the required mandatory field New Payee Header is getting displayed
And To verify whether the required mandatory field Country is getting displayed
And To verify whether the required mandatory field Country Value is getting displayed
And To verify whether the entering the payee name in Name field
And To verify whether on clicking Bank field in New Payee screen is navigate to transfer to Recipient bank page
And To verify whether on clicking selected bank is navigate back to transfer to New Payee page
And To verify whether the entering account number in Account number field
And To verify whether on clicking next arrow is navigate back to transfer to payee alert page
And To verify whether on clicking i know this payee is navigate back to transfer to amount entry screen page
And To verify whether the entering the amount value in amount field in adhoc flow
And To verify whether the required mandatory field next button is getting displayed in adhoc flow
And To verify whether on clicking next button is navigate to review and confirm screen in adhoc flow

And To verify whether the required mandatory field fast time transfer instantly description is getting displayed
And To verify whether the required mandatory field pay button is getting displayed
And To verify whether on clicking pay button is navigate to fast down time drawer screen
And To verify whether the required mandatory field red exclaimation mark in fast down time is getting displayed
And To verify whether the required mandatory field fast down time transfer header in fast down time is getting displayed
And To verify whether the required mandatory field fast down time transfer description in fast down time is getting displayed
And To verify whether the required mandatory field pay with giro button in fast down time is getting displayed
And To verify whether the required mandatory field cancel button in fast down time is getting displayed
And To verify whether on clicking cancel button is navigate to stay on same screen
And To verify whether on clicking pay button is navigate to fast down time drawer screen
And To verify whether on clicking pay with giro button in fast down time is navigate to fast down time drawer screen



