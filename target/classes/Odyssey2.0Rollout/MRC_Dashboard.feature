@All
Feature: Test Dasshboard funcationality for SG country

@MRC_Deposits
Scenario: Verify MRC Dashboard Deposit funcationality

Given Login with username and password
And To verify whether the required mandatory field deposit category is getting displayed
And To verify whether the required mandatory field deposit category value is getting displayed
And To verify whether on clicking deposit down chevron is it expanded the category 
And To verify whether on clicking deposit up chevron is it collapse the category
And To verify whether on clicking deposit down chevron is it expanded the category

And To verify whether the required mandatory field checking account is getting displayed
And To verify whether the required mandatory field checking account number is getting displayed
And To verify whether the required mandatory field checking account balance ccy is getting displayed
And To verify whether the required mandatory field checking account balance value is getting displayed
And To verify whether the required mandatory field checking account right chevron is getting displayed

And To verify whether the required mandatory field savings account is getting displayed
And To verify whether the required mandatory field savings account number is getting displayed
And To verify whether the required mandatory field savings account balance ccy is getting displayed
And To verify whether the required mandatory field savings account balance value is getting displayed
And To verify whether the required mandatory field savings account right chevron is getting displayed

And To verify whether the required mandatory field time deposit account is getting displayed
And To verify whether the required mandatory field time deposit account number is getting displayed
And To verify whether the required mandatory field time deposit account balance ccy is getting displayed
And To verify whether the required mandatory field time deposit account balance value is getting displayed
And To verify whether the required mandatory field time deposit account right chevron is getting displayed

@MRC_Cards
Scenario: Verify MRC Dashboard CreditCard functionality

Given Login with username and password
And To verify whether the required mandatory field credit cards category is getting displayed
And To verify whether the required mandatory field credit cards category value is getting displayed
And To verify whether on clicking credit cards down chevron is it expanded the category 
And To verify whether on clicking credit cards up chevron is it collapse the category
And To verify whether on clicking credit cards down chevron is it expanded the category

And To verify whether the required mandatory field credit card name is getting displayed
And To verify whether the required mandatory field credit card number is getting displayed
And To verify whether the required mandatory field credit card balance ccy is getting displayed
And To verify whether the required mandatory field credit card balance value is getting displayed
And To verify whether the required mandatory field credit card right chevron is getting displayed
And To verify whether the required mandatory field citi payall title is getting displayed
And To verify whether the required mandatory field citi payall description is getting displayed
And To verify whether the required mandatory field citi payall right chevron is getting displayed

@MRC_Loans
Scenario: Verify MRC Dashboard Loans funcationality

Given Login with username and password
And To verify whether the required mandatory field loan category is getting displayed
And To verify whether the required mandatory field loan category value is getting displayed
And To verify whether on loans down chevron is it expanded the category
And To verify whether on loans up chevron is it collapse the category
And To verify whether on loans down chevron is it expanded the category

And To verify whether the required mandatory field mortgage name is getting displayed
And To verify whether the required mandatory field mortgage number is getting displayed
And To verify whether the required mandatory field mortgage balance ccy is getting displayed
And To verify whether the required mandatory field mortgage balance value is getting displayed
And To verify whether the required mandatory field mortgage right chevron is getting displayed


@MRC_Investment
Scenario: Verify MRC Dashboard Investments functionality

Given Login with username and password
And To verify whether the required mandatory field investments category is getting displayed
And To verify whether the required mandatory field investments category value is getting displayed
And To verify whether on investments down chevron is it expanded the category
And To verify whether on investments up chevron is it collapse the category
And To verify whether on investments down chevron is it expanded the category

And To verify whether the required mandatory field mutualfund account is getting displayed
And To verify whether the required mandatory field mutualfund account number is getting displayed
And To verify whether the required mandatory field mutualfund account balance ccy is getting displayed
And To verify whether the required mandatory field mutualfund account balance value is getting displayed
And To verify whether the required mandatory field mutualfund account right chevron is getting displayed

And To verify whether the required mandatory field bonds account is getting displayed
And To verify whether the required mandatory field bonds account number is getting displayed
And To verify whether the required mandatory field bonds account balance ccy is getting displayed
And To verify whether the required mandatory field bonds account balance value is getting displayed
And To verify whether the required mandatory field bonds account right chevron is getting displayed

