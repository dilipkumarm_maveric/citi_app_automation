@All
Feature: Test Dasshboard funcationality for SG country

#Author:Dilipkumar
#Functionality:Retail Dashboard
#Domain:Enable
#Platform:Android

@Retail_Checkings
Scenario: Verify Retail Dashboard Checkings 

Given Login with username and password
And To verify whether the required mandatory field checking account in depositada is getting displayed
And To verify whether the required mandatory field checking account number in depositada is getting displayed
And To verify whether the required mandatory field checking account manage cta in depositada is getting displayed
And To verify whether the required mandatory field checking account available now label in depositada is getting displayed
And To verify whether the required mandatory field checking account inactive account speak label in depositada is getting displayed
And To verify whether the required mandatory field checking account available now balance ccy in depositada is getting displayed
And To verify whether the required mandatory field checking account available now balance value in depositada is getting displayed
And To verify whether the required mandatory field checking account available now right chevron is getting displayed
And To verify whether the required mandatory field checking account activate your account cta is getting displayed
And To verify whether the required mandatory field checking account make a transfer cta is getting displayed
And To verify whether the required mandatory field checking account global wallet icon is getting displayed
And To verify whether the required mandatory field checking account global wallet header label is getting displayed
And To verify whether the required mandatory field checking account global wallet description is getting displayed
And To verify whether the required mandatory field checking account global wallet right chevron is getting displayed
And To verify whether the required mandatory field checking account statement icon is getting displayed
And To verify whether the required mandatory field checking account statement label is getting displayed
And To verify whether the required mandatory field checking account statement month and year value is getting displayed
And To verify whether the required mandatory field checking account statement right chevron is getting displayed
And To verify whether the required mandatory field checking account last transactions label is getting displayed
And To verify whether the required mandatory field checking account last transaction search icon is getting displayed
And To verify whether the required mandatory field checking account last transaction search inline text is getting displayed
And To verify whether the required mandatory field checking account first transaction date is getting displayed
And To verify whether the required mandatory field checking account first trasaction amount ccy is getting displayed
And To verify whether the required mandatory field checking account first trasaction amount value is getting displayed
And To verify whether the required mandatory field checking account first trasaction description is getting displayed
And To verify whether the required mandatory field checking account first trasaction right chevron is getting displayed
And To verify whether the required mandatory field checking account view all transaction label is getting displayed
And To verify whether the required mandatory field checking account view all transaction right chevron is getting displayed


@Retail_Savings
Scenario: Verify Retail Dashboard Savings 
Given Login with username and password
And To verify whether the required mandatory field savings account in depositada is getting displayed
And To verify whether the required mandatory field savings account number in depositada is getting displayed
And To verify whether the required mandatory field savings account manage cta in depositada is getting displayed
And To verify whether the required mandatory field savings account available now label in depositada is getting displayed
And To verify whether the required mandatory field savings account available now balance ccy in depositada is getting displayed
And To verify whether the required mandatory field savings account available now balance value in depositada is getting displayed
And To verify whether the required mandatory field savings account available now right chevron is getting displayed
And To verify whether the required mandatory field savings account make a transfer cta is getting displayed
And To verify whether the required mandatory field savings account statement icon is getting displayed
And To verify whether the required mandatory field savings account statement label is getting displayed
And To verify whether the required mandatory field savings account statement month and year value is getting displayed
And To verify whether the required mandatory field savings account statement right chevron is getting displayed
And To verify whether the required mandatory field savings account last transactions label is getting displayed
And To verify whether the required mandatory field savings account last transaction search icon is getting displayed
And To verify whether the required mandatory field savings account last transaction search inline text is getting displayed
And To verify whether the required mandatory field savings account first transaction date is getting displayed
And To verify whether the required mandatory field savings account first trasaction amount ccy is getting displayed
And To verify whether the required mandatory field savings account first trasaction amount value is getting displayed
And To verify whether the required mandatory field savings account first trasaction description is getting displayed
And To verify whether the required mandatory field savings account first trasaction right chevron is getting displayed
And To verify whether the required mandatory field savings account view all transaction label is getting displayed
And To verify whether the required mandatory field savings account view all transaction right chevron is getting displayed

@Retail_TimeDeposits
Scenario: Verify Retail Dashboard TimeDeposits 

Given Login with username and password
And To verify whether the required mandatory field time deposits account in depositada is getting displayed
And To verify whether the required mandatory field time deposits account number in depositada is getting displayed
And To verify whether the required mandatory field time deposits account manage cta in depositada is getting displayed
And To verify whether the required mandatory field time deposits total principal label in depositada is getting displayed
And To verify whether the required mandatory field time deposits total principal amount value in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit principal number in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit amount ccy in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit amount value in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit maturity date in depositada is getting displayed
And To verify whether the required mandatory field time deposits first multiple time deposit right chevron in depositada is getting displayed
And To verify whether the required mandatory field time deposits open time deposit cta in depositada is getting displayed
And To verify whether the required mandatory field time deposits statement in depositada is getting displayed
And To verify whether the required mandatory field time deposits statement right chevron in depositada is getting displayed




