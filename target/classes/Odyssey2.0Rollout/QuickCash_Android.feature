@All
Feature: Test Quick cash functionality for SG country mobile app

#Author:Dilipkumar
#Functionality:ALOP - Quick Cash
#Domain:Borrow
#Platform:Android

@QC-MRC-PT0
Scenario: Verify Quick cash MRC PT0 functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen
And Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen

And Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen
And Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen
And verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button

@QC-MRC-PT1
Scenario: Verify Quick cash MRC PT1 functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field My account with other banks is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field My account with other banks is getting navigated to ALOP_410 screen


And To Verify whether the required mandatory Field Other Bank accounts is getting displayed in Header
And To verify whether the bank name select button is navigate to the bank name screen as per the condition
And To Verify whether the required mandatory Field Bank Name header is getting displayed as per the condition
And To Verify whether the required mandatory Field list of account name if select PT1 account navigate to 410 screen
And To Verify whether the required mandatory Field Account Number for above account has to be entered in the account number field
And To verify whether the required mandatory field select bank name and enter account number then visible the next button and navigated to ALOP 600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen
And Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen

And Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen
And Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen
And verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button


@QC-MRC-PT2
Scenario: Verify Quick cash MRC PT2 functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field My account with other banks is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field My account with other banks is getting navigated to ALOP_410 screen


And To Verify whether the required mandatory Field Other Bank accounts is getting displayed in Header
And To verify whether the bank name select button is navigate to the bank name screen as per the condition
And To Verify whether the required mandatory Field Bank Name header is getting displayed as per the condition
And To Verify whether the required mandatory Field list of account name if select PT2 account navigate to 410 screen
And To Verify whether the required mandatory Field Account Number for above account has to be entered in the account number field
And To verify whether the required mandatory field select bank name and enter account number then visible the next button and navigated to ALOP 600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen
And Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen

And Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen
And Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen
And verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button


@QC-MRC-100
Scenario: Verify Quick cash MRC 100 screen functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field We convert your available credit limit into cash, payable in monthly installations among with your monthly Citi card payment is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Your Benefits is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Icon for No documents required is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field No documents required is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field No credit check Instant approval is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field icon for Hassle free process is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Hassle free process is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Done in 60 secs is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field icon for No Processing fees is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field No Processing fees is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Zero hidden costs is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen


@QC-MRC-200
Scenario: Verify Quick cash MRC 200 screen functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Amount Currency is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Amount is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field MinLoanAmount is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field MaxLoanAmount is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Amount edit button is clickable in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field amount digit comma is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field down arrow is getting navigated to Dashboard_200 screen from monthly repayment screen
And Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Lowest rate is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Monthly repayment value with country curreny code is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Monthly repayment value for is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Monthly repayment value with down arrow is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field Nominal Interest Rate Value with percentage and p a is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field EIR is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field EIR Value Interest Rate from API is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field % is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field p a is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field Question is getting displayed in ALOP_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen


@QC-MRC-600
Scenario: Verify Quick cash MRC 600 functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Loan Details is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Review & confirm is getting displayed with specified values in ALOP_600 screen
And Verify whether the required mandatory Field Your Quick Cash amount is getting displayed as per the condition If ALOP offer product type is Credit Credit based on ALOP_200 screen selected amount in ALOP_600 screen
And Verify whether the required mandatory Field Your Quick Cash amount with CCY is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Your Quick Cash amount with decimal validated is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Monthly repayment is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field monthly repayment Amount is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field monthly repayment Amount Country currency code is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field monthly repayment value with for is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Account Details is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Charge to is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Bank name with product name and account number display with mock is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Nominal interest rate Label is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field EIR is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field EIR Value is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field % is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field p a is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Receiving Quick Cash at is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Source Account Name is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Source Account Number is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field select account is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field repayment schedule is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Tap Get cash now is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field PDF is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Terms and Conditions is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field PDF Terms and conditions is navigate to respective page in ALOP_600 screen
And Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen


@QC-MRC-RPS
Scenario: Verify Quick cash MRC Repayment Schedule functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field repayment schedule is getting displayed in ALOP_600 screen
And verify whether the application is getting navigated to the required screen repayement schedule on clicking the provided Button navigate to respective page in ALOP_600 screen

And Verify whether the required mandatory Field Repayment Schedule is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Repayment Details is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Your Quick Cash amount Label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field our Quick Cash amount value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Your Quick Cash amount value country currency code is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Your Quick Cash amount value decimal validation is displayed in repayment schedule screen
And Verify whether the required mandatory Field First month instalment label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field First month instalment amount value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field First Month instalment amount value with country currency code is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field First month instalment amount double validation is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Subsequent month instalment label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Subsequent month instalment amount value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Subsequent Month instalment amount value with country currency code is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Subsequent month instalment amount double validation is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Repayment Schedule is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Instalment label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Principle label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field interest label is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Instalment value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Principle value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field interest value is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field Principle amount country currency code is getting displayed in repayment schedule screen
And Verify whether the required mandatory Field interest amount country currency code is getting displayed in repayment schedule screen

@QC-MRC-700
Scenario: Verify Quick cash MRC 700 functionality for Odyssey2.0 app

Given Login with username and password
And Click on cards in dashboard screen
And Click on quick cash in dashboard screen

And Verify whether the required mandatory Field What is quick cash is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting displayed in ALOP_Own accounts_100 screen
And Verify whether the required mandatory Field Get started is getting navigated in ALOP_Own accounts_100 screen

And Verify whether the required mandatory Field Quick Cash is getting displayed in ALOP_Own accounts_200 screen
And Verify whether the required mandatory Field next arrow is getting navigated to_400 screen

And Verify whether the required mandatory Field Send Quick cash to is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field Masked Account number is getting displayed in ALOP_400 screen
And Verify whether the required mandatory Field own account detail with checkbox is select navigate to ALOP_600 screen

And Verify whether the required mandatory Field Review & confirm is getting displayed in ALOP_600 screen
And Verify whether the required mandatory Field Get cash now is getting displayed in ALOP_600 screen
And Verify whether the application is getting navigated to the required screen ALOPBooking_700 if success on clicking get cash now from ALOP_600 screen

And Verify whether the required mandatory Field Tick image is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field All Done! is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Your QuickCash of should be available in your Citibank account instantly in ALOP_700 screen
And Verify whether the required mandatory Field View summary label is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field View summary drop down option is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field First instalment is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field First instalment amount is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field First instalment amount country currency code is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Subsequent monthly instalments is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Subsequent monthly instalments amount is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Subsequent monthly instalments amount country currency code is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Repayment period is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Repayment period value is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Nominal Interest Rate is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Nominal Interest rate value is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field % is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field p a is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field repayment schedules is getting displayed in ALOP_700 screen
And Verify whether the required mandatory Field Done is getting displayed in ALOP_700 screen
And verify whether the application is getting navigated to the required screen : Dashboard_100 on clicking the provided image or Button