@All
Feature: Test Positive Friction Banner functionality for HK country

#Author:Dilipkumar
#Functionality:Positive Friction Banner - Dynamic Content - User defined field
#Domain:Grow
#Platform:Android


@UDF_Cards
Scenario: Verify Dynamic Contents UDF Empty Key Value for Cards Products

Given Login with username and password
And To verify whether the required mandatory field credit card category is getting displayed
And To verify whether the required mandatory field speak label for credit card category is getting displayed
And To verify whether on clicking on credit card category to expand to show all cards products
And To verify whether the required mandatory field speak label for credit card product is getting displayed
And To verify whether the required mandatory field speak label action button under credit card product is getting displayed
And To verify whether the required mandatory field todo widget title in cards category is getting displayed
And To verify whether the required mandatory field todo widget description in cards category is getting displayed
And To verify whether the required mandatory field todo widget action button under credit card product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer thumbnail in cards category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in cards category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in cards category is getting displayed
And To verify whether on clicking on the selected credit card which has speak label is navigating to the cards dashboard
And To verify whether the required mandatory field speak label for credit card in cards dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in cards dashboard is getting displayed
And To verify whether the required mandatory field todo widget title in cards dashboard is getting displayed
And To verify whether the required mandatory field todo widget description in cards dashboard is getting displayed
And To verify whether the required mandatory field todo widget action button in cards dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer thumbnail in cards dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in cards dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer thumbnail in cards dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer description in cards dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer thumbnail in cards dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in cards dashboard is getting displayed
And Close the application
