@All
Feature: Test Positive Friction Banner Theme Takeover for PH country

#Author:Dilipkumar
#Functionality:Positive Friction Banner - Theme Takeover
#Domain:Grow
#Platform:Android

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking CVP CTA Action pointed to Theme Takeover offer which is navigating to CVP Screen
And To verify whether the required mandatory field expected CVP screen offer title is getting displayed
And To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC WebView Yes and No CTA Action

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Theme Takeover offer which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen
And To verify whether on clicking WebView CTA Action pointed to Theme Takeover offer which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC OK CTA Action

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Theme Takeover offer which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed
And To Verify whether the required mandatory Field CTA_OK is getting displayed
And To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer
And To Verify whether the required mandatory Field CTA_OK_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC WebView Error 

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Theme Takeover offer which is navigating to expected Webview Screen
And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed
And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always
And To Verify whether on clicking CTA_CLOSE is closing the drawer and navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC STO Action

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking STO Journey CTA Action pointed to Theme Takeover offer which is navigating to expected STO Journey Screen
And To verify whether the required mandatory field expected STO screen offer title is getting displayed
And To verify whether on clicking back button in STO screen which is navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Theme Takeover functionality for MRC STP Action Journey

Given Login with username and password
And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking STP Journey CTA Action pointed to Theme Takeover offer which is navigating to expected STP Journey Screen
And To verify whether the required mandatory field expected STP screen offer title is getting displayed
And To verify whether on clicking back button in STP screen which is navigate back to dashboard screen

@Banner_MRCThemeTakeover
Scenario: Verify Banner and Speak Label functionality

Given Login with username and password

And To Verify whether the required mandatory Field Theme Takeover offer title is getting displayed as per the condition Delightbox widget is only applicable for mrc dashboard screens
And To Verify whether the required mandatory Field Theme Takeover offer thumbnail is getting displayed as per the condition Optional Image icon
And To Verify whether the required mandatory Field Theme Takeover offer decscription is getting displayed as per the condition Array containing all UDF variable keys

#CVP

And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking CVP CTA Action pointed to Theme Takeover offer which is navigating to CVP Screen
And To verify whether the required mandatory field expected CVP screen offer title is getting displayed
And To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen

#STP 
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking STP Journey CTA Action pointed to Theme Takeover offer which is navigating to expected STP Journey Screen
And To verify whether the required mandatory field expected STP screen offer title is getting displayed
And To verify whether on clicking back button in STP screen which is navigate back to dashboard screen
#STO

And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking STO Journey CTA Action pointed to Theme Takeover offer which is navigating to expected STO Journey Screen
And To verify whether the required mandatory field expected STO screen offer title is getting displayed
And To verify whether on clicking back button in STO screen which is navigate back to dashboard screen
#CVP

And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking CVP CTA Action pointed to Theme Takeover offer which is navigating to CVP Screen
And To verify whether the required mandatory field expected CVP screen offer title is getting displayed
And To verify whether on clicking back button in CVP screen which is navigate back to dashboard screen
#NO_ACTION
And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard should not getting displayed
And To verify whether on clicking No Action CTA Action pointed to Theme Takeover offer which should not navigating to any screen
#WEBVIEW

And To verify whether the required mandatory field Theme Takeover offer right chevron in dashboard is getting displayed
And To verify whether on clicking WebView CTA Action pointed to Theme Takeover offer which is navigating to expected Webview Screen
And To verify whether the required mandatory field expected WebView screen offer title is getting displayed

#Webview YES CTA

And To Verify whether the required mandatory Field CTA_YES is getting displayed
And To Veirfy whether on clicking CTA_YES is navigating to CTA_YES_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_YES_SUCCESS_BODY_CONTENT is getting displayed
#Webview NO CTA

And To Verify whether the required mandatory Field CTA_NO is getting displayed
And To Veirfy whether on clicking CTA_NO is navigating to CTA_NO_SUCCESS_HEADER drawer
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_NO_SUCCESS_BODY_CONTENT is getting displayed
#Webview OK CTA

And To Verify whether the required mandatory Field CTA_OK is getting displayed
And To Veirfy whether on clicking CTA_OK is navigating to CTA_OK_BODY_CONTENT drawer
And To Verify whether the required mandatory Field CTA_OK_BODY_CONTENT is getting displayed

#Webview Error CTA

And To Verify whether the required mandatory Field CTA_ERROR_HEADER is getting displayed
And To Verify whether the required mandatory Field CTA_ERROR_BODY_CONTENT is getting displayed

And To Verify whether the required mandatory Field CTA_CLOSE is getting displayed as per the condition Always