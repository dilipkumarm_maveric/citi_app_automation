@All
Feature: Test Positive Friction Banner functionality for APAC country

@Banner_ProductPalette
Scenario: Verify Get More Product Palette Maximum 6 functionality

Given Login with username and password
And To verify whether the required mandatory field get more in footer menu is getting displayed 
And To verify whether on clicking on get more in footer menu it is navigating to get more offer screen page
And To verify whether the required mandatory field product palette price tag icon is getting displayed 
And To verify whether the required mandatory field product palette header title is getting displayed 
And To verify whether the required mandatory field product palette category1 icon is getting displayed 
And To verify whether the required mandatory field product palette category1 label is getting displayed 
And To verify whether the required mandatory field product palette category2 icon is getting displayed 
And To verify whether the required mandatory field product palette category2 label is getting displayed 
And To verify whether the required mandatory field product palette category3 icon is getting displayed 
And To verify whether the required mandatory field product palette category3 label is getting displayed
And To verify whether the required mandatory field product palette category4 icon is getting displayed
And To verify whether the required mandatory field product palette category4 label is getting displayed
And To verify whether the required mandatory field product palette category5 icon is getting displayed 
And To verify whether the required mandatory field product palette category5 label is getting displayed
And To verify whether the required mandatory field product palette category6 icon is getting displayed
And To verify whether the required mandatory field product palette category6 label is getting displayed 
And To verify whether the required mandatory field product palette view all product label is getting displayed
And To verify whether on clicking on view all product label it is navigating to webview page
And To verify whether the required mandatory field product palette view all product webview title is getting displayed 
And To verify whether on clicking on webview back button it is navigating to get more offer screen page


@Banner_SharewithFriends
Scenario: Verify Share With Friends functionality

Given Login with username and password
And To verify whether the required mandatory field get more in footer menu is getting displayed 
And To verify whether on clicking on get more in footer menu it is navigating to get more offer screen page
And To verify whether the required mandatory field share with friends icon is getting displayed
And To verify whether the required mandatory field share with friends title is getting displayed
And To verify whether the required mandatory field share with friends description is getting displayed
And To verify whether the required mandatory field share with friends right chevron is getting displayed
And To verify whether on clicking share with friends right chevron it is navigating to mobile share drawer


@Banner_AddToSupplementaryCards
Scenario: Verify Add to Supplementary cards functionality

Given Login with username and password
And To verify whether the required mandatory field activate your replacement card is getting displayed 
And To verify whether on clicking activate your replacement card it is navigating to replacement card activation drawer
And To verify whether the required mandatory field yes button in replacement card activation drawer is getting displayed 
And To verify whether on clicking yes button in replacement card activation drawer it is navigating to OTP screen page
And To verify whether the required mandatory field Enter CVV number drawer is getting displayed
And To verify whether the entering the cvv value in Enter CVV field
And To verify whether the required mandatory field activate button is enabled is getting displayed
And To verify whether on clicking activate button it is navigating to card activation drawer
And To verify whether the required mandatory field your card is activated is getting displayed
And To verify whether the required mandatory field add to suppplementaty card header is getting displayed
And To verify whether the required mandatory field add to suppplementaty card description is getting displayed
And To verify whether on clicking add to suppplementaty card it is navigating to STO navigation screen page







