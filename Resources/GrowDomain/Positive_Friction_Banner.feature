@All
Feature: Test Positive Friction Banner functionality for APAC country

#Author:Dilipkumar
#Functionality:Positive Friction Banner - SP Upsell,TJ Sell,X Sell,Speak Label,Action Button
#Domain:Grow
#Platform:Android

@Friction_Cards
Scenario: Verify Banner and Speak Label functionality for Cards

Given Login with username and password
And To verify whether the required mandatory field credit card category is getting displayed
And To verify whether the required mandatory field speak label for credit card category is getting displayed
And To verify whether on clicking on credit card category to expand to show all cards products
And To verify whether the required mandatory field speak label for credit card product is getting displayed
And To verify whether the required mandatory field speak label action button under credit card product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in cards category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in cards category is getting displayed
And To verify whether on clicking on the selected credit card which has speak label is navigating to the cards dashboard
And To verify whether the required mandatory field speak label for credit card in cards dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in cards dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in cards dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer description in cards dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in cards dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in cards dashboard is getting displayed
And Close the application

@Friction_Loans
Scenario: Verify Banner and Speak Label functionality for Loans

Given Login with username and password
And To verify whether the required mandatory field loans category is getting displayed
And To verify whether the required mandatory field speak label for loans category is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in loans dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in loans dashboard is getting displayed
And To verify whether on clicking on loans category to expand to show all loans products
And To verify whether the required mandatory field speak label for loans product is getting displayed
And To verify whether the required mandatory field speak label action button under loans product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in loans category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in loans category is getting displayed
And To verify whether on clicking on the selected loans which has speak label is navigating to the loans dashboard
And To verify whether the required mandatory field speak label for loans in loans dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in loans dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in loans dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in loans dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer title in loans dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer description in loans dashboard is getting displayed
And Close the application

@Friction_Deposit
Scenario: Verify Banner and Speak Label functionality for Deposit

Given Login with username and password
And To verify whether the required mandatory field cross sell ghost deposit banner offer title in mrc dashboard is getting displayed
And To verify whether the required mandatory field cross sell ghost deposit banner offer description in mrc dashboard is getting displayed
And To verify whether the required mandatory field deposit category is getting displayed
And To verify whether the required mandatory field speak label for deposit category is getting displayed
And To verify whether on clicking on deposit category to expand to show all deposit products
And To verify whether the required mandatory field speak label for deposit product is getting displayed
And To verify whether the required mandatory field speak label action button under deposit product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in deposit category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in deposit category is getting displayed
And To verify whether on clicking on the selected deposit which has speak label is navigating to the deposit dashboard
And To verify whether the required mandatory field speak label for deposit in deposit dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in deposit dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in deposit dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in deposit dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer title in deposit dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer description in deposit dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in deposit dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in deposit dashboard is getting displayed
And Close the application

@Friction_Investment
Scenario: Verify Banner and Speak Label functionality for Investment

Given Login with username and password
And To verify whether the required mandatory field investment category is getting displayed
And To verify whether the required mandatory field speak label for investment category is getting displayed
And To verify whether on clicking on investment category to expand to show all investment products
And To verify whether the required mandatory field speak label for investment product is getting displayed
And To verify whether the required mandatory field speak label action button under investment product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in investment category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in investment category is getting displayed
And To verify whether on clicking on the selected investment which has speak label is navigating to the investment dashboard
And To verify whether the required mandatory field speak label for investment in investment dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in investment dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in investment dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in investment dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in investment dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in investment dashboard is getting displayed
And Close the application

@Friction_Insurance
Scenario: Verify Banner and Speak Label functionality for Insurance

Given Login with username and password
And To verify whether the required mandatory field insurance category is getting displayed
And To verify whether the required mandatory field speak label for insurance category is getting displayed
And To verify whether on clicking on insurance category to expand to show all insurance products
And To verify whether the required mandatory field speak label for insurance product is getting displayed
And To verify whether the required mandatory field speak label action button under insurance product is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in insurance category is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in insurance category is getting displayed
And To verify whether on clicking on the selected insurance which has speak label is navigating to the insurance dashboard
And To verify whether the required mandatory field speak label for insurance in insurance dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in insurance dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in insurance dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in insurance dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in insurance dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in insurance dashboard is getting displayed
And Close the application

@Friction_Retail
Scenario: Verify Banner and Speak Label functionality for Retail

Given Login with username and password
And To verify whether the required mandatory field speak label for retail in retail dashboard is getting displayed
And To verify whether the required mandatory field speak label action button in retail dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer title in retail dashboard is getting displayed
And To verify whether the required mandatory field sp upsell banner offer description in retail dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer title in retail dashboard is getting displayed
And To verify whether the required mandatory field tj upsell banner offer description in retail dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer title in retail dashboard is getting displayed
And To verify whether the required mandatory field cross sell banner offer description in retail dashboard is getting displayed
And Close the application
