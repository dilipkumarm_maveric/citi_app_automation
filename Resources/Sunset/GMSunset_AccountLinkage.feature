@All
Feature: Test GM Sunset Refresh functionality for APAC country

@GMSunset_AccountLinkage
Scenario: Verify GM Sunset Account Linkage functionality

Given Login with username and password
And To verify whether the required mandatory field Profile & Settings icon in Dashboard is getting displayed
And To verify whether on clicking Profile & Settings icon is navigate to Settings page
And To verify whether the required mandatory field GM Refresh in settigs page is getting displayed
And To verify whether on clicking GM Refresh is navigate to New Profile & Settings page
And To verify whether the required mandatory field Debit card is getting displayed
And To verify whether the required mandatory field Accounts is getting displayed
And To verify whether on clicking Debit card is navigate to Manage cards & accounts page
And To verify whether the required mandatory field Account Linkage is getting displayed
And To verify whether on clicking Account Linkage is navigate to Debit card linkage page
And To verify whether the required mandatory field select debit card in select card page is getting displayed
And To verify whether on clicking on any one of the debit card in select card page is navigate to Debit card linkage page
And To verify whether the required mandatory field back arrow in linkage page is getting displayed 
And To verify whether the required mandatory field Debit card details in linkage page is getting displayed 
And To verify whether the required mandatory field Currently linked label in linkage page is getting displayed 
And To verify whether the required mandatory field Other Accounts label in linkage page is getting displayed 
And To verify whether the required mandatory field currently linked account details in linkage page is getting displayed 
And To verify whether the required mandatory field other account details in linkage page is getting displayed 
And To verify whether on clicking any one of the other account to link debit card for selection 
And To verify whether the required mandatory field Disclaimer in linkage page is getting displayed 
And To verify whether the required mandatory field Update button is getting displayed
And To verify whether on clicking update button for is navigate to linkage 300 page 
And To verify whether the required mandatory field tick image in linkage 300 page is getting displayed 
And To verify whether the required mandatory field successful message in linkage 300 page is getting displayed 
And To verify whether the required mandatory field card number label in linkage 300 page is getting displayed 
And To verify whether the required mandatory field card number value in linkage 300 page is getting displayed 
And To verify whether the required mandatory field account linked label in linkage 300 page is getting displayed 
And To verify whether the required mandatory field account linked details in linkage 300 page is getting displayed 
And To verify whether the required mandatory field done button in linkage 300 page is getting displayed 
And To verify whether on clicking done button for is navigate to Manage cards & accounts page 





